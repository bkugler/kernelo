using LinearAlgebra
using KissMCMC
using Kernelo.Models
using Kernelo.Pred
using Dates
using Statistics

include("../playground/procedure.jl")
include("../playground/comptimes.jl")

abstract type StdObs end

struct StdRatio <: StdObs
    ratio::Float64
end
resoud_std(so::StdRatio, yobs::Vector, _::Int) = yobs ./ so.ratio

struct StdFixed <: StdObs
    stds::Matrix{Float64}
end
resoud_std(so::StdFixed, _::Vector, index::Int) = so.stds[:, index]


"""
 Parametrize the MCMC inversion 
"""
struct MCMCParams
    Niter::Int
    Nkeep::Int
    alpha::Float64

    # to compute the covariance sigma_train ^ 2 + (y / ratio) ^ 2
    std_train::Float64
    std_obs::StdObs
end
MCMCParams(std_train::Float64, std_obs_ratio::Int) = MCMCParams(10_000_000, 5_000_000, 0.02, std_train, StdRatio(std_obs_ratio))
MCMCParams(std_train::Float64, Ystds::Matrix) = MCMCParams(10_000_000, 5_000_000, 0.02, std_train, StdFixed(Ystds))

function ComputationTime(context::AbstractContext, mcmc_params::MCMCParams, Nobs::Int)
    ComputationTime(0, ComputationMCMC, Millisecond(0), Millisecond(0), Millisecond(0), "MCMC - " * context.LABEL, context.L, context.D, 0, 0, Nobs, mcmc_params.Niter)
end


"""
 Invert observations one by one, using MCMC
"""
function complete_prediction(c::AbstractContext, Yobs::Matrix{T}, params::MCMCParams) where {T}
    _, N = size(Yobs)

    @info "Starting $N inversions with MCMC..."
    Log.indent_log()
    
    ti = Dates.now()
    monitor = Monitor(N; step = 4)
    out = Vector{CompleteInversion}(undef, N)
    accept_ratios = zeros(N)
    # for n in 1:N
    Threads.@threads for n in 1:N
        yobs = Yobs[:, n]
        yobs_std = resoud_std(params.std_obs, yobs, n) 

        Sigma = Diagonal(params.std_train^2 .+ yobs_std.^2)
        thetas, accept_ratios[n] = _sample_from_posterior(c, yobs, Sigma, params)
        out[n] = _exploit_samples(c, yobs, thetas)
        update(monitor)
    end

    Log.deindent_log()
    @debug "Average accept ratio: $(mean(accept_ratios))"
    duration = since(ti)
    @debug "Time for invertions : $(Dates.canonicalize(Dates.CompoundPeriod(duration))) sec."

    out, duration
end


function _sample_from_posterior(c::AbstractContext, y::Vector{T}, Sigma::DiagCov{T}, params::MCMCParams) where {T <: Number}
    L = c.L
    a, mu = 1., zeros(c.D)

    # target density
    logpdf(x::Vector{T}) = Models.log_conditionnal_density(c, x, y, a, mu, Sigma)
    # samples the proposal (or jump) distribution
    sample_prop_normal(theta::Vector{T}) = 0.02 * randn(L) .+ theta
    # starting points
    theta0 = 0.5 .+ zeros(4)

    thetas, accept_ratio = metropolis(logpdf, sample_prop_normal, theta0,
        niter = params.Niter, use_progress_meter = false, nburnin = params.Niter - params.Nkeep)
    return hcat(thetas...), accept_ratio
end

function _exploit_samples(c::AbstractContext, yobs::VecOrSub{T}, thetas::Matrix{T})::CompleteInversion where {T}
    L, keep = size(thetas)
    a, mu = 1., zeros(c.D)

    # computing estimators
    mean = (sum(thetas; dims = 2) ./ keep)[:, 1]
    cov  = zeros(L)
    for k in 1:keep
        u = (mean .- thetas[:, k]).^2 # only diagonal terms
        cov .+= u
    end
    cov /= keep

    # Y err (with x in mathematical space)
    y_err = Y_relative_err(c, yobs, mean, 1., mu)

    # rescale x
    mean, cov = Models.to_physique(c, mean, cov)

    CompleteInversion(Methods.MCMC, mean, cov, y_err, nothing, Dict())
end
