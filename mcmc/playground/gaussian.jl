using KissMCMC
using LinearAlgebra

""" Y = AX + Eps , where X ~ N(0, Gamma), Eps ~ N(0, Sigma) """
struct Model 
    Gamma::Symmetric
    A::Matrix
    Sigma::Symmetric
end

function posterior(m::Model, y::Vector)
    Sigma_inv = inv(m.Sigma)
    SigmaS = inv(inv(m.Gamma) + m.A' * Sigma_inv * m.A) 
    SigmaS * m.A' * Sigma_inv * y, SigmaS
end

""" Conditionnal expectancy of X | Y = y """
function posterior_expectancy(m::Model, y::Vector)
    mean, _ = posterior(m, y)
    mean
end

function _create_cov(L::Int) 
    t = UpperTriangular(0.1 * rand(L,L))
    Symmetric(Diagonal(ones(L)) + t * t')
end


""" proportionnal to the real posterior """
function _logpdf(m::Model, y::Vector, x::Vector)
    mean, cov = posterior(m, y)
    u = x .- mean 
    -0.5 * sum(u .* (inv(cov) * u))
end

A = [1 0
    0 1
    0 1]
    
m = Model(20 * _create_cov(2), A, 0.01 * _create_cov(3))
x = randn(2)
y = A * x 


logpdf(x::Vector) = _logpdf(m, y, x)
sample_prop_normal(theta) = 1.5*randn(2) .+ theta # samples the proposal (or jump) distribution
theta0 = zeros(2)

thetas, accept_ratio = metropolis(logpdf, sample_prop_normal, theta0, niter=10^5)
@show x 
@show posterior_expectancy(m, y)
@show sum(thetas) ./ length(thetas)