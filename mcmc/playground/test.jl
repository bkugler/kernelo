include("../procedure.jl")

c = Models.NontroniteHapkeContext()
Yobs, wavelengths, _  = Models.load_observations(c)

params = MCMCParams(0.001, 20)
complete_prediction(c, Yobs, params)