using Kernelo.Models
using LinearAlgebra
using KissMCMC 
using Serialization 
using Kernelo.Archive
using Kernelo.Pred
using Dates
using Plots 
using LaTeXStrings

include("../../playground/setup.jl")
include("../../playground/plotting/base_plots.jl")
include("../../playground/procedure.jl")

# using Log 

c = Models.NontroniteHapkeContext()
Yobs, wavelengths, _  = Models.load_observations(c)
Xmcmc, Covsmcmc = Models.load_results_alt(c, false)
res = deserialize(Archive.get_path(Archive.FOLDER_MISC, c, :tmp; label = "hapke_lab"))
Xismean = res["is_mean"]
Xbest = res["best"]

const L_, D = c.L, c.D
const a, mu = 1., zeros(D)
const _, N = size(Yobs)

function _sample_from_posterior(y::Vector{T}, nb_try, keep) where {T <: Number}
    Sigma = Diagonal(0.001^2 .+ (y ./ 20).^2)

    logpdf(x::Vector{T}) = Models.log_conditionnal_density(c, x, y, a, mu, Sigma)
    sample_prop_normal(theta::Vector{T}) = 0.02 * randn(L_) .+ theta # samples the proposal (or jump) distribution
    theta0 = 0.5 .+ zeros(4)

    # save all samples, then average / plot 
    thetass = zeros(T, L_, keep, nb_try)
    Niter = 10^7

    for i in 1:nb_try
        # Threads.@threads for i in 1:nb_try
        @debug "try $i / $nb_try"
        thetas, accept_ratio = metropolis(logpdf, sample_prop_normal, theta0,
            niter = Niter, use_progress_meter = false, nburnin = Niter - keep, nthin = 1)
        @debug "\tAccept ratio : $accept_ratio"
        thetas_mat = hcat(thetas...)
        thetass[:, :, i] = thetas_mat
    end

    return thetass
end

function _exploit_samples(index, thetass; showplot = false, showtraces = false)
    L_, keep, nb_try = size(thetass)
    means, covs = zeros(L_, nb_try), zeros(L_, nb_try)
    for i in 1:nb_try
        if showtraces # plotting dispertion 
            Log.indent_log()
            @debug "Plotting dispertion..."
            subplots = []
            for l in 1:4
                p1 = scatter(1:keep, thetass[l,:, i];
                    ylabel = c.variables_names[l],
                    label = "My MCMC", dpi = 300, markersize = 1, markerstrokewidth = 0,
                    ylims = (0, 1))
                push!(subplots, p1)
            end
            p = plot(subplots...; layout = (2, 2))
            savepath = Archive.get_path(Archive.FOLDER_GRAPHS, c, index, keep, i; label = "mcmc_disp")   
            png(p, savepath)
            @debug "Dispertion plotted in $savepath"
            Log.deindent_log()
        end
        
        # computing estimators
        mean = (sum(thetass[:, :, i]; dims = 2) ./ keep)[:, 1]
        cov  = zeros(L_)
        for k in 1:keep
            u = (mean .- thetass[:, k, i]).^2 # only diagonal terms
            cov .+= u
        end
        cov /= keep
        means[:, i] = mean
        covs[:, i] = cov
    end

    if showplot
        p = scatter(means[3,:], means[4,:], xlabel = "b", ylabel = "c", label = "My MCMC", xlims = (0, 1), ylims = (0, 1))
        scatter!(p, Xmcmc[3, index:index], Xmcmc[4, index:index], label = "MCMC Ref", xlims = (0, 1), ylims = (0, 1))
        scatter!(p, Xismean[3, index:index], Xismean[4, index:index], label = "GLLiM - IS - Mean", xlims = (0, 1), ylims = (0, 1))
        scatter!(p, Xbest[3, index:index], Xbest[4, index:index], label = "GLLiM - Best", xlims = (0, 1), ylims = (0, 1))

        savepath = Archive.get_path(Archive.FOLDER_GRAPHS, c, index, keep; label = "comp_mcmc_$(keep)")
        png(p, savepath)
        @info "Saved in $savepath"
    end
    sum(means; dims = 2)[:, 1] / nb_try, sum(covs; dims = 2)[:, 1] / nb_try
end

function inverse_one(index; showplot = false, showtraces = false, nb_try = nb_try, keep = 5_000_000)
    y = Yobs[:, index]
    Log.indent_log()
    thetass = _sample_from_posterior(y, nb_try, keep)
    Log.deindent_log()
    mean, cov = _exploit_samples(index, thetass; showplot = showplot, showtraces = showtraces)
    mean, cov
end 

inverse_one(33; nb_try = 10, keep = 1_000_000, showplot = true, showtraces = true)
# thetass = _sample_from_posterior(33, 10, 10_000)
# mean, cov = _exploit_samples(33, thetass; showplot = true, showtraces = true)


function inverse_serie()
    means, covs = zeros(L_, N), zeros(L_, N)
    ti = Dates.now()
    Log.indent_log()
    for n in 1:N
        @debug "Starting inversion $n / $N"
        means[:, n], covs[:, n] = inverse_one(n; nb_try = 1, keep = 5_000_000)
    end
    Log.deindent_log()
    ms =  Dates.now() - ti
    @show ms
    Archive.save([means, covs], Archive.FOLDER_PREDICTIONS, c, :my_mcmc)
end
# inverse_serie()
means, covs = Archive.load(Archive.FOLDER_PREDICTIONS, c, :my_mcmc)

means, covs = Models.to_physique(c, means, covs)

# Y err take mathematical X
Yerr1 = Y_relative_err(c, Yobs, Models.from_physique(c, means), a, mu)
Yerr2 = Y_relative_err(c, Yobs, Models.from_physique(c, Xmcmc), a, mu)
Yerr3 = Y_relative_err(c, Yobs, Models.from_physique(c, Xismean), a, mu)

predictions = [
    Prediction(Xmcmc, Covsmcmc; config = SerieConfig("MCMC", :basic, "green"), Yerr = Yerr2),
    Prediction(means, covs; config = SerieConfig("My MCMC", :basic, "brown"), Yerr = Yerr1),
    Prediction(Xismean; config = SerieConfig("GLLIM -IS", :basic, "red"), Yerr = Yerr3)
]
Plots.pyplot()
savepath = Archive.get_path(Archive.FOLDER_GRAPHS, c, :comp_mcmc)
pl = plot_components(c, predictions; savepath = savepath)
