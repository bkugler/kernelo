using LaTeXStrings
using Statistics 
using Kernelo.Models 
using Kernelo.Noise 
using Kernelo.Archive
using Kernelo.Gllim
using MAT 
using Dates 

include("../playground/setup.jl")
include("../playground/procedure.jl")
include("latex_tables.jl")
include("../playground/inversion_cube.jl")
include("../playground/plotting/base_plots.jl")
include("../playground/plotting/plots.jl")
include("../mcmc/procedure.jl")

"""
 ignore nan values 
"""
_safe_err(V) = (v = V[isfinite.(V)]; St(Statistics.mean(v), Statistics.std(v)))
"""
 use infinite norm so error stays in [0,1] 
"""
_err(pred, ref) = (err = maximum(abs.(pred .- ref); dims = 1); _safe_err(err))

"""
    Match each predicton to either "ref" or "alt".
"""
function unmix_centroids(Xref::Matrix, Xcentroid1::Vector{CompleteInversion}, Xcentroid2::Vector{CompleteInversion})
    _, N = size(Xref)
    ref, alt = similar(Xref), similar(Xref)
    for n in 1:N 
        e1, e2 =  norm(Xcentroid1[n].mean .- Xref[:, n]), norm(Xcentroid2[n].mean .- Xref[:,n])
        if e1 > e2 # 2 is closer to ref
            ref[:, n] = Xcentroid2[n].mean
            alt[:, n] = Xcentroid1[n].mean
        else # 1 is closer to ref
            ref[:, n] = Xcentroid1[n].mean
            alt[:, n] = Xcentroid2[n].mean
        end
    end
    return ref, alt
end

"""
 Compute errors statistics for the given results 
"""
function basic_measure(context::Models.AbstractContext, Xobs::Matrix, mat_preds::Matrix)
    # we compute errors in math. space 
    Xobs = Models.from_physique(context, Xobs)
    
    meths = [Methods.GllimMean, Methods.GllimMergedCentroid(1), Methods.GllimMergedCentroid(2), Methods.IsMean, Methods.IsCentroid(1), Methods.IsCentroid(2), Methods.Best]

    series = extract_series(mat_preds, meths)

    aggregateY = serie->_safe_err([v.y_err for v in serie])

    errors = Dict() # simple errors 
    for meth in meths 
        # x error 
        Xpred = aggregate(series[meth])
        # we normalise to have comparable values (in [0,1])
        Xpred = Models.from_physique(context, Xpred)
        err_X = _err(Xpred, Xobs)
        
        # residual error
        err_Ypred = aggregateY(series[meth])

        errors[meth]  = BasicError(err_X, err_Ypred)
    end 
    
    # special case for double solutions 
    alt_X = Models.alternative_solution(context, Xobs)
    # centroid errors 
    double = nothing
    # centroid with IS errors 
    doubleIS = nothing
    if alt_X !== nothing 
        # we match centroids predictions
        ref, alt = unmix_centroids(Xobs, series[Methods.GllimMergedCentroid(1)], series[Methods.GllimMergedCentroid(2)])
        err_X1 = _err(ref, Xobs)
        err_X2 = _err(alt, alt_X)
        double = DoubleSolutionError(err_X1, err_X2)
        
        ref_IS, alt_IS = unmix_centroids(Xobs, series[Methods.IsCentroid(1)], series[Methods.IsCentroid(2)])
        err_XIS1 = _err(ref_IS, Xobs)
        err_XIS2 = _err(alt_IS, alt_X)
        doubleIS = DoubleSolutionError(err_XIS1, err_XIS2)
    end

    # group the results
    return MesureOutput(errors, double, doubleIS)
end

function double_solutions_simple(run, context::Models.AbstractContext, gllim_params::MetaGLLiM, Nobs::Int, is_params::DataCovIs)
    Xobs =  vcat([ _wave_sample(Nobs, l)' for l in 1:context.L]...) 
    Xobs_alt = Models.alternative_solution(context, Xobs)
    Yobs = F(context, Xobs)

    mat_preds, _,  ct = train_inverse(run, context, Yobs, gllim_params, is_params)
    regularize_inversions!(mat_preds, [ Methods.GllimMergedCentroid(""), Methods.IsCentroid("")])
    configs = [
        Methods.GllimMean => SerieConfig(N.GllimMean, :basic, "red"; alpha = 0.8),
        Methods.IsMean => SerieConfig(N.IsMean, :dashed, "red"),
        Methods.GllimMergedCentroid(1) => SerieConfig(N.GllimMergedCentroid1, :basic, "blue"; alpha = 0.4),
        Methods.GllimMergedCentroid(2) => SerieConfig(N.GllimMergedCentroid2, :basic, "orange"; alpha = 0.5),
    ]
    preds = format_inversions(mat_preds, configs)

    preds = [preds...,
        Prediction(Xobs; config = SerieConfig(N.Xalt1, :basic, "green"; alpha = 0.3)),
        Prediction(Xobs_alt; config = SerieConfig(N.Xalt2, :basic, "green"; alpha = 0.3)),
    ]

    path = Archive.get_path(Archive.FOLDER_GRAPHS, context, gllim_params, is_params; label = "double_solutions_simple_K$(gllim_params.K)")
    plot_components(context, preds;  savepath = path, varnames = [L"x"], xlabel = L"n")
    return [ct]
end

function double_solutions_complete(run, context::Models.QuadraticDoubleContext, variants::Vector{Gllim.GLLiMTraining}, Nobs::Int, is_params::DataCovIs)
    Xobs =  vcat([ _wave_sample(Nobs, l)' for l in 1:context.L]...) 
    Xobs_alt = Models.alternative_solution(context, Xobs)
    Yobs = F(context, Xobs)

    cts = []
    measures::Vector{MesureOutput} = []
    for gllim_params in variants 
        mat_preds, _,  ct = train_inverse(run, context, Yobs, gllim_params, is_params)
        regularize_inversions!(mat_preds, [ Methods.GllimMergedCentroid(""), Methods.IsCentroid("")])
        
        # Plotting 
        configs = [
            # Methods.GllimMean => SerieConfig(N.GllimMean, :basic, "red"; alpha = 0.5),
            # Methods.IsMean => SerieConfig(N.IsMean, :dashed, "red"; alpha = 0.7),
            Methods.GllimMergedCentroid(1) => SerieConfig(N.GllimMergedCentroid1, :circle, "blue"),
            Methods.GllimMergedCentroid(2) => SerieConfig(N.GllimMergedCentroid2, :circle, "blue"),
            Methods.IsCentroid(1) => SerieConfig(N.IsCentroid1, :circle, "orange"),
            Methods.IsCentroid(2) => SerieConfig(N.IsCentroid2, :circle, "orange"),
        ]
        preds = format_inversions(mat_preds, configs)

        preds = [preds...,
            Prediction(Xobs; config = SerieConfig(N.Xalt1, :basic, "green"; alpha = 0.3)),
            Prediction(Xobs_alt; config = SerieConfig(N.Xalt2, :basic, "green"; alpha = 0.3)),
        ]

        path = Archive.get_path(Archive.FOLDER_GRAPHS, context, gllim_params, is_params; label =  "double_solutions_is_K$(gllim_params.K)")
        plot_components(context, preds;  savepath = path, varnames = context.variables_names, xlabel = L"n")

        # Errors stats (table)
        measure = basic_measure(context, Xobs, mat_preds)
        push!(measures, measure)
        push!(cts, ct)
    end

    options = TableOptions([
        # Methods.IsMean,
        Methods.GllimMergedCentroid(1),
        Methods.GllimMergedCentroid(2),
        Methods.IsCentroid(1),
        Methods.IsCentroid(2),
    ], true, true, true)
    latex_table = render_errors("double_solutions_is", measures, variants, options)
    path = Archive.get_path(Archive.FOLDER_GRAPHS, context, variants, Nobs, is_params; label = "double_solutions_is") * ".tex"
    write(path, latex_table)
    @info "Saved in $path"

    return cts
end


function hapke_synthetique(run, context::Models.LabHapkeContext, variants::Vector{Gllim.GLLiMTraining}, Nobs::Int, is_params::DataCovIs)
    Xobs =  vcat([ _wave_sample(Nobs, l)' for l in 1:context.L]...) 
    Yobs = F(context, Xobs)
    Xobs, _ = Models.to_physique(context, Xobs)

    cts = []
    measures::Vector{MesureOutput} = []
    for gllim_params in variants 
        mat_preds, _,  ct = train_inverse(run, context, Yobs, gllim_params, is_params)
        regularize_inversions!(mat_preds, [ Methods.GllimMergedCentroid(""), Methods.IsCentroid("")])
        
        # Plotting 
        configs = [
            Methods.GllimMean => SerieConfig(N.GllimMean, :basic, "red"; alpha = 0.5),
            Methods.IsMean => SerieConfig(N.IsMean, :circle, "red"; alpha = 0.5),
            Methods.Best => SerieConfig(N.Best, :circle, "blue"; alpha = 0.5)
        ]
        preds = format_inversions(mat_preds, configs)

        preds = [preds...,
            Prediction(Xobs; config = SerieConfig(N.Xobs, :basic, "green")),
        ]
        # plot for the article with Best 
        path = Archive.get_path(Archive.FOLDER_GRAPHS, context, gllim_params, is_params; label = "hapke_synthetique_K:$(gllim_params.K)")
        plot_components(context, preds;  savepath = path,xlabel = L"n", varnames = context.variables_names)

        # extra plot without Best 
        path = path * "_no_best"
        plot_components(context, preds[[1,2,4]];  savepath = path, varnames = context.variables_names, xlabel = L"n")

        # Errors stats (table)
        measure = basic_measure(context, Xobs, mat_preds)
        push!(measures, measure)
        push!(cts, ct)
    end

    options = TableOptions([
        Methods.GllimMean,
        Methods.IsMean,
        Methods.IsCentroid(1),
        Methods.IsCentroid(2),
        Methods.Best,
    ], false, false, false)
    latex_table = render_errors("hapke_synthetique", measures, variants, options)
    path = Archive.get_path(Archive.FOLDER_GRAPHS, context, variants, Nobs, is_params; label = "hapke_synthetique") * ".tex"
    write(path, latex_table)
    @info "Saved in $path"

    return cts
end

function _hapke_lab(run, context::Models.LabHapkeContext, gllim_params::MetaGLLiM, is_params, Yobs, wavelengths, Xmcmc, Covsmcmc)    
    
    # we use the gllim models learned during K selection 
    # in practice, it is more convenient and it should be sufficient to remember K and train again 
    # but to be thorough we use here exactly the same parameters values
    if run >= 1
        trained_gllim  = Archive.load(Archive.FOLDER_GLLIM, context, gllim_params, :K_selection)

        ti = Dates.now()
        mat_preds, entropies = complete_prediction(context, Yobs, trained_gllim, is_params, nothing)
        dura = since(ti)
        Archive.save([mat_preds, entropies, dura], Archive.FOLDER_PREDICTIONS, context, gllim_params, is_params, :K_selection)
    end
    mat_preds_selection, _, _ = Archive.load(Archive.FOLDER_PREDICTIONS, context, gllim_params, is_params, :K_selection)

    # we use the normal approach
    mat_preds, _,  ct = train_inverse(run, context, Yobs, gllim_params, is_params)

    _plot_hapke_lab(context, Yobs, wavelengths, gllim_params, is_params, Xmcmc, Covsmcmc, mat_preds_selection, true)
    _plot_hapke_lab(context, Yobs, wavelengths, gllim_params, is_params, Xmcmc, Covsmcmc, mat_preds, false)
    return ct
end 

function _plot_hapke_lab(context, Yobs, wavelengths, gllim_params, is_params, Xmcmc, Covsmcmc, mat_preds, is_from_K_selection)
    label = "hapke_lab_K$(gllim_params.K)"
    if is_params isa FixedCovIs
        label = label * "_floor" 
    end
    if is_from_K_selection 
        label *= "from_K_sel"
    end

    configs = [
        Methods.IsMean => SerieConfig(N.IsMean, :basic, "red"; hide_std = false),
        Methods.Best => SerieConfig(N.Best, :dashed, "blue"; hide_std = false)
    ]
    preds = format_inversions(mat_preds, configs)
   
    mcmcYerr = Y_relative_err(context, Yobs, Models.from_physique(context, Xmcmc), 1., zeros(context.D))
    refPred = Prediction(Xmcmc, Covsmcmc; config = SerieConfig("MCMC", :basic, "green"; hide_std = false), Yerr = mcmcYerr)
    
    # bestWithFilter = median_filter(preds[1], context, Yobs, SerieConfig("GLLiM IS Mean with filter", :dashed, "black"); width = 2)
    preds = [preds..., 
        refPred,
        # bestWithFilter, 
    ]

    path = Archive.get_path(Archive.FOLDER_GRAPHS, context, gllim_params, is_params; label = label)
    plot_components(context, preds;  savepath = path, varnames = context.variables_names, x_ticks = 1000 .* wavelengths, xlabel = "wavelength (nm)")

    # export results for further analysis (X in physical space)
    X_ismean = get_by_key(preds, Methods.IsMean)
    X_best = get_by_key(preds, Methods.Best)
    serialize(Archive.get_path(Archive.FOLDER_MISC, context, :tmp; label = label), 
        Dict(Methods.IsMean => X_ismean, Methods.Best => X_best))

    # second plots : observables 
    geom = Models._load_geometries(context)
    
    # we compute the reconstruction
    Yismean = F(context, Models.from_physique(context, X_ismean))
    Ybest = F(context, Models.from_physique(context, X_best))
    Ymcmc = F(context, Models.from_physique(context, Xmcmc))
    
    # obs_indexes = sortperm(mcmcYerr; rev = true)[1:3] #3 worst ref preds
    _, Nobs = size(Yobs)
    obs_indexes = [8, 44, 76] # expert value 
    subplots = []
    for (i, index) in enumerate(obs_indexes)
        wl = wavelengths[index] * 1000
        Yplot = hcat(Yobs[:,index], Ymcmc[:,index], Yismean[:,index], Ybest[:,index])
        noMathIs, noMathBest = N.noMath(N.IsMean), N.noMath(N.Best)
        labelIs, labelBest = "\$F($noMathIs)\$", "\$F($noMathBest)\$"
        labels = [N.Yobs * L" - $\lambda = " * string(wl) *  L" nm$",  L"F(\mathbf{x}_{MCMC})" ,labelIs, labelBest]
        titlefunc  = inc->"wavelength : $wl nm"
        p = plot_polar(Yplot, geom, labels, ["black", "green", "red", "blue"]; title = titlefunc)
        push!(subplots, p)
    end 
    obs_plot = plot(subplots..., layout = (1, length(obs_indexes)), size = (length(obs_indexes) * 500, 500))
    obs_path = path * "observables.png"
    png(obs_plot, obs_path)
    @debug "Saved in $obs_path"
end

function hapke_lab(run, context::Models.LabHapkeContext, gllim_paramss::Vector, is_params::DataCovIs)
    Yobs, wavelengths, _  = Models.load_observations(context)
    _, Nobs = size(Yobs)

    stds =  Yobs / is_params.std_ratio
    stds[ stds .< 0.01 ] .= 0.01 # floor at 0.01
    is_params_floor = FixedCovIs(is_params.train_std, stds, is_params.Ns)
    
    # we compute our own MCMC inversions
    mcmc_params = MCMCParams(is_params.train_std, is_params.std_ratio)  # withouh noise floor
    mcmc_params_floor = MCMCParams(is_params.train_std, stds)  # floor of 0.01
    
    ct_mcmc, ct_mcmc_floor = ComputationTime(context, mcmc_params, Nobs), ComputationTime(context, mcmc_params_floor, Nobs)
    if run >= 2
        invs_mcmc, dura = complete_prediction(context, Yobs, mcmc_params)    
        invs_mcmc_floor, dura_floor = complete_prediction(context, Yobs, mcmc_params_floor)    
        Archive.save([invs_mcmc, dura], Archive.FOLDER_PREDICTIONS, context, Yobs, mcmc_params)
        Archive.save([invs_mcmc_floor, dura_floor], Archive.FOLDER_PREDICTIONS, context, Yobs, mcmc_params_floor)
    end
    invs_mcmc, ct_mcmc.prediction = Archive.load(Archive.FOLDER_PREDICTIONS, context, Yobs, mcmc_params)
    invs_mcmc_floor, ct_mcmc_floor.prediction = Archive.load(Archive.FOLDER_PREDICTIONS, context, Yobs, mcmc_params_floor)

    Xmcmc, Covsmcmc = aggregate(invs_mcmc), aggregate_std(invs_mcmc).^2
    Xmcmc_floor, Covsmcmc_floor = aggregate(invs_mcmc_floor), aggregate_std(invs_mcmc_floor).^2

    # we only show computation times for floor version
    cts = [ct_mcmc_floor]
    for gllim_params in gllim_paramss
        # _hapke_lab(run, context, gllim_params, is_params, Yobs, wavelengths, Xmcmc, Covsmcmc)
        ct = _hapke_lab(run, context, gllim_params, is_params_floor, Yobs, wavelengths, Xmcmc_floor, Covsmcmc_floor)
        push!(cts, ct)
    end
    return cts
end

# sigma is an std, not a covariance
function noise_synthetique(run, context::AbstractContext, gllim_params::EMMetaGLLiM, Nobs::Int, sigma_target::Float64, sigma_init::Float64, max_iteration::Int)
    synthetic = Noise.Bias(1., 0. * ones(context.D), Diagonal(sigma_target^2 * ones(context.D)))
    init = Noise.Bias(1., 0. * ones(context.D), Diagonal(sigma_init^2 * ones(context.D)))

    params = EmIsGLLiMParams(D = context.D, targets = Noise.Targets(false, false, true), init = init, max_iteration = max_iteration, Ns = 20_000)

    Xobs, Yobs = Models.data_training(context, Nobs; noise_a = synthetic.a, noise_mean = synthetic.mu, noise_std = sigma_target)
    estimator = Noise.theorical_estimators(context, Xobs, Yobs)

    ct = ComputationTime(context, gllim_params, params, size(Yobs)[2])

    if run >= 2
        ti = Dates.now()
        perfect_gllim = inference(context, gllim_params)
        dura = since(ti)
        Archive.save([perfect_gllim, dura], Archive.FOLDER_GLLIM, context, gllim_params)
    end
    perfect_gllim, ct.learning = Archive.load(Archive.FOLDER_GLLIM, context, gllim_params)

    if run >= 1
        ti = Dates.now()
        history = Noise.run(params, Yobs, context, perfect_gllim)
        dura = since(ti)
        Archive.save([history, estimator, dura], Archive.FOLDER_NOISE, context, gllim_params, params, synthetic)
    end
    history, estimator, ct.prediction = Archive.load(Archive.FOLDER_NOISE, context, gllim_params, params, synthetic)

    path = Archive.get_path(Archive.FOLDER_GRAPHS, context, synthetic, init, params, gllim_params; label = "noise_synthetic")
    plot_noise_estimation(context, params, history, synthetic, estimator; savepath = path)

    # polar plot for the last iteration 
    geom = Models._load_geometries(context)
    stds = hcat(sqrt.(diag(synthetic.sigma)), sqrt.(diag(estimator.sigma)), sqrt.(diag(history[end][1].sigma)))
    plot_polar(stds, geom, [L"\sigma_D", L"\sigma^{est}_D", L"\sigma^{last}_D"], ["black", "red", "blue"]; savepath = path * "polar")
    return [ct]
end

function noise_reel(run, context::Models.HapkeContext, gllim_params::EMMetaGLLiM, sigma_init::Float64, max_iteration::Int)
    # we compare diagonal constraint with isotropic constraint
    init_diag = Noise.Bias(1., 0. * ones(context.D), Diagonal(sigma_init * ones(context.D)))
    init_iso = Noise.Bias(1., 0. * ones(context.D), UniformScaling(sigma_init))

    params_diag = EmIsGLLiMParams(D = context.D, targets = Noise.Targets(false, false, true), init = init_diag, max_iteration = max_iteration)
    params_iso = EmIsGLLiMParams(D = context.D, targets = Noise.Targets(false, false, true), init = init_iso, max_iteration = max_iteration)
    

    Yobs, _, Ystds  = Models.load_observations(context)
    synthetic, estimator = nothing, nothing # real data 

    ct_diag = ComputationTime(context, gllim_params, params_diag, size(Yobs)[2])
    ct_iso = ComputationTime(context, gllim_params, params_iso, size(Yobs)[2])

    if run >= 2
        ti = Dates.now()
        perfect_gllim = inference(context, gllim_params)
        dura = since(ti)
        Archive.save([perfect_gllim, dura], Archive.FOLDER_GLLIM, context, gllim_params)
    end
    perfect_gllim, dura = Archive.load(Archive.FOLDER_GLLIM, context, gllim_params)
    ct_diag.learning = dura
    ct_iso.learning = dura

    if run >= 1
        ti = Dates.now()
        history = Noise.run(params_diag, Yobs, context, perfect_gllim)
        dura = since(ti)
        Archive.save([history, synthetic, estimator, dura], Archive.FOLDER_NOISE, context, gllim_params, params_diag)
        ti = Dates.now()
        history = Noise.run(params_iso, Yobs, context, perfect_gllim)
        dura = since(ti)
        Archive.save([history, synthetic, estimator, dura], Archive.FOLDER_NOISE, context, gllim_params, params_iso)
    end
    history_diag, _, _,  ct_diag.prediction  = Archive.load(Archive.FOLDER_NOISE, context, gllim_params, params_diag)
    history_iso, _, _, ct_iso.prediction  = Archive.load(Archive.FOLDER_NOISE, context, gllim_params, params_iso)

    # final estimation
    std_est_diag = sqrt.(diag(history_diag[end][1].sigma))
    std_est_iso = sqrt.(history_iso[end][1].sigma * ones(context.D)) # converted as vector

    # comparison with the obs mean noise 
    std_obs = Statistics.mean(Ystds, dims = 2)[:, 1]

    geom = Matrix(Models._load_geometries(context)')

    # polar plot for easy visualisation
    path = Archive.get_path(Archive.FOLDER_GRAPHS, context, synthetic, params_diag, gllim_params; label = "noise_reel_$(context.LABEL)") 

    stds = hcat(std_obs, std_est_diag, std_est_iso)
    labels = [notation_obs_std, notation_bias_std * " - Diagonal",  notation_bias_std * " - Isotropic"]
    colors = ["black", "red", "orange"]

    # we dont plot the diagonal constraint
    stds, labels, colors = stds[:, [1,3]], labels[[1,3]], colors[[1,3]]

    titlefunc = inc->context.LABEL
    plot_polar(stds, Matrix(geom'), labels, colors; savepath = path * "polar", title = titlefunc)
    # we only expose iso constraint 
    return [ct_iso]
end

function hapke_massive(run, context::Models.GlaceContext, gllim_params::Gllim.GLLiMTraining, Nis::Int)
    Yobs_cube, _, Ystd_cube, mask = Models.load_observations(context, :S)

    ct = inverse_export_cube(run, context, gllim_params, Nis, Yobs_cube, Ystd_cube, mask; label = "glace")
    return [ct]
end

function exploit_K_selection()
    # to be kept in sync with cluster script 
    Ks = collect(range(5, 100; step = 5))
    context = Models.NontroniteHapkeContext()
    Log.indent_log()
    for Ystd in [0.1, 0.01, 0.001]
        # train_select_K(context, Ks, 0.1) is executed on the cluster
        
        res::KSelection, ti = Archive.load(Archive.FOLDER_PREDICTIONS, context, Ks, Ystd)
        @debug "K selection time : $(Dates.canonicalize(Dates.CompoundPeriod(ti)))"
    
        path = Archive.get_path(Archive.FOLDER_GRAPHS, context, Ks, Ystd; label = "$(context.LABEL)_$Ystd")
        index_bic, index_slope = plot_k_selection(Ks, res, Ystd, context.D; savepath = path)
        Kbic, Kslope = Ks[index_bic], Ks[index_slope]

        # we save the learned gllim to ensure coherence with K selection, with a special tag
        params_bic = Gllim.GLLiMTraining(Gllim.MultiInit(), Kbic; Ntrain = 100_000, Ystd = Ystd) # best K 
        params_slope = Gllim.GLLiMTraining(Gllim.MultiInit(), Kslope; Ntrain = 100_000, Ystd = Ystd) # slope heuristic
        Log.indent_log()
        Archive.save(res.models[index_bic],  Archive.FOLDER_GLLIM, context, params_bic, :K_selection)
        Archive.save(res.models[index_slope],  Archive.FOLDER_GLLIM, context, params_slope, :K_selection)
        Log.deindent_log()
        @info "K selection : Gllim models for K = $Kbic and K = $Kslope saved."
    end
    Log.deindent_log()
end