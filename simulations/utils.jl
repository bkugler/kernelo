# Generate a description of all the parameters
# used in the simulations

using LaTeXStrings
using Serialization
using Formatting

include("jobs.jl")

abstract type Donnees end 

struct Synthetiques <: Donnees
    # bruit::Float64 # std
end

struct Reelles <: Donnees
end

struct Experience 
    titre::String 
    but::String
    description::String 
    donnees::Donnees
    context::Models.AbstractContext
    job::Function # first two args are `run`and `context`
    params::Vector # args of `job` (starting after `context`) 
end

description_donnees(::Synthetiques) = "{\\em  synthetic data}"
description_donnees(::Reelles) = "{\\em  real data}"

# Latex pretty printing

# fallback 

function Base.show(io, ::MIME"text/latex", v)
    show(io, v)
end

function Base.show(io, ::MIME"text/latex", v::MesureParams)
    print(io, "\$N_{test} = $(v.N_test)\$ - \$\\sigma_{test} = $(v.std_gen)\$ - \$\\sigma_{obs} = $(v.std_test)\$")
end


function Base.show(io, ::MIME"text/latex", v::Gllim.MultiInit)
    print(io, "multiple - $(v.starts) starts - $(v.max_iteration_gllim) iter. - init. with GMM (cov.precision: $(v.x_precision))")
end

function Base.show(io, ::MIME"text/latex", v::Gllim.GLLiMTraining)
    init = repr("text/latex", v.init)
    s =  """
   GLLiM with EM - details :
    \\begin{description}
        \\item[init.] $(init)
        \\item[K] $(v.K)
        \\item[training set] size $(v.Ntrain), std $(v.Ystd)
        \\item[stop] max iterations $(v.max_iteration) ratio $(v.stopping_ratio)
    \\end{description}
    """
    print(io, s)
end

function Base.show(io, ::MIME"text/latex", v::Vector{T}) where T <: Gllim.EMMetaGLLiM
    items = join(["     \\item " * repr("text/latex", arg) for arg in v], "")
    s = """ \\
    \\begin{enumerate}
        $items
    \\end{enumerate}
    """
    print(io, s)
end

function Base.show(io, ::MIME"text/latex", v::DataCovIs)
    print(io, "$(v.Ns) IS samples - \$\\Sigma_{train} = $(v.train_std^2)\$ - \$\\Sigma_{obs} = diag(F(x) / $(v.std_ratio))^2\$")
end

function description_params(params::Vector)
    fprint = x->repr("text/latex", x)
    items = join([ "\\item[$name] $(fprint(arg))" for (name, arg) in params],
        "\n")
    """
    \\begin{description}
        $items
    \\end{description}
    """
    # replace(s, "\n"=> "\\\\")
end

function genere_experience(i::Int, exp::Experience, hideNotes::Bool) 
    donnees = description_donnees(exp.donnees)
    params = description_params(exp.params)
    notes = """ \\\\
    {\\em Objectif } : $(exp.but) \\\\
    {\\em Description } : $(exp.description) 
    """
    
    if hideNotes
        notes = "" 
    end

    return """
    {\\footnotesize
    \\paragraph{$i - $(exp.titre)}
    $donnees - $(exp.context.LABEL) (\$L = $(exp.context.L) ,  D = $(exp.context.D)\$)
    $notes
    }
    {\\scriptsize
        $params
    }
    """
end

function generate_exps_parameters(exps::Vector{Experience}, hideNotes::Bool,  out::String) 
    s =  join([genere_experience(i, exp, hideNotes) for (i, exp) in enumerate(exps)])
    write(out, s)
end

function _fmt_time(time::Millisecond) 
    dt = DateTime(Dates.UTM(time))
    if time < Second(1)
        return Dates.format(round(dt, Millisecond(10)), "S.s\\s")
    elseif time < Minute(1)
        return Dates.format(round(dt, Millisecond(100)), "S.s\\s")
    elseif time < Minute(10)
        return Dates.format(dt, " M\\m S\\s")
    elseif time < Hour(1)
        return Dates.format(round(dt, Minute(1)), " M\\m")
    else
        return Dates.format(dt, "H\\h M\\m")
    end
end

function _fmt_int(N::Int)
    power = log10(N)
    if N > 100 && floor(power) == power 
        return "\$10^{$(Int(power))}\$"
    end
    if N > 1000 && N % (10^floor(power)) == 0 
        a = Int(N ÷ 10^floor(power))
        return "\$ $a \\, 10^{$(Int(floor(power)))}\$"
    end

    if N <= 10 
        return "-" # non significant 
    end 
    # latex thin space
    return replace(Formatting.format(N, commas = true), "," => "\\,")
end

function genere_time(c::ComputationTime)
    fmt_learning = _fmt_time(c.learning)
    fmt_prediction = _fmt_time(c.prediction)
    fmt_gllim_prediction = _fmt_time(c.gllim_only_prediction)
    fmt_prediction_relative = " - "  * _fmt_time(c.prediction ÷ c.Nobs)
    if c.kind == ComputationNoise 
        fmt_prediction_relative = ""
        fmt_gllim_prediction = "-"
    end
    if c.kind == ComputationMCMC 
        fmt_learning = "-"
        fmt_gllim_prediction = "-"
    end

    fmt_Nobs = _fmt_int(c.Nobs) 
    fmt_Nis = _fmt_int(c.Nis) 
    fmt_Ntrain = _fmt_int(c.Ntrain) 
    fmt_K = _fmt_int(c.K) 
    
    if c.kind == ComputationMCMC && fmt_Nis != "-"
        fmt_Nis *= L"\, ^*"
    end

    return """
    {\\footnotesize $(c.label)} & $(c.L) & $(c.D) &  $fmt_K & $fmt_Ntrain & $fmt_Nis & $fmt_Nobs & $fmt_learning & $fmt_gllim_prediction & $fmt_prediction $fmt_prediction_relative 
    """
end
function generate_times(cts::Vector{ComputationTime}, out::String)
    cts = [ ComputationTime(([getfield(x, k) for k in fieldnames(ComputationTime)]...)) for x in cts] # deep copy
    def = " m{3.5cm} || c | c | c | c | c | c || r | r | r"
    header = L"Experiment & $L$ & $D$ &  $K$ & $N$ &  $I$ & $N_{obs}$ & Learn. & " * N.GllimMean * " & Pred. - 1 obs. "

    # post-processing : short repeated exp + exp numbers 
    last_label = ""
    for ct in cts
        if ct.kind == ComputationNoise
            continue    
        end
        if ct.label == last_label # repeated exp, dont increment
            label = "-"
        else 
            label = "Ex. $(ct.exp_index) : " * ct.label
        end
        last_label = ct.label # save initial label
        ct.label = label # update label in place
    end
    # variant for noise
    last_label = ""
    for ct in cts
        if ct.kind !== ComputationNoise
            continue    
        end
        if ct.label == last_label # repeated exp
            label = "-"
        else 
            label = ct.label
        end
        last_label = ct.label # save initial label
        ct.label = label # update label in place
    end
    rows1 = join([genere_time(ct) for ct in cts if ct.kind != ComputationNoise ], "\\\\ \n")
    rows2 = join([genere_time(ct) for ct in cts if ct.kind == ComputationNoise ], "\\\\ \n")

    latex = """
    \\begin{table}[h!]
    \\centering
    \\begin{tabular}{$def}
    $header \\\\
    \\hline
    $rows1 \\\\
    \\hline 
    $rows2
    \\end{tabular}
    \\caption{ \\label{tab:comp_times} Settings and computation times for synthetic and real data experiments.  The column $(N.GllimMean) displays the time spend only for the computation of $(N.GllimMean) for all the observations, while the last column takes into account all the prediction schemes. \$^*\$ For MCMC, the number of simulations done for each observation is reported under \$I\$.}
    \\end{table} 
    """
    
    write(out, latex)
end

