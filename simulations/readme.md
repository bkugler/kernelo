## Simulations for the article *FAST BAYESIAN INVERSION FOR HIGH DIMENSIONAL INVERSE PROBLEMS*

The simulations are defined in [main.jl](main.jl), with their hyper-parameters. The actual workload is done in [jobs.jl](jobs.jl), which utilises the whole package. 

[notations.jl](notations.jl),[latex_tables.jl](latex_tables.jl) and [utils.jl](utils.jl) defines helpers functions used to automatically generate LaTeX tables of notations and computation times.