using Kernelo
using Kernelo.Models
using Kernelo.Archive
using Colors
using Plots

const ARTICLE_DIR = "/Users/kuglerb/Documents/docs/latex/article_GLLiM_IS_v1/"

include("utils.jl")

experiences = [
    Experience("Simple double solutions problem",
    "Illustrer graphiquement l'apport de la prédiction par les centroids $(N.GllimMergedCentroid1) et $(N.GllimMergedCentroid2) par rapport à la moyenne.",
    "Inversion d'une fonction à deux solutions, simple.", 
    Synthetiques(),
    Models.DoubleSolutionContext(1), 
    double_solutions_simple,
    [
      ("GLLiM settings",   Gllim.GLLiMTraining(Gllim.MultiInit(), 40; Ystd = 0.01)),
      ("Number of observations ",  100),
      ("IS settings",   DataCovIs(0.01, 1000, 10000))
    ]),

    Experience("Centroids for double solutions", 
    "Cas de solutions multiples : $(N.GllimMean) n'est assez performant. $(N.GllimMergedCentroid1) et $(N.GllimMergedCentroid2) sont nécessaires, et peuvent être améliorés par IS.", 
    "Inversion d'une fonctionnelle synthétique construite pour avoir exactement 2 solutions pour chaque observation.", 
    Synthetiques(),
    Models.QuadraticDoubleContext(),
    double_solutions_complete,
    [
        ("GLLiM variants", [
            Gllim.GLLiMTraining(Gllim.MultiInit(), 30),
            Gllim.GLLiMTraining(Gllim.MultiInit(), 50),
            Gllim.GLLiMTraining(Gllim.MultiInit(), 70),
            ]),
        ("Number of observations ",  1000),
        ("IS settings",   DataCovIs(0.001, 1000, 10000))
    ]),

    Experience("Hapke - Synthetic data", 
    "Comportement du modèle cible : mise en évidence des occasionnelles solutions multiples.", 
    "Inversion du modèle de Hapke (géometries de la série Olivine)", 
    Synthetiques(), 
    Models.NontroniteHapkeContext(),
    hapke_synthetique,
    [
      ("GLLiM variants", [
        Gllim.GLLiMTraining(Gllim.MultiInit(), 40; Ystd = 0.0001),
        Gllim.GLLiMTraining(Gllim.MultiInit(), 70; Ystd = 0.0001),
        Gllim.GLLiMTraining(Gllim.MultiInit(), 100; Ystd = 0.0001),
      ]),
      ("Number of observations",      1000),
      ("IS settings",      DataCovIs(0.0001, 1000, 50000)),
    ],
    ),
    
    Experience("Hapke - Laboratory observations",
    "Comparaison avec la référence MCMC",
    """Inversion des données réelles (série Basalt ou une autres des 5) et comparaison avec MCMC. Plot des solutions X ($(N.IsMean) et $(N.Best)). Plot des observables les plus différentes.
    Note : selon Pilorget : "An uncertainty of 5\\%, that represents the uncertainty on the measurement is used in the inversion procedure."
    """,
    Reelles(),
    Models.NontroniteHapkeContext(),
     hapke_lab,
     [
        ("GLLiM variants", [
            # Gllim.GLLiMTraining(Gllim.MultiInit(), 20; Ntrain = 100_000), # slope heuristic
            Gllim.GLLiMTraining(Gllim.MultiInit(), 100; Ntrain = 100_000), # best K 
        ]),
        ("IS settings", DataCovIs(0.001, 20, 50000))
     ],
     ),

     Experience("Hapke - Glace observations",
     "Inverson massive, impossible à faire en MCMC", 
     """Jeu de données le plus complet, avec dépendance spatiale et spectrale. Plot en fausse couleur : RGB(x1, x2, x3), où xi est la prédiction (normalisée) à la longueur d'onde i.
 
    On utilise l'écart-type fourni avec chaque observation.
    """, 
    Reelles(),
    Models.GlaceContext(),
    hapke_massive,
    [
       ("GLLiM settings", Gllim.GLLiMTraining(Gllim.MultiInit(), 50; Ystd = 0.001)),
       ("Number of IS samples", 20000)
    ],
    ),

   Experience("Noise estimation on synthetic data - \$ \\sigma = 0.2 \$", 
   "Montrer qu'on peut retrouver \$\\Sigma\$ sur données synthétiques", 
   "Estimation du bruit via EM-IS-GLLIM (sur le modèle de Hapke). Plot de la convergence.
   ", 
   Synthetiques(), 
   Models.NontroniteHapkeContext(),
   noise_synthetique,
   [
       ("GLLiM settings",    Gllim.GLLiMTraining(Gllim.MultiInit(), 50; Ystd = 0.0001)),
       ("Number of observations",    2000),
       (L"$\sigma$",    0.2),
       (L"$\sigma_{init}$",    0.5),
       ("Number of iterations",    25)
   ],
   ),

   Experience("Noise estimation on synthetic data - \$ \\sigma = 0.03 \$", 
   "Montrer qu'on peut retrouver \$\\Sigma\$ sur données synthétiques", 
   "Estimation du bruit via EM-IS-GLLIM (sur le modèle de Hapke). Plot de la convergence.
   ", 
   Synthetiques(), 
   Models.NontroniteHapkeContext(),
   noise_synthetique,
   [
       ("GLLiM settings",    Gllim.GLLiMTraining(Gllim.MultiInit(), 50; Ystd = 0.0001)),
       ("Number of observations",    2000),
       (L"$\sigma$",    0.03),
       (L"$\sigma_{init}$",    0.5),
       ("Number of iterations",    25)
   ],
   ),
    
    # Experience("Hapke - Noise estimation", 
    # "Comparer l'estimation du biais au bruit donné par l'expert",
    #  """Estimation d'un biais systématique sur un jeu d'observations faiblement bruitées.
    # D'un côté, l'algorithme proposé estime la somme du biais et du bruit moyen des observations. De l'autre, on dispose d'une référence de l'expert donnant le bruit moyen sur les observations (série Mukundpura). La comparaison qualitative des deux quantités donne une indication sur la pertinence du modèle physique.
    # """, 
    # Reelles(), 
    # Models.MukundpuraPoudreContext(),
    # noise_reel,
    # [
    #  ("GLLiM settings",   Gllim.GLLiMTraining(Gllim.MultiInit(), 50)),
    #  (L"$\sigma_{init}$",   1.),
    #  ("Number of iterations",   30)
    # ],
    # ),

    Experience("Noise estimation on Nontronite", 
    "Comparer l'estimation du biais au bruit donné par l'expert",
     """Idem. Jeu de données différent.""", 
    Reelles(), 
    Models.NontroniteHapkeContext(),
    noise_reel,
    [
     ("GLLiM settings",   Gllim.GLLiMTraining(Gllim.MultiInit(), 50)),
     (L"$\sigma_{init}$",   1.),
     ("Number of iterations",   30)
    ],
    ),

    Experience("Noise estimation on Basalt", 
    "Comparer l'estimation du biais au bruit donné par l'expert",
     """Idem. Jeu de données différent.""", 
    Reelles(), 
    Models.BasaltHapkeContext(),
    noise_reel,
    [
     ("GLLiM settings",   Gllim.GLLiMTraining(Gllim.MultiInit(), 50)),
     (L"$\sigma_{init}$",   1.),
     ("Number of iterations",   30)
    ],
    ),

    Experience("Noise estimation on Olivine", 
    "Comparer l'estimation du biais au bruit donné par l'expert",
     """Idem. Jeu de données différent.""", 
    Reelles(), 
    Models.OlivineHapkeContext(),
    noise_reel,
    [
     ("GLLiM settings",   Gllim.GLLiMTraining(Gllim.MultiInit(), 50)),
     (L"$\sigma_{init}$",   1.),
     ("Number of iterations",   30)
    ],
    )
]

function run_simulations(runs::Vector)
    cts::Vector{ComputationTime} = []
    for (i, (run, exp)) in enumerate(zip(runs, experiences))
        if run <= -1
            @info "Skipping exp. $i "
            continue
        end
        @info "Starting exp. $i - $(exp.titre)"
        Log.indent_log()
        params = [p for (name, p) in exp.params]
        current_cts = exp.job(run, exp.context, params...)
        Log.deindent_log()
        for c in current_cts
            c.exp_index = i
            c.label = exp.titre
            if c.kind == ComputationMCMC 
                c.label *= " - MCMC"
            end    
        end
        push!(cts, current_cts...)
    end
    cts
end

Plots.pyplot()
Config.LEGEND_BACKGROUND = Colors.RGBA(0.9, 0.9, 0.9, 0.7)
Config.DPI = 200
Config.CIRCLE_SIZE = 3

# Uncomment to launch the simulations :
# 2  : full simulation 
# 1  : re-use existing trained model 
# 0  : re-use trained model and prediction (only plot)
# -1 : skip the experience

# cts = run_simulations([2,2,2,2,2,2,2,2,2,2])
# cts = run_simulations([1,1,1,1,1,1,1,1,1,1])
cts = run_simulations([0,0,0,0,0,0,0,0,0,0])


# Uncomment to write meta-data 

# generate_exps_parameters(experiences, true, ARTICLE_DIR * "simulations/exps.tex")
generate_times(cts, ARTICLE_DIR * "simulations/times.tex")
# N.render_def_notations(ARTICLE_DIR * "preamble_notations.tex")
# N.render_table_notations(ARTICLE_DIR * "notations.tex")