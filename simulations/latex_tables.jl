using Kernelo.Gllim
using Kernelo.Models

include("notations.jl")

const notation_obs_std = L"\mathbf{\sigma_D^{obs}}"
const notation_bias_std = L"\mathbf{\sigma_D^{last}}"

struct MesureParams 
    N_test::Int 
    std_test::Float64
    std_gen::Float64
end

""" Summary statistics for a serie of errors """
struct St 
    mean::Float64
    std::Float64
end

""" Group X prediction and Y reconstruction error """
struct BasicError 
    X::St 
    Y::St 
end

""" Store X errors for couple of double solutions.
Y error is already computed in "basic" errors.
"""
struct DoubleSolutionError 
    Pred1::St 
    Pred2::St
end

struct MesureOutput 
    errors::Dict{String, BasicError}
    double::Union{DoubleSolutionError,Nothing}
    doubleIS::Union{DoubleSolutionError,Nothing}
end

# --------------------------------- Latex tables ---------------------------------


_r(m) = round(m; digits = 4)
_r4(m) = round(m; digits= 4)

struct TableOptions 
    methods::Vector{String}
    show_double::Bool
    show_doubleIS::Bool
    hide_X::Bool # methods only
end

struct Label 
    label::String
    kind::String # 'X' or 'Y'
end



""" aggregate the results to show """ 
function _select_errors(measures::Vector{MesureOutput}, options::TableOptions)
    rows = []
    labels::Vector{Label} = [] # we be display in column 
    # show basic errors, grouped by X or Y
    if !options.hide_X 
        for meth in options.methods 
            notation = N.noMath(N.fromMethods[meth])
            labelX = Label("\t \$\\bar{E}($notation)\$", "X")
            # columns
            rowX = [measure.errors[meth].X  for measure in measures]
            push!(rows, rowX)
            push!(labels, labelX)
        end
    end

    for meth in options.methods 
        notation = N.noMath(N.fromMethods[meth])
        labelY = Label("\t \$\\bar{R}($notation)\$", "Y")
        # columns
        rowY = [measure.errors[meth].Y  for measure in measures]
        push!(rows, rowY)
        push!(labels, labelY)
    end

    # optionnal double solutions 
    if options.show_double
        labelX1 = Label("\t $(N.GllimMergedCentroid1)", "X")
        labelX2 = Label("\t $(N.GllimMergedCentroid2)", "X")
        # columns
        rowX1 = [measure.double.Pred1  for measure in measures]
        rowX2 = [measure.double.Pred2  for measure in measures]
        push!(rows, rowX1, rowX2)
        push!(labels, labelX1, labelX2)
    end
    
    # optionnal double solutions with IS
    if options.show_doubleIS
        labelX1 =Label("\t $(N.IsCentroid1)", "X")
        labelX2 =Label("\t $(N.IsCentroid2)", "X")
        # columns
        rowX1 = [measure.doubleIS.Pred1  for measure in measures]
        rowX2 = [measure.doubleIS.Pred2  for measure in measures]
        push!(rows, rowX1, rowX2)
        push!(labels, labelX1, labelX2)
    end

    mat = permutedims(hcat(rows...)) # display order
    @assert size(mat) == (length(labels), length(measures))
    return mat, labels 
end

""" returns an array of pairs of index (maybe -1) """ 
function _find_bests(mat::Matrix{St}, labels::Vector{Label})
    out::Vector{Pair{Int, Int}} = []

    function findMinByKind(col, kind::String)
        indexes = [ i for (i, label) in enumerate(labels) if label.kind ==  kind ] 
        errs = [ st.mean for (st, label) in zip(col, labels) if label.kind ==  kind ] 
        indexMin = - 1
        if length(errs) > 0 
            _, tmpI = findmin(errs) # local index
            indexMin = indexes[tmpI] # global index
        end
        return indexMin
    end

    for measure in eachcol(mat)
        # errors X 
        indexMinX = findMinByKind(measure, "X")
        # errors Y 
        indexMinY = findMinByKind(measure, "Y")
        push!(out, indexMinX => indexMinY)
    end
    return out
end

function _b(val, isBold::Bool)
    if isBold
        return "\\textbf{$val}"
    end
    return val
end

""" For each param (column), show the errors (row) """ 
function render_errors(label::String, measures::Vector{MesureOutput}, params::Vector, options::TableOptions)
    def =" r | " * join(("c" for _ in params), "|")
    header = "Prediction schemes & " * join(("K = $(gllim_params.K)" for gllim_params in params), " & ")

    mat, labels = _select_errors(measures, options) # pre-process 
    bests = _find_bests(mat, labels)
    I, _ = size(mat)
    rows = []
    for i in 1:I
        row = labels[i].label
        # columns
        for (j, best) in enumerate(bests)
            isBest = best.first == i || best.second == i
            st = mat[i, j] 
            row *= "& $(_b(_r(st.mean), isBest)) ($(_r(st.std)))"
        end
        push!(rows, row)
    end

    rows = join(rows, "\\\\ \n")
    """
    \\begin{table}[h!]
    \\centering
    \\begin{tabular}{$def}
    $header \\\\
    \\hline
    $(rows) 
    \\end{tabular}
    \\caption{ \\label{tab:$label}  Standard deviations are in parenthesis and best averages are in bold. }
    \\end{table} 
    """
end



# noise comparison 

 _label_geometrie(g::Vector) = "$(Int(g[1])),$(Int(g[2])),$(Int(g[3]))"

_tabular_def(geometries::Matrix) = " l | " * join(("c" for _ in geometries[1,:]), "|")
_tabular_header(geometries::Matrix) = " & " * join((_label_geometrie(geometries[:, d]) for d in 1:size(geometries)[2]), " & ")

function render_noise(context::AbstractContext, Ystd_reff::Vector, Ystd_est::Vector, geometries::Matrix) 
    def = _tabular_def(geometries)
    header = _tabular_header(geometries)
    row_ref = "\t $(notation_obs_std) & " * join(("$(_r4(s))" for s in Ystd_reff), " & ")
    row_est = "\t $(notation_bias_std) & " * join(("$(_r4(s))" for s in Ystd_est), " & ")
    rows = join([row_ref, row_est], "\\\\ \n")

    """
    \\begin{table}[h!]
    \\centering
    \\caption{ \\label{} $(context.LABEL) - Biais estimation}
        \\begin{tabular}{$def}
            $header \\\\
            \\hline
            $rows 
        \\end{tabular}
    \\end{table} 
    """
end

# render_noise(Models.MukundpuraBlocContext(), rand(30), rand(30), rand(3, 30) .* 90)

