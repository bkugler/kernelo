using LinearAlgebra
using JSON 

using Kernelo
using Kernelo.Models
using Kernelo.Probas
using Kernelo.Noise 
using Kernelo.ModeFinding
using Kernelo.Gllim
using Kernelo.Sobol
using Kernelo.Perf 
using Kernelo.Archive

function test_density()
    g = Gllim.randGLLiM(20, 4, 30)
    x = rand(4)
    Gllim.conditionnal_density(g, x)


    X = rand(4, 1000)
    w1, m1 = Gllim.conditionnal_density(g, X)
    @time Gllim.conditionnal_density(g, X)
end


function test_sampling() 
    L, K, N, Ns = 10, 40, 100, 1100
    weights = rand(K, N)
    meanss = rand(L, K, N)
    covs = FullCovs{Float64}(K, L)

    Probas.GMM_sampling(weights, meanss, covs, Ns)
    @time Probas.GMM_sampling(weights, meanss, covs, Ns)
end


struct _dummyModel <: Is.IsModel
    p::Is.IsProposition
    A::Matrix 
    y::Vector
end 
@inline Is.get_proposition(m::_dummyModel)::Is.IsProposition = m.p
@inline function Is.get_L(m::_dummyModel)
    _, L = size(m.A)
    L
end
Is.log_p(m::_dummyModel, x::VecOrSub, j::Int) = - 0.5 * norm(m.A * x - m.y)

function test_importance_sampling()
    T = Float64
    L, D, Ns = 4, 70, 50000
    K = 5
    mean, cov = rand(L), FullCov{Float64}(L)
    # p = StudentRegularizedProposition(mean, cholesky(cov).L, 1.2)
    # p = Is.GaussianRegularizedProposition(mean, cholesky(cov).L)
    p = Is.GaussianMixtureProposition{T}(rand(K), rand(L, K), [Probas.CholCov{T}(L) for i in 1:K])
    A = rand(D, L)
    y = rand(D)
    m = _dummyModel(p, A, y)
    Is.importance_sampling(m, Ns)
    @time Is.importance_sampling(m, Ns);
end

function test_hapke()
    path = "/Users/kuglerb/Documents/planeto-gllim-is/DEV_TOOLS/TEST_HAPKE/test_hapke.json"
    d = JSON.parsefile(path)

    sza = Vector{Float64}(d["inc"])
    vza = Vector{Float64}(d["eme"])
    dphi = Vector{Float64}(d["phi"])
    w = Vector{Float64}(d["omega"])
    r = Vector{Float64}(d["theta0"])
    b = Vector{Float64}(d["b"])
    c = Vector{Float64}(d["c"])
    hh = Vector{Float64}(d["hh"])
    b0 = Vector{Float64}(d["b0"])

    Y = hcat([y for y in d["y"]]...)
    D = length(sza)
    N = length(w)
    out = zeros(D, N)
    Models._Hapke!(sza, vza, dphi, w, r, b, c, hh, b0, out)

    @assert isapprox(Y, out)
end

function test_context()
    Archive.PATHS.ROOT_PATH_DATA = "/home/bkugler/Documents/DATA/"

    for c in [
        # Models.OlivineHapkeContext(),
        # Models.SeismeContext(),
        # Models.CrismHapkeContext(),
        # Models.LinearContext(),
        # Models.ExpContext(4),
        # Models.CercleContext(),
        # Models.InjectifHard(),
        # Models.GlaceContext()
        ]
        D = c.D
        X = rand(c.L, 10000)
        F(c, X)
        @time F(c, X)
    end


    c = Models.GlaceContext()
    obs, wl, stds, mask = Models.load_observations(c, :S)
    obs, wl, stds, mask = Models.load_observations(c, :L)
    N, D, W = size(obs)
    @assert size(wl) == (W,)
    @assert size(stds) == size(obs)
    @assert N > 0
end


function test_load_clean()
    c = Models.CrismHapkeContext()
    Yobs, Ystd, Xres, Xstd, latlong = Models.load_clean(c)
    @assert size(Yobs) == size(Ystd)
end

function test_noise_em()
    model = Models.QuadraticContext()
    L, D = model.L, model.D
    K, Ns, Nobs = 50, 10, 10
    gllim_chol_covs = [ CholCov{Float64}(L) for k in 1:K ]
    weightss = rand(K, Nobs)
    weightss ./= sum(weightss, dims = 1)
    meanss = rand(L, K, Nobs)
    gmms = [Gmm{Float64}(weightss[:,i], meanss[:,:,i], gllim_chol_covs)  for i in 1:Nobs ]

    Xobs, Yobs = Models.data_training(model, Nobs)

    ws = rand(Ns, Nobs)
    Xs = rand(L, Ns, Nobs)
    FXs = rand(D, Ns, Nobs)

    estimator = Noise.theorical_estimators(model, Xobs, Yobs)

    current_a = 1.
    current_mean = rand(D)
    current_cov = Diagonal(rand(D) .+ 2)
    bias = Noise.Bias(current_a, current_mean, current_cov)

    Noise.e_step(Yobs, Xs, model, gmms, bias)
    @time Noise.e_step(Yobs, Xs, model, gmms, bias)

    params = EmIsGLLiMParams(init = bias, max_iteration = 3, Ns = Ns)
    perfect_gllim = Gllim.randGLLiM(K, L, D)
    history = Noise.run(params, Yobs,  model, perfect_gllim)
    Plotting.plot_noise_estimation(model, params, history, nothing, estimator)
end

function test_noise_gradient()
    Nobs = 100
    # Xtrain = rand(10, 10000) .+ 1
    Ytrain = rand(10, 100000) .+ 2
    Xobs = rand(10, Nobs) .+ 1
    Yobs = 0.9 * rand(10, Nobs) .+ 2
    p = GradientParams(max_iteration = 30, fixed_a = 0.9, beta = 0.5)

    x = zeros(10)
    a = 1.2
    dir = 20 * rand(11)

    # _best_dist(a, x, Ytrain, Yobs)
    # @time _best_dist(a, x, Ytrain, Yobs)

    # J(a, x, Ytrain, Yobs)
    # @time J(a, x, Ytrain, Yobs)

    # dJ(a, x, Ytrain, Yobs)
    # @time dJ(a, x, Ytrain, Yobs)

    # _sigma_estimator(a, x, Ytrain, Yobs)
    # @time _sigma_estimator(a, x, Ytrain, Yobs)

    # @code_warntype backtrackingLineSearch(p,a, x, dir, Ytrain, Yobs)
    # @time backtrackingLineSearch(p, a, x, dir, Ytrain, Yobs)

    Y_generator = (N)->rand(10, 100000)
    Noise.run(p, Yobs, Y_generator)
end


function test_mode_finding_f()
    L, K = 7, 60
    gmm = ModeFinding.Gmm{Float64}(rand(K), rand(L, K), [CholCov{Float64}(L) for k in 1:K])
    gmm.weights ./= sum(gmm.weights)
    tmp = ModeFinding.tmpStorage{Float64}(L)
    x = rand(L)
    out = zeros(L)
    ModeFinding.f_rec!(gmm, x, tmp, out)
    @time ModeFinding.f_rec!(gmm, x, tmp, out)
    # @assert  isapprox(x, gmm.means[:,1]) # special case only if K = 1
end

function test_search_modes()
    L, K = 4, 30
    gmm = ModeFinding.Gmm{Float64}(rand(K), rand(L, K), [CholCov{Float64}(L) for k in 1:K])
    gmm.weights ./= sum(gmm.weights)
    
    res = ModeFinding.search_modes(gmm, gmm.means)

    @show length([r for r in res if r.is_maximum])
    @time ModeFinding.search_modes(gmm, gmm.means)
end

function test_regularization()
    L, K, N = 4, 5, 100
    Xs = [ rand(L, N) for k in 1:K] 
    out = Pred.regularization(Xs)
    @assert length(out) == N
end

function test_sobol()
    L, D, K = 10, 70, 50
    gl = Gllim.randGLLiM(K, L, D)
    Sobol.indice_sobol_inverse(gl, 100)
    @time Sobol.indice_sobol_inverse(gl, 100)
    Sobol.indice_sobol_direct(Sobol.FunctionnalGLLiMInverse(gl), 10)
    @time Sobol.indice_sobol_direct(Sobol.FunctionnalGLLiMInverse(gl), 10)
end

function test_gllim_training()
    L, D, K, N = 6, 50, 50, 10000
    X = rand(L, N)
    Y = rand(D, N)
    gllim = Gllim.randGLLiM(K, L, D)
    init = Gllim.FixedInit()
    Gllim.train(gllim, X, Y, Gllim.GLLiMTraining(init, K; max_iteration = 2))
    
    gllim_diag = GLLiM(gllim.π, gllim.c, gllim.Γ, gllim.A, gllim.b, DiagCovs{Float64}(K, D))
    Gllim.train(gllim_diag, X, Y, Gllim.GLLiMTraining(init, K; max_iteration = 2))
end

function test_perf()
    model = Models.QuadraticContext()
    K = 50
    gllim = Gllim.randGLLiM(K, model.L, model.D)
    X, Y = Models.data_training(model, 10_000)
    
    Perf.direct_error(model, gllim)
    Perf.sigma_error(gllim)
    Perf.dimension(gllim)
    Perf.bic(gllim, X, Y)
end

function test_archive()
    Archive.PATHS.ROOT_PATH_ARCHIVE = "/Users/kuglerb/Documents/Work/"
    Archive.PATHS.ROOT_PATH_DATA = "/Users/kuglerb/Documents/docs/DATA/"

    Archive.get_path(Archive.FOLDER_GLLIM, "dsllmdksmd")
end 

function test_imis()
    include("../src/IS/imis_test.jl")
end

# test_density()
# test_sampling()
# test_importance_sampling()
# test_hapke()
# test_context()
# test_load_clean()
# test_noise_em()
# test_noise_gradient()
# test_mode_finding_f()
# test_search_modes()
# test_regularization()
# test_sobol()
# test_gllim_training()
# test_perf()
# test_archive()
# test_serialize()
test_imis()