## Function to read excel file and extract observations and geometries from mukundpura data.
## Should be run once to create mukundpura_bloc_poudre.ser, since the deserializing is much faster than reading the .xlsx file 

import XLSX
using Serialization 

const _BASE_PATH = "/Users/kuglerb/Documents/docs/DATA/HAPKE/"

reg = r"i(\d+)(?:e(-?\d+)(?:a(\d+))?|(?:a(\d+))?e(-?\d+))"

function read_xlsx(xf::XLSX.XLSXFile, sheet::String; endColOffset = 0) 
    sh = xf[sheet][:]
    header = sh[1,2:end - endColOffset]
    wavelengths = sh[2:end,1]
    wavelengths = float.(wavelengths)
    geometries = parse_geometries(header)
    data = sh[2:end, 2:end - endColOffset][:, 1:2:end]
    stds = sh[2:end, 2:end - endColOffset][:, 2:2:end]
    data = Matrix{Float64}(data')
    stds = Matrix{Float64}(stds')
    geometries, wavelengths, data, stds
end

function parse_geometries(header::Array) 
    G = length(header)
    @assert G % 2 == 0 "Les colonnes doivent être des couples réflectance - erreur "
    D = G ÷ 2
    out = zeros(D, 3)
    for i in 1:D
        col = 2 * i - 1
        s = replace(header[col], "_" => "") 
        m = match(reg, s)
        if m === nothing 
            error("Le format de la cellule $(col + 1) : $(s) est erroné.")
        else
            inc = parse(Float64, m.captures[1])
            if m.captures[2] == nothing 
                azi = m.captures[4]
                eme = parse(Float64, m.captures[5])
            else 
                eme = parse(Float64, m.captures[2])
                azi = m.captures[3]
            end
            azi = azi == nothing ? 0. : parse(Float64, azi) 
        end
        out[i,1] = inc 
        out[i,2], out[i, 3] = normalise(eme, azi)
    end
    out
end

function normalise(e, a) 
    if e >= 0
        return e, 180 - a 
    else 
        return -e, a
    end 
end

"""
Read `in` and serialize data in `out`.
"""
function extract(in::String, out::String, series::Vector{String}; endColOffset = 0) 
    data = Dict()
    XLSX.openxlsx(in; enable_cache = false) do xf
        for serie in series 
            data_serie = Dict()
            data_serie["geometries"], data_serie["wavelengths"], data_serie["observations"], data_serie["stds"] = read_xlsx(xf, serie; endColOffset = endColOffset)
            data[serie] = data_serie
        end
    end
    serialize(out, data)    
end

# uncomment to run the extraction (should only be executed once)
# extract(_BASE_PATH * "MUKUNDPURA/mukundpura_bloc_poudre.xlsx", _BASE_PATH * "MUKUNDPURA/mukundpura_bloc_poudre.ser", ["bloc", "poudre"])
# extract(_BASE_PATH * "BLACKY/data_Blacky.xlsx", _BASE_PATH * "BLACKY/blacky_bloc_poudre.ser", ["bloc", "poudre"])
# extract(_BASE_PATH * "data_howardite.xlsx", _BASE_PATH * "howardite.ser", ["data_howardite.txt"]; endColOffset = 1)