# Function to read SAV files and serialize them

using ReadSav
using Serialization

const PATH_DATA = "/Users/kuglerb/Documents/docs/DATA/HAPKE/resultats_photom_lab_pilorget_2016/lab_data/"
const PATH_RESULTS = "/Users/kuglerb/Documents/docs/DATA/HAPKE/resultats_photom_lab_pilorget_2016/result_photom/"

struct paths 
    origin::String 
    target::String
end

const path_exps = [
    paths(PATH_DATA * "data_olivine_olv_corr.sav", PATH_DATA * "Olivine.ser"),
    paths(PATH_DATA * "data_nontronite_ng1_corr.sav", PATH_DATA * "Nontronite.ser"),
    paths(PATH_DATA * "data_BAS_corr.sav", PATH_DATA * "Basalt.ser"),
    paths(PATH_DATA * "data_BasGl_corr.sav", PATH_DATA * "Glass.ser"),
    paths(PATH_DATA * "data_MGC_corr.sav", PATH_DATA * "Magnesite.ser"),
]

const path_results = [
    paths(PATH_RESULTS * "OLV_new_optim_100iter/optim_list.lis", PATH_RESULTS * "Olivine.ser"),
    paths(PATH_RESULTS * "NG1_new_optim_100iter/optim_list.lis", PATH_RESULTS * "Nontronite.ser"),
    paths(PATH_RESULTS * "BAS_new_optim_100iter/optim_list.lis", PATH_RESULTS * "Basalt.ser"),
    paths(PATH_RESULTS * "BasGl_new_optim_100iter/optim_list.lis", PATH_RESULTS * "Glass.ser"),
    paths(PATH_RESULTS * "MGC_new_optim_100iter/optim_list.lis", PATH_RESULTS * "Magnesite.ser"),
]

function extract_lab()
    for exp in path_exps
        # observations
        variables = readsav(exp.origin)
        serialize(exp.target, variables)
    end

    for exp in path_results
        res_path = exp.origin
        lines = readlines(res_path)
        base_res_path = dirname(res_path) * "/"
        N = length(lines)
        out = []
        for i in 1:N
            variables = readsav(base_res_path *  string(strip(lines[i])))  
            push!(out, variables)
        end
        serialize(exp.target, out)
    end
end

function extract_lab_alt() 
    dir = PATH_RESULTS * "Nontronite_alt"
    out = Dict()
    # with floor
    out[:WITH_FLOOR] = readsav(dir * "/data_nontronite_ng1_corr_allwave_threshold.sav")
    # without
    out[:WITHOUT_FLOOR] = readsav(dir * "/data_nontronite_ng1_corr_allwave.sav")
    serialize(dir * "/results.ser", out)
end

# uncomment to run the extraction (should only be executed once)
# extract_lab()
# extract_lab_alt()