# Function to read ENVY files and serializec them

using ArchGDAL
using MAT

# const PATH_DATA = "/Users/kuglerb/Documents/docs/DATA/"
const PATH_DATA = "/home/bkugler/Documents/DATA/"

const _path_140520 = PATH_DATA * "HAPKE/ar_IPAG_INRIA_140520"
const _exps_140520 = Dict(:FRT => "/Inv_FRT47A3_S_Wfix_0.56_rho_mod", :HRL => "/Inv_HRL40FF_S_Wfix_0.46_rho_mod")
function extract_140520()   # doesn't clean NAN
    out = Dict()
    for (exp, path) in _exps_140520
        data = ArchGDAL.read(_path_140520 * path)
        obs = ArchGDAL.read(data) # size ( D, N, W)
        out[exp] = Array{Float32,3}(obs)
    end
    path = _path_140520 * "/data.mat"
    matwrite(path, out; compress = true)
    @info "Serialized in $path"
end

const _path_B285 = PATH_DATA * "HAPKE/ar_FRTB385_S"
function extract_B285()  
    data = ArchGDAL.read(_path_B285 * "/Inv_FRTB385_S_Wfix_0.2477_rho_mod")
    obs = ArchGDAL.read(data) # size ( D, N, W)
    obs = Array{Float32,3}(obs)

    data = ArchGDAL.read(_path_B285 * "/Inv_FRTB385_S_Wfix_0.2477_drho")
    stds = ArchGDAL.read(data) # size ( D, N, W)
    stds = Array{Float32,3}(stds)

    out = Dict("obs" => obs, "stds" => stds)
    path = _path_B285 * "/data.mat"
    matwrite(path, out; compress = true)
    @info "Serialized in $path"
end

const _path_FRT_144E9_L = PATH_DATA * "HAPKE/glace/ar_FRT144E9_L_070720"
function extract_FRT_144E9_L()
    data = ArchGDAL.read(_path_FRT_144E9_L * "/Inv_FRT144E9_L_Wfix_0.61075_rho_mod")
    obs = Array{Float32,3}(ArchGDAL.read(data)) # size ( D, N, W)
    obs = permutedims(obs, [2,1,3]) # size (N , D, W)

    data = ArchGDAL.read(_path_FRT_144E9_L * "/Inv_FRT144E9_L_Wfix_0.61075_drho")
    stds = Array{Float32,3}(ArchGDAL.read(data)) # size ( D, N, W)
    stds = permutedims(stds, [2,1,3]) # size (N , D, W)

    header = read(_path_FRT_144E9_L * "/Inv_FRT144E9_L_Wfix_0.61075_rho_mod.hdr", String)
    regexp = r"wavelength = \{([^\}]*)\}"
    sw = match(regexp, header).captures[1]
    wavelength = [parse(Float64, strip(s)) for s in split(sw, ",")]
    @assert length(wavelength) == size(obs)[3]

    out = Dict("obs" => obs, "stds" => stds, "wavelength" => wavelength)
    path = _path_FRT_144E9_L * "/data.mat"
    matwrite(path, out; compress = true)
    @info "Serialized in $path"
end

# data for S serie
const _path_FRT_144E9_S = PATH_DATA * "HAPKE/glace/ar_FRT144E9_S"
function extract_FRT_144E9_S()
    data = matread(_path_FRT_144E9_S * "/Inv_FRT144E9_S_Wfix_0.60427_rho_mod.mat")

    wavelength = data["lambda"][:, 1]
    obs = Array{Float32,3}(data["cub_rho_mod"])
    stds = Array{Float32,3}(data["cub_drho_mod"])

    out = Dict("obs" => obs, "stds" => stds, "wavelength" => wavelength)
    path = _path_FRT_144E9_S * "/data.mat"
    matwrite(path, out; compress = true)
    @info "Serialized in $path"
end



# uncomment to run the extraction (should only be executed once)
# extract_140520()
# extract_B285()
# extract_FRT_144E9_L()
# extract_FRT_144E9_S()