# Implements helpers to save/load exp. results over differents sessions. 

import CRC32c
using Serialization
using Dates


mutable struct _paths
    """ Base directory containing observations data """
    ROOT_PATH_DATA::String
    
    """ Base directory for saving files """
    ROOT_PATH_ARCHIVE::String
end
const PATHS = _paths("", "")

"""
 Possibles subdirectorys
"""
const FOLDER_GLLIM = "_gllim/"
const FOLDER_NOISE =  "_noise/"
const FOLDER_GRAPHS =  "graphs/"
const FOLDER_BENCHMARK =  "_benchmark/"
const FOLDER_MISC =  "misc/"
const FOLDER_PREDICTIONS =  "predictions/"


get_hash(metas...) = join([string(CRC32c.crc32c(repr(meta))) for meta in metas], "-")


"""
 Return a suitable path to save data 
"""
function get_path(folder, metas... ; format_filename = nothing, label = "")
    h = get_hash(metas...)
    if format_filename != nothing
        h = format_filename(h)
    end
    
    dir = PATHS.ROOT_PATH_ARCHIVE * folder 
    if !isdir(dir)
        mkpath(dir)
    end
    dir * label * ":" * h
end 

function save(data, folder::String, metas...)
    meta_data = Dict("__data__" => data, "__saved__" => Dates.now())
    path = get_path(folder, metas...)
    f = open(path, "w")
    serialize(f, meta_data)
    close(f)
    @debug "Saved in $(path)"
end


function load(folder::String, metas...)
    path = get_path(folder, metas...)
    f = open(path, "r")
    d = deserialize(f)
    close(f)
    @debug "Loaded data from drive (saved on $(d["__saved__"]))"
    d["__data__"]
end


