using ..Probas
using LinearAlgebra 



struct tmpStorage{T <: Number} 
    cov::Matrix{T}
    x::Vector{T}
    density::Vector{T}
    tmpStorage{T}(L::Int) where {T <: Number} = new(Symmetric(zeros(T, L, L)), zeros(T, L), zeros(T, L))
end


"""
 update x_out in place. `tmp` is only used as memory storage
"""
function f_rec!(gmm::Gmm{T}, x_in::VecOrSub{T}, tmp::tmpStorage{T}, x_out::VecOrSub{T}) where {T}
    L::Int, K::Int = size(gmm.means)

    # reset the storage
    tmp.cov .= 0.
    tmp.x .= 0.
    
    @inbounds for k in 1:K 
        # compute the density p(x,k)
        p = gmm.weights[k] * exp(Probas.log_gaussian_density!(x_in, view(gmm.means, :, k), gmm.chol_covs[k], L, gmm.pre_factors[k], tmp.density))

        # update the current terms 
        tmp.cov .+= p * gmm.inv_covs[k]
        tmp.x .+= p * gmm.inv_covs[k]  * view(gmm.means, :, k)
    end

    iv = similar(tmp.cov)
    try 
        iv = inv(tmp.cov)
    catch err
        iv = pinv(tmp.cov)
    end
    x_out .= iv * tmp.x
end

function hessian(gmm::Gmm{T}, x::VecOrSub{T}) where {T} 
    L::Int, K::Int = size(gmm.means)
    out = zeros(T, L, L)
    for k in 1:K 
        p = gmm.weights[k] * exp(Probas.log_gaussian_density(x, gmm.means[:,k], gmm.chol_covs[k], L))
        m = gmm.means[:,k] .- x
        out .+= p * gmm.inv_covs[k] * (m * m' .-  inv(gmm.inv_covs[k])) * gmm.inv_covs[k]
    end
    Symmetric(out)
end

"""
 Store the converged point, if it's a local maxima, and the history 
    of the iterations 
"""
struct ModeResult{T <: Number} 
    x::Vector{T}
    is_maximum::Bool
    history::Matrix{T}
end

"""
    Run a fixed point algorithm started from `starts`
"""
function search_modes(gmm::Gmm{T}, starts::Matrix{T}, tol = 1e-8)::Vector{ModeResult{T}} where {T}
    L, K = size(starts)
    out = Vector{ModeResult{T}}(undef, K)
    
    # allocate memory 
    tmp = tmpStorage{T}(L)
    x_new = zeros(T, L)

    @inbounds for k in 1:K 
        # starting point 
        x = starts[:, k]

        # we do at least one iteration
        diff = tol + 1 
        hist = [copy(x)]

        while diff > tol 
            f_rec!(gmm, x, tmp, x_new) # compute the new point
            diff = norm(x - x_new)  # update criteria
            x .= x_new  # update current point
            push!(hist, copy(x)) # add to hist
        end
        
        is_maximum = isposdef!(- hessian(gmm, x)) 
        history = hcat(hist...) # store the path 
        out[k] =  ModeResult{T}(x, is_maximum, history)
    end
    out
end