using Colors 

"""
Setup graphic options 
"""
struct SerieConfig
    label::String
    format::Symbol  # one of :basic :dashed :circle 
    color::Union{String,Symbol,Colors.RGBA}
    alpha::Number
    hide_std::Bool
end 
SerieConfig(label, format, color; alpha=1, hide_std=true) = SerieConfig(label, format, color, alpha, hide_std)

"""
Store a serie of predictions, with optionnal diagnostics.
"""
struct Prediction
    id::String 

    Xmean::Matrix
    Xcovs::Union{Matrix,Nothing}

    Yerr::Union{Vector,Nothing} # || y - F(x) ||_2
    Xerr::Union{Vector,Nothing} # || x_obs - x_pred ||_2

    diagnostics::Dict{String,Vector} # label values

    config::SerieConfig
end
function Prediction(Xmean, Xcovs=nothing; diagnostics=Dict{String,Vector}(), Yerr=nothing, Xerr=nothing,
    config=SerieConfig("", :basic, :auto), id="")
    Prediction(id, Xmean, Xcovs, Yerr, Xerr, diagnostics, config)
end
