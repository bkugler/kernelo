using LinearAlgebra
using Statistics

using ..Gllim 
using ..Probas
using ..Models

const nbSamplingMonteCarlo = 100_000

"""
 Fast path for scalar mixture (D = 1)
"""
function covariance_mixture(weights::VecOrSub{T}, means::VecOrSub{T}, covs::VecOrSub{T})::T where {T <: Number}
    K = length(means)
    p1, sc = zero(T), zero(T)
    @inbounds for k in 1:K
        wk, mk = weights[k], means[k]
        sc += wk * mk
        p1 += wk * (covs[k] + mk^2)
    end
    p1 - sc^2
end


_sub_cov(cov::FullCov, d::Int) = UniformScaling(cov[d, d])
_sub_cov(cov::DiagCov, d::Int) = UniformScaling(cov[d,d])
_sub_cov(cov::IsoCov, d::Int) = cov

function sub_gllim(gllim::GLLiM{T,gT}, d::Int) where {T,gT <: CovsType{T}}
    sub_A = [A[d:d, :] for A in gllim.A]
    sub_b = gllim.b[d:d, :]
    sub_Sigma = [_sub_cov(S, d) for S in gllim.Σ]
    GLLiM{T,gT,IsoCovs{T}}(gllim.π, gllim.c, gllim.Γ, sub_A, sub_b, sub_Sigma)
end

"""
    Computes the Sobol first order indices, for the direct model.
    Returns a L x D matrix M,  where M_l,d = Var(E(Y_d|X_l)) / Var(Y_d)
    and X, Y ~ GLLiM
"""
function indice_sobol_direct(gllim::GLLiM{T,gT}, Ns = nbSamplingMonteCarlo) where {T,gT <: CovsType{T}}
    L::Int, D::Int, K::Int = gllim.L, gllim.D, gllim.K
    s_direct = zeros(T, L, D)

    x_sample = zeros(T, 1)
    tmp = zeros(T, 1)

    cS, gammaS = Gllim.cGamma_star(gllim)

    # Variance(Y_d)
    vy = zeros(T, D)
    @inbounds for d in 1:D
        vy[d] = covariance_mixture(gllim.π, cS[d, :], [m[d,d] for m in gammaS])
    end 

    cumweights = cumsum(gllim.π)
    @inbounds for l in 1:L
        gamma_chol = [cholesky(_sub_cov(g, l) * ones(1, 1)).L for g in gllim.Γ]
        pre_factors = [Probas.pre_factor(g, 1) for g in gamma_chol]
        
        esp_sampled = zeros(T, D, Ns)
        for s in 1:Ns 
            # sample Xl
            Probas.gmmsampling!(cumweights, gllim.c[l:l,:], gamma_chol, x_sample, tmp, K, 1)
            
            # compute E[Y| Xl = xl]
            sum_px = 0.
            for k in 1:K
                px = exp(Probas.log_gaussian_density!(x_sample, gllim.c[l:l, k], gamma_chol[k], 1, pre_factors[k], tmp))
                px *= gllim.π[k]
                mkx = cS[:, k] .+ (gllim.A[k] * gllim.Γ[k])[:, l:l] * inv(_sub_cov(gllim.Γ[k], l)) * (x_sample - gllim.c[l:l, k] ) 
                esp_sampled[:, s] .+= px .* mkx
                sum_px += px
            end
            esp_sampled[:, s] ./= sum_px # normalization
        end
        
        @assert all(isfinite.(esp_sampled))

        # compute S_l,d
        for d in 1:D 
            v = Statistics.var(esp_sampled[d,:])
            s_direct[l,d] = v / vy[d]
        end
    end

    s_direct, vy
end

struct FunctionnalGLLiM
    g::GLLiM
    L::Int 
    D::Int

    cache::Gllim.ConditionnalDensityCache{Float64}
    function FunctionnalGLLiM(gllim::GLLiM) 
        cache = Gllim.ConditionnalDensityCache{Float64}(gllim)
        new(gllim, gllim.L, gllim.D, cache)
    end
end


function Models.f!(g::FunctionnalGLLiM, x::VecOrSub, out::VecOrSub, D::Int) 
    Gllim.conditionnal_density!(g.g, x, g.cache)
    Probas.mean_mixture!(g.cache.out_weights, g.cache.out_means, out)
end

FunctionnalGLLiMInverse(gllim::GLLiM) = FunctionnalGLLiM(Gllim.inversion(gllim))



const with_f = Union{FunctionnalGLLiM,AbstractContext}

"""
 Implementation for classical Sobol indices, where the model is given as a function 
"""
function indice_sobol_direct(c::with_f, N = 100_000) 
    L, D = c.L, c.D
    X1, X2 = rand(L, N), rand(L, N)
    f02 = zeros(D)
    v = zeros(D)
    vl = zeros(L, D)
    y1, y2, y3 = zeros(D), zeros(D), zeros(D)
    for n in 1:N 
        Models.f!(c, X1[:, n], y1, D)
        Models.f!(c, X2[:, n], y2, D)
        for l in 1:L 
            x = X2[:, n]
            x[l] = X1[l,n]
            Models.f!(c, x, y3, D)
            vl[l, :] .+= y1 .* y3
        end
        f02 .+= y1 .* y2 
        v .+= y1.^2 .+ y2.^2
    end
    f02 ./= N
    v .= v ./ 2(N - 1) .- f02
    vl .= vl ./ N .- f02'

    s_direct = vl ./ v'
    s_direct, v
end


"""
    Computes the Sobol first order indices, for the inverse model.
    Returns a L x D matrix M,  where M_l,d = Var(E(X_l|Y_d)) / Var(X_l)
    and X, Y ~ GLLiM
"""
function indice_sobol_inverse(gllim::GLLiM{T,gT}, Ns = nbSamplingMonteCarlo) where {T,gT <: CovsType{T}}
    L::Int, D::Int, K::Int = gllim.L, gllim.D, gllim.K
    s_inverse = zeros(T, L, D)
    c_inverse = zeros(T, L, D)

    y_samples = zeros(T, 1, Ns)
    tmp = zeros(T, 1)

    # Variance(X_l)
    vx = zeros(T, L)
    @inbounds for l in 1:L 
        vx[l] = covariance_mixture(gllim.π, gllim.c[l,:], [m[l,l] for m in gllim.Γ])
    end 

    @inbounds for d in 1:D
        s_gllim = sub_gllim(gllim, d)
        i_gllim = Gllim.inversion(s_gllim)

        # sample Yd 
        gamma_s_chol = [cholesky(g).L for g in i_gllim.Γ]
        cumweights = cumsum(i_gllim.π)
        for s in 1:Ns 
            Probas.gmmsampling!(cumweights, i_gllim.c, gamma_s_chol, view(y_samples, :, s), tmp, K, 1)
        end
        
        # compute E[X| Yd ]
        fd, vard = Gllim.predict(i_gllim, y_samples; with_cov = true)

        # compute S_l,d
        for l in 1:L 
            vd = Statistics.var(fd[l,:])
            s_inverse[l,d] = vd / vx[l]
            
            # extract Var(Xl | Y) for each sample and average
            ed = Statistics.mean([cov[l,l] for cov in vard])
            c_inverse[l,d] = ed / vx[l]
        end
    end

    s_inverse, c_inverse
end

