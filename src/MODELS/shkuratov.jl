using LinearAlgebra

"""
 photometric coordinates in radians 
"""
struct PhotoCoord{T <: Number} 
    alpha::T 
    beta::T 
    gamma::T 
end


"""
 Returns incidence, emergence and azimuth in degrees 
 from photometric coordinates in radians (0...pi)
"""
function from_luminance(geoms::PhotoCoord)
    alpha, beta, gamma = geoms.alpha, geoms.beta, geoms.gamma
    cb = cos(beta)
    cosi = cb * cos(alpha - gamma)
    cose = cb * cos(gamma)
    i = acosd(cosi)
    e = acosd(cose)
    phi = acosd((cos(alpha) - cosi * cose) / (sind(i) * sind(e)))
    i, e, phi
end 

"""
 Returns photometric coordinates (in radians), from angles in degrees (0...180)
"""
function to_luminance(i, e, phi)::PhotoCoord
    sie2 = sind(i + e)^2
    cphi2 = cosd(phi / 2)^2
    ssei = sind(2 * e) * sind(2 * i)
    ci, ce, si, se = cosd(i), cosd(e), sind(i), sind(e)
    alpha = acos(ci * ce + si * se * cosd(phi))
    cos_beta = sqrt((sie2 - cphi2 * ssei) / (sie2 - cphi2 * ssei + se^2 * si^2 * sind(phi)^2 ))
    beta = acos(cos_beta)
    gamma = acos(ce / cos_beta)
    PhotoCoord(alpha, beta, gamma)
end


function shkuratov(an, mu1, nu, m, mu2, geoms::PhotoCoord)  
    alpha, beta, gamma = geoms.alpha, geoms.beta, geoms.gamma
    cosi = cos(beta) *  cos(alpha - gamma)
    f = (exp(-mu1 * alpha) + m * exp(- mu2 * alpha)) / (1 + m)
    d = cos(alpha / 2) * cos(pi * (gamma - alpha / 2) / (pi - alpha)) * cos(beta)^(nu * alpha * (pi - alpha)) / cos(gamma)
    r = an * f * d 
    r / cosi
end

# using InteractiveUtils
# g = to_luminance(10, 20, 40)
# shkuratov(1, 0.3, 0.1, 0.8, 0.2, g)
# shkuratov(1, 0.8, 4, g)



