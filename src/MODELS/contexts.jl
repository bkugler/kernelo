using MAT
using Sobol
using LaTeXStrings
using Printf
using Serialization
using JSON 

using ..Probas
using ..Archive 

include("hapke.jl")
include("shkuratov.jl")

export AbstractContext, HapkeContext, F


"""
From 0...1 to physique : parameters are in 
[0,1], [0, 1.5], [0.2,1], [0,1.5] , [0, 1.5]
"""
const SCALE_SHKURATOV = Diagonal([1,1.5,0.8,1.5,1.5])
const OFFSET_SHKURATOV = [0,0,0.2,0,0]
const SCALE_SHKURATOV_INV = inv(SCALE_SHKURATOV)

abstract type AbstractContext end 


struct LinearContext <: AbstractContext
    LABEL::String
    matrix::Matrix{Float64}
end
LinearContext() = LinearContext("Linear", 1. .* [1   0    0   0;
                                                         0   0.5  0   0;
                                                         0   0    1   0;
                                                         0   0    0   3;
                                                         0.2 0    0   0;
                                                         0   -0.5 0   0;
                                                         -0.2 0   -1  0;
                                                         -1  0    0   0;
                                                         0   0    0   -0.7;
                                                         0   0    0   0])
LinearSmallContext() = LinearContext("Linear", 0.5 .* [0.9 0.1;
                                                            0 0.7;
                                                            0 0])
LinearTrivialContext() = LinearContext("Linear", 0.5 * ones(1, 1))

struct QuadraticContext <: AbstractContext
    LABEL::String
    matrix::Matrix{Float64} # linear part 
end
QuadraticContext() = QuadraticContext("Quadratic", [1   2    2   1;
                                                    0   0.5  0   0;
                                                    0   0    1   0;
                                                    0   0    0   3;
                                                    0.2 0    0   0;
                                                    0   -0.5 0   0;
                                                    -0.2 0   -1  0;
                                                    -1  0    2   0;
                                                    0   0    0   -0.7])

struct QuadraticDoubleContext <: AbstractContext
    LABEL::String
end
QuadraticDoubleContext() = QuadraticDoubleContext("Quadratic double")

struct ExampleContext <: AbstractContext
    LABEL::String
    L::Int
    D::Int
    variables_names::Nothing
    ExampleContext() = new("Simple surface", 2, 1, nothing)
end

struct ExpContext <: AbstractContext
    LABEL::String
    L::Int
    D::Int
    variables_names::Nothing
    ExpContext(L) = new("Exponentielle", L, L, nothing)
end 

struct LogContext <: AbstractContext
    LABEL::String
    L::Int
    D::Int 
    variables_names::Nothing
    shift::Float64
    LogContext(L; shift = 5) = new("Slow function", L, L, nothing, shift)
end

"""
 Compute G o F 
"""
struct InjectifHard <: AbstractContext
    LABEL::String
    F::AbstractContext
    G::AbstractContext
end
InjectifHard() = InjectifHard("Complex injective function", ExpContext(4), QuadraticContext())


struct DoubleSolutionContext <: AbstractContext 
    LABEL::String
    L::Int
    D::Int
    variables_names::Vector{String}
    DoubleSolutionContext(L::Int) = new("Double solution", L, L, ["x$l" for l in 1:L]) 
end  
struct TripleSolutionContext <: AbstractContext 
    LABEL::String
    L::Int
    D::Int
    variables_names::Vector{String}
    TripleSolutionContext(L::Int) = new("Triple solution", L, L, ["x$l" for l in 1:L]) 
end  

struct SeismeContext <: AbstractContext
    LABEL::String
    L::Int
    D::Int
    variables_names::Vector{String}
    stations::Vector{Tuple{Float64,Float64}}
    SeismeContext() = new("Seismic epicentral", 2, 6, ["x", "y"],
        [(3, 15), (3, 16), (5, 15), (4, 16), (5, 15), (5, 16)])
end

struct CercleContext <: AbstractContext
    LABEL::String
    L::Int
    D::Int
    variables_names::Vector{String}
    CercleContext() = new("Cercle", 1, 2, ["t"])
end

"""
 Must have fields 
    LABEL
    partiel 
    settings_geometries
    variables_names
"""
abstract type HapkeContext <: AbstractContext end

abstract type LabHapkeContext <: HapkeContext end 

abstract type MeteoriteContext <: HapkeContext end 
abstract type Mukundpura <: MeteoriteContext end 
abstract type Blacky <: MeteoriteContext end 
abstract type MukundpuraBloc <: Mukundpura end 
abstract type MukundpuraPoudre <: Mukundpura end 
abstract type BlackyBloc <: Blacky end 
abstract type BlackyPoudre <: Blacky end 


struct HapkeFields 
    LABEL::String
    partiel::Bool
    w_linear::Bool # if true, change F to a linearized version in w
    settings_geometries::Vector{GeometricSettings{Float64}}
    variables_names::Vector{String}
end

mutable struct OlivineHapkeContext <: LabHapkeContext 
    fields::HapkeFields
    OlivineHapkeContext(;partiel = true, w_linear = true) = _init_hapke_fields("Olivine", partiel, w_linear, new())
end
mutable struct NontroniteHapkeContext <: LabHapkeContext  
    fields::HapkeFields
    NontroniteHapkeContext(;partiel = true, w_linear = true) = _init_hapke_fields("Nontronite", partiel, w_linear, new())
end 
mutable struct BasaltHapkeContext <: LabHapkeContext  
    fields::HapkeFields
    BasaltHapkeContext(;partiel = true, w_linear = true) = _init_hapke_fields("Basalt", partiel, w_linear, new())
end 
mutable struct GlassHapkeContext <: LabHapkeContext  
    fields::HapkeFields
    GlassHapkeContext(;partiel = true, w_linear = true) = _init_hapke_fields("Basaltic Glass", partiel, w_linear, new())
end 
mutable struct MagnesiteHapkeContext <: LabHapkeContext  
    fields::HapkeFields
    MagnesiteHapkeContext(;partiel = true, w_linear = true) = _init_hapke_fields("Magnesite", partiel, w_linear, new())
end 

mutable struct CrismHapkeContext <: HapkeContext 
    fields::HapkeFields
    CrismHapkeContext(;partiel = true, w_linear = true) = _init_hapke_fields("Crism", partiel, w_linear, new())
end
mutable struct JSC1HapkeContext <: HapkeContext
    fields::HapkeFields
    JSC1HapkeContext(;partiel = true, w_linear = true) = _init_hapke_fields("JSC1", partiel, w_linear, new())
end

mutable struct MukundpuraBlocContext <: MukundpuraBloc
    fields::HapkeFields
    indexes::Vector{Int}
    MukundpuraBlocContext(;partiel = true, w_linear = true) = _init_hapke_fields("Mukundpura - Bloc", partiel, w_linear, new())
end
mutable struct MukundpuraPoudreContext <: MukundpuraPoudre
    fields::HapkeFields
    indexes::Vector{Int}
    MukundpuraPoudreContext(;partiel = true, w_linear = true) = _init_hapke_fields("Mukundpura - Poudre", partiel, w_linear, new())
end
mutable struct MukundpuraBlocPrincipalContext <: MukundpuraBloc
    fields::HapkeFields
    indexes::Vector{Int}
    MukundpuraBlocPrincipalContext(;partiel = true, w_linear = true) = _init_hapke_fields("Mukundpura - Bloc - Plan principal", partiel, w_linear, new())
end
mutable struct MukundpuraPoudrePrincipalContext <: MukundpuraPoudre
    fields::HapkeFields
    indexes::Vector{Int}
    MukundpuraPoudrePrincipalContext(;partiel = true, w_linear = true) = _init_hapke_fields("Mukundpura - Poudre - Plan principal", partiel, w_linear, new())
end
mutable struct MukundpuraBlocReduitContext <: MukundpuraBloc
    fields::HapkeFields
    indexes::Vector{Int}
    d::Int

    function MukundpuraBlocReduitContext(d::Int ;partiel = true, w_linear = true) 
        c = new()
        c.d = d
        _init_hapke_fields("Mukundpura - Bloc - $d geom.", partiel, w_linear, c)
    end
end

mutable struct BlackyBlocContext <: BlackyBloc
    fields::HapkeFields
    indexes::Vector{Int}
    BlackyBlocContext(;partiel = true, w_linear = true) = _init_hapke_fields("Blacky - Bloc", partiel, w_linear, new())
end
mutable struct BlackyBlocPrincipalContext <: BlackyBloc
    fields::HapkeFields
    indexes::Vector{Int}
    BlackyBlocPrincipalContext(;partiel = true, w_linear = true) = _init_hapke_fields("Blacky - Bloc - Plan principal", partiel, w_linear, new())
end
mutable struct BlackyPoudreContext <: BlackyPoudre
    fields::HapkeFields
    indexes::Vector{Int}
    BlackyPoudreContext(;partiel = true, w_linear = true) = _init_hapke_fields("Blacky - Poudre", partiel, w_linear, new())
end
mutable struct BlackyPoudrePrincipalContext <: BlackyPoudre
    fields::HapkeFields
    indexes::Vector{Int}
    BlackyPoudrePrincipalContext(;partiel = true, w_linear = true) = _init_hapke_fields("Blacky - Poudre - Plan principal", partiel, w_linear, new())
end
mutable struct HowarditeContext <: MeteoriteContext
    fields::HapkeFields
    indexes::Vector{Int}
    HowarditeContext(;partiel = true, w_linear = true) = _init_hapke_fields("Howardite", partiel, w_linear, new())
end
mutable struct GlaceContext <: HapkeContext
    fields::HapkeFields
    GlaceContext(;partiel = true, w_linear = true) = _init_hapke_fields("Glace", partiel, w_linear, new())
end
mutable struct Mars2020Context <: HapkeContext
    fields::HapkeFields
    Mars2020Context(;partiel = true, w_linear = true) = _init_hapke_fields("Mars 2020", partiel, w_linear, new())
end
mutable struct B385Context <: HapkeContext
    fields::HapkeFields
    B385Context(;partiel = true, w_linear = true) = _init_hapke_fields("B385", partiel, w_linear, new())
end

struct CustomHapkeContext <: HapkeContext 
    fields::HapkeFields
    function CustomHapkeContext(geometries::Matrix; partiel = true, w_linear = true) 
        D, _ = size(geometries)
        settings_geometries = [ setup_geometries(Float64(geometries[d,1]), Float64(geometries[d,2]), Float64(geometries[d,3]))  for d in 1:D ]  
        if partiel
            variables_names = [L"\omega", L"\theta_{0}", L"b", L"c"]
        else
            variables_names = [L"\omega", L"\theta_{0}", L"b", L"c", L"H", L"B_{0}"]
        end
        fields = HapkeFields("Custom", partiel, w_linear, settings_geometries, variables_names)
        return new(fields)
    end
end


struct ShkuratovFields 
    LABEL::String
    partiel::Bool
    settings_geometries::Vector{PhotoCoord}
    variables_names::Vector{String}
end 

struct Shkuratov{T <: HapkeContext} <: AbstractContext 
    fields::ShkuratovFields
end




Base.show(io::IO, m::MIME"text/label", ct::AbstractContext) = write(io, "$(ct.LABEL) (L = $(ct.L), D = $(ct.D))")
Base.show(io::IO, m::MIME"text/label", ct::HapkeContext) = write(io, "Hapke model - $(ct.LABEL) setup")



function Base.getproperty(c::Union{LinearContext,QuadraticContext}, a::Symbol) 
    if a == :L
        size(c.matrix)[2]
    elseif a == :D
        size(c.matrix)[1]
    elseif a == :variables_names
        nothing
    else
        getfield(c, a)
    end
end
function Base.getproperty(c::QuadraticDoubleContext, a::Symbol) 
    if a == :L || a == :D || a == :variables_names
        getproperty(QuadraticContext(), a)
    else
        getfield(c, a)
    end
end
function Base.getproperty(c::InjectifHard, a::Symbol) 
    if a == :L || a == :variables_names
        getproperty(c.F, a)
    elseif a == :D
        getproperty(c.G, a)
    else
        getfield(c, a)
    end
end
const _hapke_fields = fieldnames(HapkeFields)
function Base.getproperty(c::HapkeContext, a::Symbol) 
    if a == :L
        c.fields.partiel ? 4 : 6
    elseif a == :D
        length(c.fields.settings_geometries)
    elseif a in _hapke_fields
        getfield(c.fields, a)
    else
        getfield(c, a)
    end
end
const _shkuratov_fields = fieldnames(ShkuratovFields)
function Base.getproperty(c::Shkuratov, a::Symbol) 
    if a == :L
        c.fields.partiel ? 3 : 5
    elseif a == :D
        length(c.fields.settings_geometries)
    elseif a in _shkuratov_fields
        getfield(c.fields, a)
    else
        getfield(c, a)
    end
end

_path_data(c::OlivineHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/lab_data/Olivine.ser"
_path_data(c::NontroniteHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/lab_data/Nontronite.ser"
_path_data(c::BasaltHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/lab_data/Basalt.ser"
_path_data(c::GlassHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/lab_data/Glass.ser"
_path_data(c::MagnesiteHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/lab_data/Magnesite.ser"
_path_data(c::CrismHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/exp1/Inv_FRT193AB_S_Wfix_0.33_rho_mod.mat"
_path_data(c::JSC1HapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/JSC1/analog_JSC1_BRDF.json"
_path_data(c::Mukundpura)  = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/MUKUNDPURA/mukundpura_bloc_poudre.ser"
_path_data(c::Blacky) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/BLACKY/blacky_bloc_poudre.ser"
_path_data(c::HowarditeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/howardite.ser"
_path_data(c::GlaceContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/glace"
_path_data(c::Mars2020Context) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/ar_IPAG_INRIA_140520"
_path_data(c::B385Context) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/ar_FRTB385_S"

_key_data(c::OlivineHapkeContext) = "BRF_OLV"
_key_data(c::NontroniteHapkeContext) = "BRF_NONTRONITE"
_key_data(c::BasaltHapkeContext) = "BRF_BAS"
_key_data(c::GlassHapkeContext) = "BRF_BASGL"
_key_data(c::MagnesiteHapkeContext) = "BRF_MGC"
_key_data(c::MukundpuraBloc) = "bloc"
_key_data(c::MukundpuraPoudre) = "poudre"
_key_data(c::BlackyBloc) = "bloc"
_key_data(c::BlackyPoudre) = "poudre"
_key_data(c::HowarditeContext) = "data_howardite.txt"

_indexes(geom::Matrix, c::MeteoriteContext) = 1:size(geom)[1]
function _indexes(geom::Matrix, c::Union{MukundpuraBlocPrincipalContext,MukundpuraPoudrePrincipalContext,BlackyBlocPrincipalContext,BlackyPoudrePrincipalContext}) 
    D, _ = size(geom)
    m = (geom[:, 3] .== 0) .| (geom[:,3] .== 180)
    a = [40 .<= phase_angle(geom[i, :]) .<= 120 for i in 1:D]
    m = m .& a 
    return collect(1:D)[m]    
end
function _indexes(geom::Matrix, c::MukundpuraBlocReduitContext)
    d  = c.d
    D, _ = size(geom)
    gs = [geom[i, :] for i in 1:D]
    p1 = sortperm(gs; by = g->phase_angle(g))
    p2 = sortperm(gs[p1]; by = g->g[3] % 180)
    sorted = p2[p1]
    return sorted[1:d]
end


function _to_float_64(m, precision) 
    round.(Float64.(m) .* 10^precision) ./ 10^precision
end

function _load_geometries(c::LabHapkeContext) 
    variables = deserialize(_path_data(c))
    geometries = hcat(variables["INC"], variables["EME"], variables["AZI"])
    geometries
end
# Inconsitent Data : "AZI" has length 10, "EME", "INC" have length 9 
function _load_geometries(c::MagnesiteHapkeContext) 
    variables = deserialize(_path_data(c))
    azi = variables["AZI"][2:end]
    geometries = hcat(variables["INC"], variables["EME"], azi)
    geometries
end
function _load_geometries(c::CrismHapkeContext)
    variables = matread(_path_data(c))
    geometries = hcat(variables["theta0"][1,:,], variables["theta"][1,:,], variables["phi"][1,:,])
    geometries
end
function _load_geometries(c::JSC1HapkeContext) 
    data = JSON.parsefile(_path_data(c))
    geoms_vect = data["JSC1"]["geometries"]
    geometries = Matrix(hcat(geoms_vect...)')
end
function _load_geometries(c::MeteoriteContext) 
    variables = deserialize(_path_data(c))
    geometries = variables[_key_data(c)]["geometries"]
    indexes = _indexes(geometries, c)
    c.indexes = indexes
    geometries[indexes, :]
end

const _Glace_theta0 = 72.2
const _Glace_theta = [66.8593,60.3159,54.0248,47.8762,41.8269, 4.8671,45.0859,50.2097, 55.9411,62.0412,68.1923]
const _Glace_azi = [147.131,147.700,148.347,149.093,149.999 ,105.632, 39.510, 38.805 ,38.143, 37.541, 37.018 ]
function _load_geometries(c::GlaceContext)
    D = length(_Glace_theta)
    hcat(_Glace_theta0 * ones(D), _Glace_theta, _Glace_azi)
end

const _Mars2020_theta0 = 56.0
const _Mars2020_theta = [70.0000,63.1966,56.6934,51.0794,45.0335, 8.3045,44.0245,49.3959,56.0636,62.4086,70.0642]
const _Mars2020_azi = [93.729,93.472,93.402,93.344,93.273,90.690,85.892,85.940,86.029,86.109,86.135]
function _load_geometries(c::Mars2020Context)
    D = length(_Mars2020_theta)
    hcat(_Mars2020_theta0 * ones(D), _Mars2020_theta, _Mars2020_azi)
end

const _B385_theta0 = 63.
const _B385_theta = [67.905182, 61.381596, 54.693436, 48.436565, 42.728062, 6.2155004, 46.040085, 51.366360, 57.344471, 63.380264, 69.69]
const _B385_azi = [131.760,131.121,130.297,129.265,128.308, 32.972, 40.276, 41.123, 41.871, 42.546, 42.975]
function _load_geometries(c::B385Context) 
    D = length(_B385_theta)
    hcat(_B385_theta0 * ones(D), _B385_theta, _B385_azi)
end

function _init_hapke_fields(label, partiel, w_linear, c::HapkeContext) 
    geometries = _load_geometries(c)
    @debug "Hapke geometries loaded"
    D, _ = size(geometries)
    settings_geometries = [ setup_geometries(Float64(geometries[d,1]), Float64(geometries[d,2]), Float64(geometries[d,3]))  for d in 1:D ]  
    if partiel
        variables_names = [L"\omega", L"\bar{\theta}", L"b", L"c"]
    else
        variables_names = [L"\omega", L"\bar{\theta}", L"b", L"c", L"H", L"B_{0}"]
    end
    c.fields  = HapkeFields(label, partiel, w_linear, settings_geometries, variables_names)
    return c
end

function from_hapke_context(c::HapkeContext) 
    T = Shkuratov{typeof(c)}
    geometries = _load_geometries(c)
    sg = [ to_luminance(geometries[d,1], geometries[d,2], geometries[d,3]) for d in 1:c.D ]
    if c.partiel
        variables_names = [L"A_{n}", L"\mu_1", L"\nu"]
    else
        variables_names = [L"A_{n}", L"\mu_1", L"\nu", L"m", L"\mu_2"]
    end
    fields = ShkuratovFields(c.LABEL, c.partiel, sg, variables_names)
    T(fields)
end


function load_observations(c::LabHapkeContext)
    variables = deserialize(_path_data(c))
    key = _key_data(c)
    wavelengths = variables["WAVE_VALUE"]
    observations = Matrix{Float64}(variables[key])
    # the measure error in approximately 5% 
    stds = observations .* (5. / 100)
    observations, _to_float_64(wavelengths, 2), stds
end
function load_observations(c::CrismHapkeContext) 
    variables = matread(_path_data(c))
    observations = Matrix{Float64}(variables["cub_rho_mod"][:,:,1]')
    # we repeat the mean std for each observations
    _, N = size(observations)
    stds = hcat([variables["drho"][1,:] for n in 1:N]...)
    observations, nothing, stds
end
function load_observations(c::JSC1HapkeContext) 
    data = JSON.parsefile(_path_data(c))
    obs_vect = data["JSC1"]["reflectance_poudre"]
    observations = hcat([mean_std[1] for mean_std in obs_vect]...)
    stds = hcat([mean_std[2] for mean_std in obs_vect]...)
    wavelengths = [w[1] for w in data["JSC1"]["wavelengths"]]
    observations, wavelengths, stds
end
function load_observations(c::T) where {T <: MeteoriteContext}
    variables = deserialize(_path_data(c))
    observations = variables[_key_data(c)]["observations"]
    stds = variables[_key_data(c)]["stds"]
    wavelengths = variables[_key_data(c)]["wavelengths"]
    observations = observations[c.indexes, :]
    stds = stds[c.indexes, :]
    observations, wavelengths, stds 
end

const _paths_glace = Dict(:S => "/ar_FRT144E9_S/data.mat", :L => "/ar_FRT144E9_L_070720/data.mat")
# doesn't clean NAN, voie = :S or :L
function _load_raw(c::GlaceContext, voie::Symbol)
    v = matread(_path_data(c) * _paths_glace[voie])
    obs = Array{Float64,3}(v["obs"])
    stds = Array{Float64,3}(v["stds"])
    wavelength = Vector{Float64}(v["wavelength"])
    return obs, wavelength, stds
end

"""
 Returns cubes of size N x D x W
"""
function load_observations(c::GlaceContext, voie::Symbol)
    # we keep spatials point for which all wavelengths are valid
    # we replace invalide stds by Yobs * 0.03
    Ycube, wavelengths, Stdcube  = _load_raw(c, voie)
    N, D, W = size(Ycube) # W for wavelenghts

    mask = [!all(isfinite.(Ycube[i, :,:])) || all(Ycube[i, :, :] .== 0) for i in 1:N] # bad points : null points are useless to invert

    mask_std = .~isfinite.(Stdcube) # bad
    Stdcube[mask_std] .= 0.03 * Ycube[mask_std]
    mask .= .~mask # good

    return Ycube[mask,:,:], wavelengths, Stdcube[mask,:,:], mask
end

const _Mars2020_exp = Dict(:FRT => "/Inv_FRT47A3_S_Wfix_0.56_rho_mod", :HRL => "/Inv_HRL40FF_S_Wfix_0.46_rho_mod")
# doesn't clean NAN 
function _load_raw(c::Mars2020Context, exp::Symbol)  
    dic = deserialize(_path_data(c) * "/data.ser")
    return dic[exp] # size ( D, N, W)
end

"""
 Returns cubes of size N x D x W. (symbol = FRT or HRL) 
"""
function load_observations(c::Mars2020Context, exp::Symbol)
    y = _load_raw(c, exp)
    D, N, W = size(y)
    mask = [all(isfinite.(y[:, i, :])) && any(y[:,i, :] .!= 0) for i in 1:N]
    y = permutedims(y, [2,1,3])
    # we assume std is 0.03 * Y 
    stds = y .* 0.03
    return y[mask, :, :], stds[mask, :,:], mask
end

function _load_raw(c::B385Context)  
    dic = deserialize(_path_data(c) * "/data.ser")
    return dic["obs"], dic["stds"]
end

"""
 Returns cubes of size N x D x W. (symbol = FRT or HRL) 
"""
function load_observations(c::B385Context)
    y, std = _load_raw(c)
    D, N, W = size(y)
    mask = [all(isfinite.(y[:, i, :])) && any(y[:,i, :] .!= 0) for i in 1:N]
    mask_std =  .~ isfinite.(std)
    # assume invalid std to be 0.01 * Y
    std[mask_std] .= y[mask_std] .* 0.01
    y = permutedims(y, [2,1,3])
    std = permutedims(std, [2,1,3])
    return y[mask, :, :], std[mask, :,:], mask
end



"""
Returns a matrix of results and a matrix of diagonal covariances 
"""
_path_result(c::OlivineHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/result_photom/Olivine.ser"
_path_result(c::NontroniteHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/result_photom/Nontronite.ser"
_path_result(c::BasaltHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/result_photom/Basalt.ser"
_path_result(c::GlassHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/result_photom/Glass.ser"
_path_result(c::MagnesiteHapkeContext) = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/result_photom/Magnesite.ser"

const _res_ind_lab = [4,3,1,2,6,5] # match our conventions for x variables
function load_results(c::LabHapkeContext)
    results = deserialize(_path_result(c))
    N = length(results)
    dim = c.L
    means = zeros(dim, N)
    covs = zeros(dim, N)
    for (i, vars) in enumerate(results)
        means[:,i] = vars["ESTIM_M"][_res_ind_lab][1:dim]
        covs[:,i] = (vars["VAR_M"][_res_ind_lab][1:dim]).^2 # actually, it's a STD
    end
    means, covs
end
function load_results_alt(c::NontroniteHapkeContext, with_cov_floor::Bool)
    path = Archive.PATHS.ROOT_PATH_DATA * "HAPKE/resultats_photom_lab_pilorget_2016/result_photom/Nontronite_alt/results.ser"
    key = with_cov_floor ? :WITH_FLOOR : :WITHOUT_FLOOR
    dic = deserialize(path)[key]
    means = dic["ESTIM_M_ALL"][_res_ind_lab, :][1:c.L, :] # handle partial model
    covs = (dic["VAR_M_ALL"][_res_ind_lab, :][1:c.L, :]).^2 # actually, it's STD
    means, covs
end

function load_results(c::CrismHapkeContext) 
    variables = matread(Archive.PATHS.ROOT_PATH_DATA * "HAPKE/exp1/FRT000193AB_result.mat")["result_summary"]
    latlong = variables[:, [1, 2], 1]
    mask = [-90 <= latlong[i, 1] <= 90 && -180 <= latlong[i,2] <= 180 for i in 1:size(latlong)[1]]
    RESULT_MEAN_INDEXS = [12, 9, 3, 6, 15, 18]
    RESULT_STD_INDEXS = [13, 10, 4, 7, 16, 19]
    dim = c.L
    mean_indexes, std_indexes = RESULT_MEAN_INDEXS[1:dim], RESULT_STD_INDEXS[1:dim]
    Matrix(variables[:, mean_indexes, 1]'), Matrix((variables[:, std_indexes, 1])'), Matrix(latlong'), mask
end

"""
 Cleanup observations 
"""
function load_clean(c::CrismHapkeContext)
    Yobs, _, Ystd = load_observations(c)
    mask1 = isfinite.(Yobs[1, :])
    Xres, Xstd, latlong, mask2 = load_results(c)
    mask = mask1 .& mask2
    Yobs[:,mask], Ystd[:, mask], Xres[:, mask], Xstd[:, mask], latlong[:, mask]
end


"""
Return a mask of valid vectors 
"""
function mask(c::AbstractContext, x::VecOrSub{T}, L::Int) where {T}
    @inbounds for l in 1:L 
        if (x[l] > 1) || (x[l] < 0)
            return false
        end
    end
    true
end
function mask!(c::AbstractContext, X::MatOrSub{T}, out::VecOrSub{Bool}, L::Int, N::Int) where {T}
    @inbounds for n in 1:N
        out[n] = mask(c, view(X, :, n), L)
    end
end 
function mask(c::AbstractContext, X::MatOrSub{T}) where {T} 
    L, N = size(X)
    out = zeros(Bool, N)
    mask!(c, X, out, L, N)
    out
end
function mask(c::AbstractContext, Xs::Array{T,3}) where {T}
    L, Ns, N = size(Xs)
    out = zeros(Bool, Ns, N)
    @inbounds for i in 1:N
        mask!(c, view(Xs, :, :, i), view(out, :, i), L, Ns)
    end 
    out 
end 

"""
Generate data suitable for GLLiM training 
"""
_add_offset(Y, mean::Number) = Y .+ mean 
_add_offset(Y, mean::Vector) = ( (D, N) = size(Y); Y .+ reshape(mean, D, 1) )
function data_training(c::AbstractContext, N::Int; noise_a = 1., noise_mean = 0., noise_std = 0.01)
    L = c.L
    s = SobolSeq(L)
    X::Matrix{Float64} = hcat([next!(s) for i in 1:N]...) # type hint needed to avoid infinite recursion. Bug ?
    X .+= 0.05 * (rand(size(X)...) .- 0.5) # scrambling
    X[ X .< 0 ] .= 0 # needed due to scrambling
    X[ X .> 1 ] .= 1
    Y = _add_offset(noise_a * F(c, X), noise_mean)
    Y .+= noise_std * randn(size(Y))
    X, Y 
end

"""
Compute Y = F(X).X is in [0,1], so re - scaling should be applyied here if needed
"""
function F(c::AbstractContext, X::MatOrSub{T}) where {T <: AbstractFloat}
    L, N = size(X)
    out = zeros(T, c.D, N)
    F!(c, X, out)
    out
end

function F!(c::AbstractContext, X::MatOrSub{T}, out::MatOrSub{T}) where {T <: AbstractFloat}
    D, N = size(out)
    @inbounds for n in 1:N 
        f!(c, view(X, :, n), view(out, :, n), D)
    end
end


"""
 We work with a "linear" version of Hapke, w.r.t.w 
"""
_w(wtilde) = 1 - (1 - wtilde)^2
_wtilde(w) = 1 - sqrt(1 - w)
function f!(ct::HapkeContext, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where {T <: Number}
    sg = ct.fields.settings_geometries
    r, b, c =  x[2] * THETA0_SCALING, x[3], x[4]
    w = x[1]
    if ct.fields.w_linear 
        w = _w(w)
    end
    if ct.fields.partiel 
        h, b0 = DEFAULT_H, DEFAULT_B0
    else 
        h, b0 = x[5], x[6]
    end 
    @inbounds for d in 1:D 
        out[d] = hapke(w, r, b, c, h, b0, sg[d])
    end
end
function f!(ct::Shkuratov, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where {T <: Number}
    sg = ct.fields.settings_geometries
    if ct.fields.partiel 
        m, mu2 = 0, 0
    else 
        m, mu2 = x[4] * 1.5, x[5] * 1.5 
    end 
    @inbounds for d in 1:D 
        out[d] = shkuratov(x[1], x[2] * 1.5, 0.8 * x[3] + 0.2, m, mu2, sg[d])
    end
end

function f!(c::LinearContext, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where {T <: AbstractFloat} 
    mul!(out, c.matrix, x)
end
function f!(c::QuadraticContext, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where {T <: AbstractFloat} 
    L = c.L
    out .= 0.5 * c.matrix * x
    @inbounds for l in 1:L
        out[l] += 5 * (x[l] + 0.5)^2
    end
    for l in L + 1:(2 * L)
        out[l] = (x[l - L] + 0.5)^2
    end
end
function  f!(_::ExampleContext, x::VecOrSub{T}, out::VecOrSub{T}, _::Int) where {T <: AbstractFloat} 
    out[1] = x[1]^2 + x[2]^3
end

const _cd = QuadraticContext()
function f!(c::QuadraticDoubleContext, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where {T <: AbstractFloat} 
    L = c.L
    x = copy(x)
    x[L - 1] = (x[L - 1] - 0.5)^2 / 0.25 # non injectif
    x .= exp.(x) # non linéaire
    f!(_cd, x, out, D)
end
function f!(c::InjectifHard, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where {T <: AbstractFloat} 
    tmp = zeros(c.L)
    f!(c.F, x, tmp, c.L)
    f!(c.G, tmp, out, D)
end
function f!(c::ExpContext, x::VecOrSub{T}, out::VecOrSub{T}, _::Int) where {T <: AbstractFloat}
    out .= exp.(x)
end
function f!(c::LogContext, x::VecOrSub{T}, out::VecOrSub{T}, _::Int) where {T <: AbstractFloat}
    for i in 1:c.L 
        out[i] = log(1 + c.shift * (i - 1) + x[i])
    end
end
function f!(c::DoubleSolutionContext, x::VecOrSub{T}, out::VecOrSub{T}, _::Int) where {T}
    out .= x
    out[1] = (x[1] - 0.5)^2
end
function f!(c::TripleSolutionContext, x::VecOrSub{T}, out::VecOrSub{T}, _::Int) where {T}
    out .=  0.5 .* (1 .+ cos.(3 * pi .* x))
end
function f!(c::CercleContext, x::VecOrSub{T}, out::VecOrSub{T}, _::Int) where {T}
    t = x[1]
    out[1] = cos(t * pi)
    out[2] = sin(t * pi)
end
function f!(c::SeismeContext, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where {T}
    for d in 1:D 
        xi, yi = c.stations[d]
        out[d] = sqrt((xi - x[1])^2 + (yi - x[2])^2) / 5 
    end
end

"""
    Return a scaled version of mean and cov prediction 
"""
function to_physique(c::HapkeContext, xmean::VecOrSub, xcov::Union{Vector,Nothing}) 
    A = c.fields.partiel ? [1,THETA0_SCALING,1,1] : [1,THETA0_SCALING,1,1,1,1]
    xmean_scaled = A .* xmean
    if c.fields.w_linear 
        xmean_scaled[1] =  _w(xmean_scaled[1])
    end
    xcov_scaled = xcov == nothing ? nothing : A .* xcov .* A
    xmean_scaled, xcov_scaled
end 


to_physique(c::AbstractContext, Xmean, Xcov = nothing) = Xmean, Xcov
function to_physique(c::HapkeContext, Xmean::Matrix, Xcov::Union{Matrix,Nothing} = nothing)
    L, N = size(Xmean)
    Xmean_scaled = zeros(L, N)
    Xcov_scaled = zeros(L, N)
    for n in 1:N 
        mean, cov = to_physique(c, Xmean[:, n], Xcov == nothing ? nothing : Xcov[:, n])
        Xmean_scaled[:,n] = mean 
        if cov != nothing
            Xcov_scaled[:, n] = cov
        end
    end
    Xmean_scaled, Xcov == nothing ? nothing : Xcov_scaled
end
function to_physique(c::Shkuratov, Xmean, Xcov = nothing)  
    A = c.fields.partiel ? SCALE_SHKURATOV[1:3,1:3] : SCALE_SHKURATOV
    b = c.fields.partiel ? OFFSET_SHKURATOV[1:3] : OFFSET_SHKURATOV
    Xmean_scaled = A * Xmean .+ b
    Xcov_scaled = Xcov == nothing ? nothing : hcat([A' * cov * A for cov in eachcol(Xcov)]...)
    Xmean_scaled, Xcov_scaled
end 

from_physique(c::AbstractContext, Xmean) = Xmean
function from_physique(c::HapkeContext, Xmean) 
    A = c.fields.partiel ? Diagonal([1,1 / THETA0_SCALING,1,1]) : Diagonal([1,1 / THETA0_SCALING,1,1,1,1])
    Xmean_scaled = A * Xmean
    if c.fields.w_linear 
        Xmean_scaled[1,:] =  _wtilde.(Xmean_scaled[1,:])
    end
    Xmean_scaled
end
function from_physique(c::Shkuratov, Xmean) 
    A = c.fields.partiel ? SCALE_SHKURATOV_INV[1:3,1:3] : SCALE_SHKURATOV_INV
    b = c.fields.partiel ? OFFSET_SHKURATOV[1:3] : OFFSET_SHKURATOV
    Xmean_scaled = A * (Xmean .- b)
    Xmean_scaled
end

"""
Compute the log density of y | x, given the model Y = a.F(X) + mu + eps 
It's also the log density of x | y, up to the unknow constant p(y),
assuming X is uniform
"""
function log_conditionnal_density(c::AbstractContext, x::VecOrSub{T}, y::VecOrSub{T}, 
                                a::T, mu::Vector{T}, Sigma::CovType{T}) where {T}
    L, D = c.L, length(mu)
    if !mask(c, x, L) return - Inf end
    tmp = zeros(T, D)
    f!(c, x, tmp, D)
    tmp .= a .* tmp .+ mu 
    Probas.log_gaussian_density(tmp, y, Sigma, c.D)
end

"""
Compute the log density of y | x.
It's also the log density of x | y, up to the unknow constant p(y),
assuming X is uniform.
The std on the noise is given by F(x) / ratio  
"""
function log_conditionnal_density2(c::AbstractContext, x::VecOrSub{T}, y::VecOrSub{T}, 
                                a::T, mu::Vector{T}, ratio) where {T}
    L, D = c.L, length(mu)
    if !mask(c, x, L) return - Inf end
    tmp = zeros(T, D)
    f!(c, x, tmp, D)
    y1 = a .* tmp .+ mu 
    Probas.log_gaussian_density(y1, y, Diagonal((tmp ./ ratio).^2), c.D)
end

"""
In case of double solutions, give the other one 
"""
alternative_solution(ct::AbstractContext, x) = nothing
function alternative_solution(ct::DoubleSolutionContext, X::MatOrSub) 
    out = copy(X)
    out[1, :] = 1 .- out[1, :]
    out
end
function alternative_solution(ct::TripleSolutionContext, X::MatOrSub) 
    X .+ (1 / 3)
end
function alternative_solution(ct::QuadraticDoubleContext, X::MatOrSub)
    X = copy(X)
    L = ct.L 
    X[L - 1, :] = 1 .- X[L - 1,:]
    X
end

