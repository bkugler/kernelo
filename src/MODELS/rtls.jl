""" Author: Dr. Douté S. <sylvain.doute@obs.ujf-grenoble.fr>, 2015. 
    Author: Dr. Ceamanos X. <xavier.ceamanos@onera.fr>, 2011.
"""

using LinearAlgebra

""" Returns vectors of RTLS base """
function RTLS(SZA,VZA,phi)         
    #fV
    xi = acos.(cosd.(SZA) .* cosd.(VZA) .+ sind.(SZA) .* sind.(VZA) .* cosd.(phi))
    fV = ((pi/2 .- xi) .* cos.(xi) .+ sin.(xi)) ./ (cosd.(SZA) .+ cosd.(VZA)) .- pi/4

    # fG
    b_r = 1.
    h_b = 2.
    SZA2 = atan.(b_r*tand.(SZA))
    VZA2 = atan.(b_r*tand.(VZA))
    xi2 = acos.(cos.(SZA2).*cos.(VZA2) .+ sin.(SZA2).*sin.(VZA2).*cosd.(phi))
    D = sqrt.(tan.(SZA2).^2 .+ tan.(VZA2).^2 .- 2*tan.(SZA2).*tan.(VZA2).*cosd.(phi))
    cos_t = h_b*sqrt.(D.^2 .+ (tan.(SZA2).*tan.(VZA2).*sind.(phi)).^2)./(sec.(SZA2) .+ sec.(VZA2))
    cos_t[cos_t .> 1] .= 1
    cos_t[cos_t .< -1] .= -1
    
    t = acos.(cos_t)
    O = 1/pi *(t .- sin.(t).*cos.(t)).*(secd.(SZA) .+ secd.(VZA))
    fG = O .- sec.(SZA2) .- sec.(VZA2) .+ 0.5*(1 .+ cos.(xi2)).*secd.(SZA).*secd.(VZA)
    fL = ones(size(fV))
    fL, fG, fV
end
                
""" Returns the matrix of projection of one observable onto RTLS base """
function get_projection(fL, fG, fV)
    T = UpperTriangular(zeros(3,3))

    T[1,1] = 1 / norm(fL)
    eL = T[1,1] * fL 

    pL_fG = dot(fG,eL) * eL 
    den = norm(fG - pL_fG)
    eG = (fG .- pL_fG) ./ den 
    T[1,2] = - dot(fG, eL) * T[1,1] / den
    T[2,2] = 1 / den

    pLG_fV = dot(fV, eL) * eL + dot(fV, eG) * eG
    den = norm(fV - pLG_fV)
    eV = (fV - pLG_fV) / den
    T[1,3] = - ( dot(fV, eL) * T[1,1] + dot(fV, eG)* T[1,2]) / den 
    T[2,3] = - dot(fV, eG) * T[2,2] / den 
    T[3,3] = 1 / den 

    U = T * hcat(eL, eG, eV)'
end

function rtls_projection(geometries::Matrix{T}) where {T}
    size(geometries)[2] == 3 || error("'geometries' should be a D x 3 matrix ! ")
    fL, fG, fV = RTLS(geometries[:,1], geometries[:,2], geometries[:,3])
    get_projection(fL, fG, fV)
end
