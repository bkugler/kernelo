"""
    Implementation of Hapke's radiative model in Julia. (B.K. 2019)
    Physical argument order : w, theta0, b, c, H, B0
"""
const VARIANT = 2002
const THETA0_SCALING = 30
const DEFAULT_B0 = 0.
const DEFAULT_H = 0.1 

struct GeometricSettings{T <: Number}
    geom_infos::NTuple{8,T}
    g::T
    cosg::T
    alpha::T
end

# -------------------------------- Helpers --------------------------------
function fast_e1_e2(R, x::T, x0::T) where {T <: AbstractFloat}
    tr = tan(R)
    tr2 = tr^2
    trtx = tr * tan(x)
    trtx0 = tr * tan(x0)
    if trtx == 0
        e1x = 0
        e2x = 0
    else
        e1x = exp(-2 / (trtx * pi))
        e2x = exp(-1 / (trtx^2 * pi ))
    end 
    if trtx0 == 0
        e1x0 = 0
        e2x0 = 0
    else
        e1x0 = exp(-2 / (trtx0  * pi))
        e2x0 = exp(-1 / (trtx0^2 * pi ))
    end
    e1x, e2x, e1x0, e2x0, tr
end


function H_2002(x::T, y::T) where {T <: Real}
    (1 + 2 * x) / (1 + 2 * x * y)
end


function H_1993(x::T, y::T) where {T <: Real} # eq 3 from icarus Schimdt
    u = (1 - y) / (1 + y)
    1 / ( 1 - (1 - y) * x * (  u  +  (1 - (0.5 + x) * u) * log1p(1 / x) ))
end

"""
    Only geometrical-dependant computations for roughness
"""
function _geom_roughness(theta0::T, theta::T, phi::T) where {T <: AbstractFloat}
    cose = cos(theta)
    sine = sin(theta)
    cosi = cos(theta0)
    sini = sin(theta0)

    phir = phi * pi / 180.  # radians

    f::T = 0
    if phi != 180
        f = exp(-2 * tan(phir / 2))
    end
    theta0, theta, cose, sine, cosi, sini, phir, f
end

"""
    phi in degrees for numerical stability
"""
function compute_roughness(geom_infos::NTuple{8,T}, R) where {T <: AbstractFloat}
    theta0, theta, cose, sine, cosi, sini, phir, f = geom_infos

    xidz = 1 / sqrt(1 + pi * (tan(R)^2))
    e1R, e2R, e1R0, e2R0, tR = fast_e1_e2(R, theta, theta0)

    mu_b = xidz * (cose + sine * tR * e2R / (2 - e1R))
    mu0_b = xidz * (cosi + sini * tR * e2R0 / (2 - e1R0))

    if theta0 <= theta
        mu0_e = xidz * (cosi + sini * tR * (cos(phir) * e2R + (sin(phir / 2)^2) * e2R0) / (
                2 - e1R - (phir / pi) * e1R0))
        mu_e = xidz * (cose + sine * tR * (e2R - (sin(phir / 2)^2) * e2R0) / (
                2 - e1R - (phir / pi) * e1R0))

        S = mu_e * cosi * xidz / mu_b / mu0_b / (1 - f + f * xidz * cosi / mu0_b)
    else
        mu0_e = xidz * (cosi + sini * tR * (e2R0 - sin(phir / 2)^2 * e2R) / (
                2 - e1R0 - (phir / pi) * e1R))

        mu_e = xidz * (cose + sine * tR * (cos(phir) * e2R0 + sin(phir / 2)^2 * e2R) / (
                2 - e1R0 - (phir / pi) * e1R))

        S = mu_e * cosi * xidz / mu_b / mu0_b / (1 - f + f * xidz * cose / mu_b)
    end
    mu0_e, mu_e, S
end

function phase_angle_rad(theta0r::T, thetar::T, phir::T)where {T}
    cosg = cos(theta0r) * cos(thetar) + sin(thetar) * sin(theta0r) * cos(phir)
    cosg, acos(cosg)
end
phase_angle(geom) = phase_angle_rad(geom[1] * pi / 180, geom[2] * pi / 180, geom[3] * pi / 180)[2] * 180 / pi

function setup_geometries(sza::T, vza::T, dphi::T) where {T}
    theta0r = sza * pi / 180
    thetar = vza * pi / 180
    phir = dphi * pi / 180
    geom_infos = _geom_roughness(theta0r, thetar, dphi)
    cosg, g = phase_angle_rad(theta0r, thetar, phir)
    alpha = 4 * cos(theta0r)

    GeometricSettings{T}(geom_infos, g, cosg, alpha)
end

""" Henyey-Greenstein function """
function phg2(b::T, c::T, cosg::T) where {T}
    bc = 2 * b * cosg
    b2 = b^2
    P =  (1 - c) * (1 - b2) / ((1 + bc + b2)^1.5)
    P = P + c * (1 - b2) / ((1 - bc + b2)^1.5)
    P
end

# compute the intermediate quantities 
function _hapke_steps(w::T, r1::T, h::T, b0::T, geom::GeometricSettings{T}) where {T}
    r = r1 * pi / 180.  # radians
    MUP, MU, S = compute_roughness(geom.geom_infos, r)

    B = b0 * h / ( h + tan(geom.g / 2) )
    gamma = sqrt(1 - w)
    if VARIANT == 1993
        H0 = H_1993(MUP, gamma)
        H = H_1993(MU, gamma)
    else
        H0 = H_2002(MUP, gamma)
        H = H_2002(MU, gamma)
    end
    a = (w / (MU + MUP)) * MUP  / geom.alpha
    a, S, B, H0, H
end

function hapke(w::T, r1::T, b::T, c::T, h::T, b0::T, geom::GeometricSettings{T}) where {T}
    P = phg2(b, c, geom.cosg)
    a, S, B, H0, H = _hapke_steps(w, r1, h, b0, geom)
    reff = a *  ((1 + B) * P + (H0 * H) - 1)  * S 
    reff 
end

""" Given a reflectance, compute the phase function without the need
of b and c 
"""
function phase_from_reflectance(y::T, w::T, r1::T, h::T, b0::T,  geom::GeometricSettings{T}) where {T}
    a, S, B, H0, H = _hapke_steps(w, r1, h, b0, geom)
    P = ( y / (a*S) - H0 * H + 1 ) / (1 + B)
    P
end 

# ------------------------------------------ Main function ------------------------------------------
"""
    Case of H or B0 being constant
"""
_VecOrSub{T} = Union{Vector{T},SubArray{T,1,Matrix{T}}}
function _get_h_b0(n::Int, HH::_VecOrSub{T}, B0::_VecOrSub{T}) where {T}
    HH[n], B0[n]
end 
function _get_h_b0(n::Int, HH::_VecOrSub{T}, B0::T) where {T}
    HH[n], B0
end 
function _get_h_b0(n::Int, HH::T, B0::_VecOrSub{T}) where {T}
    HH, B0[n]
end 
function _get_h_b0(n::Int, HH::T, B0::T) where {T}
    HH, B0
end 


"""
 
    Write the result in out 
"""
function _Hapke!(SZA::Vector{T}, VZA::Vector{T}, DPHI::Vector{T}, W::_VecOrSub{T}, R1::_VecOrSub{T}, 
                                    BB::_VecOrSub{T}, CC::_VecOrSub{T}, HH::Union{_VecOrSub{T},T}, B0::Union{_VecOrSub{T},T}, 
                                    out::Union{Matrix{T},SubArray{T,2,Array{T,3}}}) where {T <: AbstractFloat}

    Nx = length(W)
    D = length(SZA)
    @assert size(out) == (D, Nx) "Wrong size for given output matrix !"

    @fastmath @inbounds for d in 1:D
        geom = setup_geometries(SZA[d], VZA[d], DPHI[d])
        for n in 1:Nx
            h, b0 = _get_h_b0(n, HH, B0)
            out[d,n] = hapke(W[n], R1[n], BB[n], CC[n], h, b0, geom)
        end
    end
end


"""
 
    Write a matrix of reflectance (D, Nx)
    W, R1, BB, CC, HH, B0 are in [0,1] and will be rescalled.
"""
function Hapke!(SZA, VZA, DPHI, W, R1, BB, CC, out; HH = DEFAULT_H, B0 = DEFAULT_B0) 
    Nx = length(W)
    D = length(SZA)
    (D, Nx) == size(out) || error("out size does not match given Nx and D !")
    _Hapke!(SZA, VZA, DPHI, W, R1 .* THETA0_SCALING, BB, CC, HH, B0, out)
end

function Crelation(b)
    cp =  3.29 * exp.(-17.4 * (b.^2)) .- 0.908
    (cp .+ 1) / 2
end
