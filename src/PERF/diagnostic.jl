# Implements diagnostics tools assessing the quality of the GLLiM learning 

using Statistics
using LinearAlgebra

using ..Gllim 
using ..Models
using ..Probas 

# -------------------- Forward model --------------------

mutable struct _config 
    Nsample::Int
end
const Config = _config(10_000) 

""" Sample points and compute the errors F(x) - F_theta(x), returned as a mean """
function direct_error(c::Models.AbstractContext, gllim::Gllim.GLLiM{T}) where {T}
    Xobs, Yobs = Models.data_training(c, Config.Nsample; noise_std = 0)
    Ypred, _ = Gllim.predict(gllim, Xobs; with_cov = false)
    errors = sum((Yobs .- Ypred) .^ 2; dims = 1)
    Statistics.mean(errors)
end

# ------------------ Covariance error ------------------

_trace(gllim, sigma::IsoCov) = sigma.λ * gllim.D
_trace(gllim, sigma) = tr(sigma)

""" Averaged trace of Sigma covariance matrix """ 
function sigma_error(gllim::Gllim.GLLiM{T}) where {T}
    Statistics.mean([_trace(gllim, sigma) for sigma in gllim.Σ])
end

# ------------------------- BIC ------------------------

_dimension(cov::IsoCov) = 1
_dimension(cov::DiagCov) = length(cov.diag)
_dimension(cov::FullCov) = (D = size(cov)[1]; D * (D+1) ÷ 2)

""" Total number of scalar parameters in the model """
function dimension(gllim::Gllim.GLLiM)
    L, D = gllim.L, gllim.D
    gamma_dim = _dimension(gllim.Γ[1]) # same sizes for the whole vector
    sigma_dim = _dimension(gllim.Σ[1]) # idem
    # each params is prop to K 
    gllim.K * (1 + L + gamma_dim + L * D + D + sigma_dim)
end

""" Bayesian Information Criterion (for the training set X, Y) 
Also returns intermediate values : ll & dimension
"""
function bic(gllim::Gllim.GLLiM{T}, X::Matrix{T}, Y::Matrix{T}) where {T} 
    _, N = size(X)
    _, log_ll = Gllim.next_rnk(X, Y, log.(gllim.π), gllim.c, gllim.Γ, gllim.A, gllim.b, gllim.Σ)
    ll = sum(log_ll)
    dim = dimension(gllim)
    -2 * ll + dim * log(N), ll, dim
end