using LinearAlgebra
import Base.+, Base.one, Base.*

using ..Probas

"""
 GLLiM meta data. Useful to setup parameters, and load/save from disk
"""
abstract type MetaGLLiM end

"""
 Familly of EM training. Must have mex_iteration, K, Ystd, Ntrain fields
"""
abstract type EMMetaGLLiM <: MetaGLLiM end

Base.string(p::EMMetaGLLiM)  = string(p.max_iteration, p.K, p.Ystd, p.Ntrain)



struct GLLiM{T <: AbstractFloat,gammaT <: CovsType{T},sigmaT <: CovsType{T}}
#   Computed properties :
#   K::Int "number of Gaussians"
#   D::Int "dimension of Gaussian on Y"
#   L::Int "dimension of Gaussian on X"
    "weights (size K)"
    π::Vector{T}
    "means (size K x l)"
    c::Matrix{T}
    "covariances (size K x L for diagonal, or K x (L^2) for full)"
    Γ::gammaT
    "Linear components (size K x D x L)"
    A::Vector{Matrix{T}}
    "Intercepts size(K x D)"
    b::Matrix{T}
    "covariances (size K x D for diagonal, or K x (D^2) for full)"
    Σ::sigmaT
end


function Base.getproperty(gllim::GLLiM, a::Symbol)
    if a == :K
        length(gllim.π)
    elseif a == :D
        size(gllim.b)[1]
    elseif a == :L 
        size(gllim.c)[1]
    else
        getfield(gllim, a)
    end
end

"""
 Instanciation. Output is not a matematically valid model and needs to be filled.
"""
function GLLiM{T,gammaT,sigmaT}(K::Int, L::Int, D::Int) where {T <: AbstractFloat,gammaT <: CovsType{T},sigmaT <: CovsType{T}}
    π = zeros(K)
    c, b = zeros(L, K), zeros(D, K)
    A = []
    Γ = gammaT()
    Σ = sigmaT()
    GLLiM{T,gammaT,sigmaT}(π, c, Γ, A, b, Σ)
end

"""
Random initialisation (useful for debugging purpose)
"""
function randGLLiM(K::Int, L::Int, D::Int)
    π = rand(K)
    π ./= sum(π)
    c, b = rand(L, K), rand(D, K)
    A = [rand(D, L) for k in 1:K]
    Γ = [ (u = Matrix(LinearAlgebra.LowerTriangular(rand(L, L) .+ 2)) ;  LinearAlgebra.Symmetric(u * u')) for k in 1:K]
    Σ = [ (u = Matrix(LinearAlgebra.LowerTriangular(rand(D, D) .+ 2)) ;  LinearAlgebra.Symmetric(u * u')) for k in 1:K ]
    GLLiM{Float64,FullCovs{Float64},FullCovs{Float64}}(π, c, Γ, A, b, Σ)
end


"""
 Direct model 
"""
F_gllim(gllim::GLLiM, X::MatOrSub{T}) where {T} = predict(gllim, X; with_cov = false)


# -------------------------------------- Translation -------------------------------------- #
+(U::FullCov{T}, V::FullCov{T}) where {T} = Symmetric(invoke(+, Tuple{AbstractArray{T,2},AbstractArray{T,2}}, U, V))
+(U::FullCov{T}, V::DiagCov{T}) where {T} = Symmetric(invoke(+, Tuple{AbstractArray{T,2},AbstractArray{T,2}}, U, V))
one(U::FullCov{T}) where {T} = Symmetric(invoke(one, Tuple{AbstractArray{T,2}}, U))
*(a::T, U::FullCov{T}) where {T <: Number} = Symmetric(invoke(*, Tuple{T,AbstractArray{T,2}}, a, U))
"""
Add a, mu, Sigma to given gllim (A -> a * A, b -> ab + mu, S -> a2 S + Sigma)
"""
function add_bias(gllim::GLLiM{T,gT,sT}, a::U, mean::Vector{T}, cov::CovType{T}) where {T <: Number,U <: Number,sT <: CovsType{T},gT <: CovsType{T}}
    K = gllim.K
    A = [a * Ak for Ak in gllim.A ]
    b = hcat([ a * gllim.b[:,k] .+ mean for k in 1:K ]...)
    Σ = [ a^2 * gllim.Σ[k] + cov for k in 1:K ]
    GLLiM{T,gT,sT}(gllim.π, gllim.c, gllim.Γ, A, b, Σ)
end
add_bias(gllim, a, mean::T, cov) where {T <: Number} = add_bias(gllim, a, mean * ones(gllim.D), cov)
add_bias(gllim, a, mean, cov::T) where {T <: Number} = add_bias(gllim, a, mean, UniformScaling(cov))
add_bias(gllim, a, mean::T, cov::U) where {T <: Number,U <: Number} = add_bias(gllim, a, mean, UniformScaling(cov))

# ---------------------------------------------------------------------------------------- #
# ---------------------------------------- Inverted model -------------------------------- #
# ---------------------------------------------------------------------------------------- #

function cGamma_star(gllim::GLLiM) 
    K = gllim.K

    # (9)
    cS = hcat([gllim.A[k] * gllim.c[:,k] .+ gllim.b[:,k] for k in 1:K]...)  

    # (10)
    ΓS = [LinearAlgebra.Symmetric(gllim.Σ[k] + gllim.A[k] * gllim.Γ[k] * gllim.A[k]') for k in 1:K ] 
    cS, ΓS
end

"""
GLLiM parameters inversion
Returns a gllim instance, with full covariances.
"""
function inversion(gllim::GLLiM{T}) where {T}
    K, D, L = gllim.K, gllim.D, gllim.L
    b, c = gllim.b, gllim.c

    cS, ΓS = cGamma_star(gllim) 

    ΣS = [ LinearAlgebra.Symmetric(zeros(L, L)) for k in 1:K]
    AS = [ zeros(L, D) for k in 1:K ]
    bS = zeros(L, K)
    # @inbounds Threads.@threads for k in 1:K 
    @inbounds for k in 1:K 
        i = inv(gllim.Σ[k]) * gllim.A[k]

        if isapprox(gllim.A[k], zeros(D, L))
            sigS = gllim.Γ[k]
            bs = c[:,k]
        else
            ig = inv(gllim.Γ[k])
            sigS = inv(ig +  gllim.A[k]' * i)  # (14)
            bs = sigS * (ig * view(c, :, k) - i' * view(b, :, k))  # (13)
        end
        aS = sigS * i'  # (12)

        ΣS[k] = LinearAlgebra.Symmetric(sigS)
        AS[k] = aS
        bS[:,k] = bs
    end
    GLLiM{T,FullCovs{T},FullCovs{T}}(gllim.π, cS, ΓS, AS, bS, ΣS)
end

struct ConditionnalDensityCache{T <: Number}
    out_weights::VecOrSub{T}
    out_means::MatOrSub{T}
    allocated::Vector{T}
    chol_covs::CholCovs{T}
    pre_factors::Vector{T}
end
function ConditionnalDensityCache{T}(gllim::GLLiM{T}) where {T <: Number}
    K::Int, L::Int, D::Int = gllim.K, gllim.L, gllim.D
    out_weights = zeros(T, K)
    out_means = zeros(T, D, K)
    allocated = zeros(T, L)
    chol_covs = CholCovs{T}(undef, K)
    pre_factors  = zeros(T, K)
    for k in 1:K
        c = cholesky(gllim.Γ[k]).L 
        chol_covs[k] = c
        pre_factors[k] =  Probas.pre_factor(c, L)
    end
    ConditionnalDensityCache{T}(out_weights, out_means, allocated, chol_covs, pre_factors)
end

"""
Returns the direct contionnal density Y knowing X.
May be applied to gllim* and Y to obtain the inverse conditionnal density

Returns `weights` (K, N) and `means` (_, K,N)
"""
function conditionnal_density(gllim::GLLiM{T}, X::Matrix{T}) where {T}
    L, N = size(X)
    K::Int, D::Int = gllim.K, gllim.D
    means = zeros(T, D, K, N)
    weights = zeros(T, K, N)
    allocated = zeros(T, L, N)

    chol_covs = CholCovs{T}(undef, K)
    pre_factors  = zeros(T, K)
    for k in 1:K
        c = cholesky(gllim.Γ[k]).L 
        chol_covs[k] = c
        pre_factors[k] =  Probas.pre_factor(c, L)
    end 
    @inbounds Threads.@threads for n in 1:N
        cache = ConditionnalDensityCache{T}(view(weights, :, n), view(means, :, :, n), zeros(T, L), chol_covs, pre_factors)
        conditionnal_density!(gllim, view(X, :, n), cache)
    end
    weights, means
end




"""
 
Implementation for one observation
    chol_covs and pre_factors refer to the X covariances Gamma 
"""
function conditionnal_density!(gllim::GLLiM{T}, x::VecOrSub{T}, cache::ConditionnalDensityCache{T}) where {T} 
    K::Int, L::Int, D::Int = gllim.K, gllim.L, gllim.D

    @inbounds for k in 1:K
        Ak, b  = gllim.A[k], gllim.b
        @inbounds for d in 1:D 
            cache.out_means[d, k] = b[d, k]
            for l in 1:L 
                cache.out_means[d, k] +=  Ak[d,l] * x[l] 
            end
        end
        cache.out_weights[k] =  Probas.log_gaussian_density!(x, view(gllim.c, :, k), cache.chol_covs[k], L, cache.pre_factors[k], cache.allocated)
        cache.out_weights[k] += log(gllim.π[k]) 
    end 

    s = Probas.logsumexp(cache.out_weights)
    cache.out_weights .= exp.(cache.out_weights .- s) # so far we used log
end

# Convenience (not optimized) wrapper
function conditionnal_density(gllim::GLLiM{T}, x::VecOrSub{T}) where {T}
    K::Int, L::Int, D::Int = gllim.K, gllim.L, gllim.D
    cache = ConditionnalDensityCache{T}(gllim)
    conditionnal_density!(gllim, x, cache)
    cache.out_weights, cache.out_means
end

"""
 Shift the "basic" conditionnal density with the additionnal prior information 
 given by X0 and Gamma0
 X0 has size (L, N)
 Returns weights (K, N), means (_, K,N) and covs FullCovs (length K, N)
"""
function conditionnal_density_with_prior(weights::Matrix, means::CubeOrSub, covs::FullCovs{T}, X0::Matrix{T}, Gamma0::CovsType) where {T}
    L, K, N = size(means)
    out_weights, out_means, out_covs = zeros(size(weights)), zeros(size(means)), fill(FullCov{T}(L), (K, N))
    for i in 1:N 
        G0inv = inv(Gamma0[i])
        x0 = X0[:, i]
        s = 0.
        for k in 1:K 
            Sigkinv = inv(covs[k])
            Sk = inv(G0inv + Sigkinv)
            xk = Sk * (G0inv * x0 + Sigkinv * means[:, k,i])
            Bk = inv(Gamma0[i] + covs[k])
            betak = sqrt(det(Sk) / det(covs[k])) * exp(-0.5 * (xk - x0)' * Bk * (xk - x0)) # since |Gamma0| doesn't depend on k, it will be normalized
            out_weights[k,i] = betak * weights[k, i]
            out_means[:,k,i] = xk
            out_covs[k,i] = Sk
            s += out_weights[k,i]
        end
        out_weights[:, i] ./= s
    end
    out_weights, out_means, out_covs
end
conditionnal_density_with_prior(w, m, c, x, Gamma::CovType) = (N = size(x, 2); conditionnal_density_with_prior(w, m, c, x, [Gamma for n in 1:N]))

"""
Mean prediction, with covariance.
When used with gllim, X , gives F_gllim(x).
When used with gllim_star, Y gives F^-1_gllim(y)
"""
function predict(gllim::GLLiM, X::Matrix; with_cov = true)
    weights, means = conditionnal_density(gllim, X)
    covs = with_cov ? gllim.Σ : nothing
    Probas.mean_cov_mixtures(weights, means, covs)
end 


"""
Sample according to conditionnal law of X knowing Y.
Return samplings (L, `nb_samples`, N)
"""
function predict_sample(gllim_star::GLLiM{T}, Y::Matrix{T},
    nb_samples::Int) where {T}
    weights, means = conditionnal_density(gllim_star, Y)
    Probas.GMM_sampling(weights, means, gllim_star.Σ, nb_samples)
end


# ---------------------------------------------------------------------------------------- #
# ------------------------------------------ IO ------------------------------------------ #
# ---------------------------------------------------------------------------------------- #
"""
Export a gllim object as a Dict of parameters (suitable for Serialization) 
"""
function Base.Dict(gllim::GLLiM{T,gammaT,sigmaT}) where {T,gammaT,sigmaT} 
    Base.Dict("pi" => gllim.π, "c" => gllim.c, "Gamma" => gllim.Γ, 
"A" => gllim.A, "b" => gllim.b, "Sigma" => gllim.Σ)
end
"""
Load GLLiM from a Dict of parameters
"""
function GLLiM{T}(vars::Base.Dict) where {T}
    GLLiM{T}(vars["pi"], vars["c"], vars["Gamma"], vars["A"], vars["b"], vars["Sigma"])
end

