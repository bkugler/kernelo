# Implementation of EM algorithm for GLLiM and Hybrid GLLiM 

include("gllim.jl")

const DEFAULT_REG_COVAR = 1e-10

# ---------------------------------------------------------------------------------- #
# ------------------------------------- Meta --------------------------------------- #
# ---------------------------------------------------------------------------------- #
abstract type InitParams end 

"""
Performs `starts` initialisation by running running max_iteration_gllim on (X,Y).
GMM is initialised with random centers, and with covariance in 1 / (x_precision * K)
"""
struct MultiInit <: InitParams
    x_precision::Int 
    max_iteration_gllim::Int
    starts::Int
end
MultiInit(; x_precision = 10, max_iteration_gllim = 30, starts = 10) = MultiInit(x_precision, max_iteration_gllim, starts)

"""
 GLLiM is initialized with pi_k = 1/K, c_k a grid, Gamma_k = 1/( x_precision * K).
The corresponding rnk is computed for X only, and used to initialised GLLiM 
"""
struct FixedInit <: InitParams 
    x_precision::Int
end 
FixedInit(; x_precision = 10) = FixedInit(x_precision)

"""
    GLLiM is initialised by performing the M step, with given rnk. 
"""
struct XPartFixedInit{T <: Number} <: InitParams 
    """probas log(rnk) (size N x K)"""
    log_r::Matrix{T}
end


mutable struct GLLiMTraining <: EMMetaGLLiM
    init::InitParams
    max_iteration::Int64
    K::Int
    Ystd::Float64
    Ntrain::Int
    stopping_ratio::Float64 # log likelihood ratio need to stop early
end
GLLiMTraining(init::InitParams, K::Int; max_iteration = 50, Ystd = 0.001, Ntrain = 50000, stopping_ratio = 1e-5) = GLLiMTraining(init, max_iteration, K, Ystd, Ntrain, stopping_ratio)

Base.string(p::GLLiMTraining)  = invoke(string, EMMetaGLLiM, p) * string(p.stopping_ratio)


# ---------------------------------------------------------------------------------- #
# ------------------------------------- M step ------------------------------------- #
# ---------------------------------------------------------------------------------- #


"""
 Compute parameters of gaussian distribution W knowing Z : munk_W and Sk_W
Write munk_W_out shape (Lw, N) and Sk_W_out shape (Lw,Lw) 
"""
function _compute_rW_Z(Y::MatOrSub{Ty}, T::MatOrSub{Ty}, Ak_W::MatOrSub{Ty},
                                    Ak_T::MatOrSub{Ty}, Gammak_W::CovType{Ty}, Sigmak::CovType{Ty},
                                    bk::VecOrSub{Ty}, ck_W::VecOrSub{Ty}, munk_W_out::MatOrSub{Ty},
                                    Sk_W_out::MatOrSub{Ty}, Sk_X_out::MatOrSub{Ty}, tmp_D::VecOrSub{Ty},
                                    ATSinv::MatOrSub{Ty}) where {Ty}

    D, Lw = size(Ak_W)
    Lt, N = size(T)
    L = Lt + Lw 

    if Lw == 0
        Sk_X_out .= 0
        return
    end

    ginv = inv(Gammak_W)
    ATSinv .= Ak_W' * inv(Sigmak)

    Sk_W_out .= inv(ginv + ATSinv * Ak_W)
    Sk_X_out[1:Lt,:] .= 0

    @inbounds for l1 in Lt + 1:L
        Sk_X_out[l1,1:Lt] .= 0
        @inbounds for l2 in Lt + 1:L
            Sk_X_out[l1,l2] = Sk_W_out[l1 - Lt,l2 - Lt]
        end
    end

    @inbounds for n in 1:N
        tmp_D .= Ak_T * T[:,n]
        tmp_D .= view(Y, :, n) .- tmp_D .- bk
        munk_W_out[:,n] .= Sk_W_out * ((ATSinv * tmp_D) .+ (ginv * ck_W))
    end
end


function _compute_Xnk(T::MatOrSub{Ty}, munk::MatOrSub{Ty}, Xnk::MatOrSub{Ty}) where {Ty}
    Lw, _ = size(munk)
    Lt, N = size(T)
    @inbounds for n in 1:N
        @inbounds for lt in 1:Lt
            Xnk[lt,n] = T[lt,n]
        end
        @inbounds for lw in 1:Lw
            Xnk[Lt + lw,n] = munk[lw, n]
        end
    end
end

function _compute_cT(T::MatOrSub{Ty}, log_rnk::VecOrSub{Ty}, log_rk::Ty,
                         ck_T::VecOrSub{Ty}) where {Ty}
    Lt, N = size(T)
    @inbounds for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        @inbounds for l in 1:Lt
            ck_T[l] += s * T[l,n]
        end
    end
end

"""
 Gamma iso 
"""
function _compute_GammaT(T::MatOrSub{Ty}, log_rnk::VecOrSub{Ty}, log_rk::Ty, ck_T::VecOrSub{Ty},
                         Gamma_T::IsoCovs{Ty}, k::Int, tmp::VecOrSub{Ty}) where {Ty}
    Lt, N = size(T)
    gamma = 0.
    if Lt != 0
        @inbounds for n in 1:N
            _tmp = 0.
            s = exp(0.5 * (log_rnk[n] - log_rk))
            @inbounds for l in 1:Lt
                _tmp += (s * (T[l,n] - ck_T[l]))^2
            end
            _tmp /= Lt
            gamma += _tmp
        end
    end
    Gamma_T[k] = UniformScaling(gamma)
end

"""
 Gamma diag 
"""
function _compute_GammaT(T::MatOrSub{Ty}, log_rnk::VecOrSub{Ty}, log_rk::Ty, ck_T::VecOrSub{Ty},
                        Gamma_T::DiagCovs{Ty}, k::Int, tmp::VecOrSub{Ty}) where {Ty}
    Lt, N = size(T)
    gk = Gamma_T[k]
    @inbounds for n in 1:N
        s = exp(0.5 * (log_rnk[n] - log_rk))
        @inbounds for l in 1:Lt
            gk[l,l] += (s * (T[l,n] - ck_T[l]))^2
        end
    end
end

"""
 Gamma full 
"""
function _compute_GammaT(T::MatOrSub{Ty}, log_rnk::VecOrSub{Ty}, log_rk::Ty, ck_T::VecOrSub{Ty},
                                Gamma_T::FullCovs{Ty}, k::Int, tmp::VecOrSub{Ty}) where {Ty}
    Lt, N = size(T)
    gk = Gamma_T[k].data
    @inbounds for n in 1:N
        s = exp(0.5 * (log_rnk[n] - log_rk))
        @inbounds for l in 1:Lt
            tmp[l] = s * (T[l,n] - ck_T[l])
        end

        @inbounds for l1 in 1:Lt
            @inbounds for l2 in 1:Lt
                gk[l1,l2] += tmp[l1] * tmp[l2]
            end
        end
    end
end

function _add_numerical_stability(Covs::Union{FullCovs{Ty},DiagCovs{Ty}}, k::Int) where {Ty}
    D, _ = size(Covs[k])
    @inbounds for d in 1:D
        Covs[k][d,d] += DEFAULT_REG_COVAR
    end
end

function _add_numerical_stability(Covs::IsoCovs{Ty}, k::Int) where {Ty}
    Covs[k] = UniformScaling(Covs[k] + DEFAULT_REG_COVAR)
end



function _compute_Ak(Y::MatOrSub{Ty}, Xnk::MatOrSub{Ty}, log_rnk::VecOrSub{Ty},
                      log_rk::Ty, Sk_X::MatOrSub{Ty}, Ak::MatOrSub{Ty},
                      xk_bar::VecOrSub{Ty}, yk_bar::VecOrSub{Ty}, X_stark::MatOrSub{Ty},
                      Y_stark::MatOrSub{Ty}, YXt_stark::MatOrSub{Ty}) where {Ty}

    L, _ = size(Xnk)
    D, N = size(Y)
    s = 0.
    sum = 0.
    @inbounds for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        @inbounds for l in 1:L
            xk_bar[l] += s * Xnk[l,n]
        end
        @inbounds for d in 1:D
            yk_bar[d] += s * Y[d,n]
        end
        sum += s
    end
    @inbounds for n in 1:N
        s = exp(0.5 * (log_rnk[n] - log_rk))
        @inbounds for l in 1:L
            X_stark[l,n] = s * (Xnk[l,n] -  xk_bar[l]) # (33)
        end

        @inbounds for d in 1:D
            Y_stark[d,n] = s  * (Y[d,n] - yk_bar[d])  # (34)
        end
    end
    M = Sk_X .+ X_stark * X_stark'
    try
        Ak .= Y_stark * X_stark' * pinv(M) # (31)
    catch e 
        # we keep old Ak
    end
end


function _compute_bk(Y::MatOrSub{Ty}, Xnk::MatOrSub{Ty},
                log_rnk::VecOrSub{Ty}, log_rk::Ty, Ak::MatOrSub{Ty},
                bk::VecOrSub{Ty}) where {Ty}
    D, N = size(Y)
    L, _ = size(Xnk)

    @inbounds for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        @inbounds for d in 1:D
            u = 0.
            @inbounds for l in 1:L
                u += Ak[d,l] * Xnk[l,n]
            end
            bk[d] += s * (Y[d,n] - u)
        end
    end
end

"""
 Sigma full 
"""
function _compute_Sigmak(Y::MatOrSub{Ty}, Xnk::MatOrSub{Ty}, log_rnk::VecOrSub{Ty},
                          log_rk::Ty, Ak::MatOrSub{Ty}, bk::VecOrSub{Ty},
                          Sk_W::MatOrSub{Ty}, tmp::VecOrSub{Ty}, Sigmak::FullCovs, k::Int) where {Ty}

    
    D, N = size(Y)
    Lw, _  = size(Sk_W)
    L, _ = size(Xnk)
    Lt = L - Lw
    sk = Sigmak[k].data
    @inbounds for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        @inbounds for d in 1:D
            u = 0.
            @inbounds for l in 1:L
                u += Ak[d,l] * Xnk[l,n]
            end
            tmp[d] = (Y[d,n] - u - bk[d])
        end

        @inbounds for d1 in 1:D
            @inbounds for d2 in 1:D
                sk[d1,d2] += s * tmp[d1] * tmp[d2]
            end
        end
    end

    @inbounds for d1 in 1:D
        @inbounds for d2 in 1:D
            @inbounds for i in 1:Lw
                @inbounds for j in 1:Lw  # Ak_W
                    sk[d1,d2] += Ak[d1, i + Lt] * Sk_W[i,j] * Ak[d2,j + Lt]
                end
            end
        end
    end
end

"""
 Sigma diag 
"""
function _compute_Sigmak(Y::MatOrSub{Ty}, Xnk::MatOrSub{Ty}, log_rnk::VecOrSub{Ty},
                                log_rk::Ty, Ak::MatOrSub{Ty}, bk::VecOrSub{Ty},
                                Sk_W::MatOrSub{Ty}, tmp::VecOrSub{Ty}, Sigmak::DiagCovs, k::Int) where {Ty}
    D, N = size(Y)
    Lw, _  = size(Sk_W)
    L, _ = size(Xnk)
    Lt = L - Lw
    sk = Sigmak[k]
    for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        for d in 1:D
            u = 0.
            for l in 1:L
                u += Ak[d,l] * Xnk[l,n]
            end
            sk[d,d] += s * ((Y[d,n] - u - bk[d])^2)
        end
    end
    for d in 1:D
        for i in 1:Lw
            for j in 1:Lw # Ak_W
                sk[d,d] += Ak[d, i + Lt] * Sk_W[i,j] * Ak[d,j + Lt]
            end
        end
    end
end

"""
 Sigma iso 
"""
function _compute_Sigmak(Y::MatOrSub{Ty}, Xnk::MatOrSub{Ty}, log_rnk::VecOrSub{Ty},
                        log_rk::Ty, Ak::MatOrSub{Ty}, bk::VecOrSub{Ty},
                        Sk_W::MatOrSub{Ty}, tmp::VecOrSub{Ty}, Sigmak::IsoCovs, k::Int) where {Ty}
    D, N = size(Y)
    Lw, _  = size(Sk_W)
    L, _ = size(Xnk)
    Lt = L - Lw
    sig = 0.
    for n in 1:N
        s = exp(0.5 * (log_rnk[n] - log_rk))
        for d in 1:D
            u = 0.
            for l in 1:L
                u += Ak[d,l] * Xnk[l,n]
            end
            sig += (s * (Y[d,n] - u - bk[d]))^2
        end
    end
    for d in 1:D
        for i in 1:Lw
            for j in 1:Lw  # Ak_W
                sig += Ak[d, i + Lt] * Sk_W[i,j] * Ak[d,j + Lt]
            end
        end
    end
    Sigmak[k] = UniformScaling(sig / D)
end


function _create_tmps(Lt, Lw, D, N, _::Ty) where {Ty <: AbstractFloat}
    L = Lt + Lw
    xk_bar = zeros(Ty, L)  
    yk_bar = zeros(Ty, D)  
    X_stark = zeros(Ty, L, N)  
    Y_stark = zeros(Ty, D, N)
    YXt_stark = zeros(Ty, D, L)
    inv_tmp = zeros(Ty, L, L)

    munk = zeros(Ty, Lw, N)
    Xnk = zeros(Ty, L, N) 
    tmp_Lt = zeros(Ty, Lt)  
    tmp_Lw = zeros(Ty, Lw)  
    tmp_D = zeros(Ty, D)  

    Sk_W = zeros(Ty, Lw, Lw) 
    Sk_X = zeros(Ty, L, L)

    ATSinv_tmp = zeros(Ty, Lw, D)

    (munk, Sk_W, Sk_X, Xnk, tmp_Lt, tmp_D, xk_bar, yk_bar, X_stark, Y_stark, YXt_stark, ATSinv_tmp,
         tmp_Lw)
end 



_allocate_covs(dim, K, ::Type{FullCovs{Ty}}) where {Ty} = [ Symmetric(zeros(Ty, dim, dim)) for k in 1:K]
_allocate_covs(dim, K, ::Type{DiagCovs{Ty}}) where {Ty} = [ Diagonal(zeros(Ty, dim)) for k in 1:K]
_allocate_covs(dim, K, ::Type{Vector{Ty}}) where {Ty} = zeros(Ty, K)


"""
 Gamma Full and Sigma Full - Multi-threading
"""
function next_theta(T::MatOrSub{Ty}, Y::MatOrSub{Ty}, log_rnk::MatOrSub{Ty}, GammaW::GT, Sigma::ST, Lw,
                    AW::Union{Vector{Matrix{Ty}},Nothing}, AT::Union{Vector{Matrix{Ty}},Nothing},  
                    b::Union{MatOrSub{Ty},Nothing}, cW::Union{MatOrSub{Ty},Nothing}) where 
                    {Ty,GT <: CovsType{Ty},ST <: CovsType{Ty}}


    N, K = size(log_rnk)
    Lt, _ = size(T)
    D, _ = size(Y)
    L = Lt + Lw

    # Out allocation 
    log_pi_out = zeros(Ty, K)
    c_out = zeros(Ty, Lt, K) 
    A_out = [ zeros(Ty, D, L) for k in 1:K ]
    b_out = zeros(Ty, D, K)

    Γ_out = _allocate_covs(Lt, K, GT)
    Σ_out = _allocate_covs(D, K, ST)

    # @inbounds for k in 1:K
    @inbounds Threads.@threads for k in 1:K
        log_rk = Probas.logsumexp(log_rnk[:,k], N)
        if log_rk == -Inf  # degenerate cluster 
            log_pi_out[k] = -Inf
            _add_numerical_stability(Γ_out, k)
            _add_numerical_stability(Σ_out, k)
            @warn "degenerate cluster" k
            continue
        end
        # Temporary Memory allocation
        munk, Sk_W, Sk_X, Xnk, tmp_Lt, tmp_D, xk_bar, yk_bar, X_stark, Y_stark, YXt_stark, ATSinv_tmp,
        tmp_Lw = _create_tmps(Lt, Lw, D, N, one(Ty))

        log_pi_out[k] = log_rk - log(N)

        if AW !== nothing
            _compute_rW_Z(Y, T, AW[k], AT[k], GammaW[k], Sigma[k], b[:,k], cW[:,k], munk, Sk_W, Sk_X,  
                            tmp_D, ATSinv_tmp)
        end

        _compute_cT(T, view(log_rnk, :, k), log_rk, view(c_out, :, k))
        
        _compute_GammaT(T, view(log_rnk, :, k), log_rk, view(c_out, :, k), Γ_out, k, tmp_Lt)
        _add_numerical_stability(Γ_out, k)

        _compute_Xnk(T, munk, Xnk)

        _compute_Ak(Y, Xnk, view(log_rnk, :, k), log_rk, Sk_X, A_out[k],
                    xk_bar,  yk_bar,  X_stark, Y_stark,  YXt_stark)

        _compute_bk(Y, Xnk, view(log_rnk, :, k), log_rk, A_out[k], view(b_out, :, k))

        _compute_Sigmak(Y, Xnk, view(log_rnk, :, k), log_rk, A_out[k], view(b_out, :, k),
                              Sk_W, tmp_D, Σ_out, k)
        _add_numerical_stability(Σ_out, k)
    end
    log_pi_out, c_out, Γ_out, A_out, b_out, Σ_out
end

"""
 No hybrid. GammaW and Sigma only for dispatch : their size and values dont matter, only their type.
"""
next_theta(T, Y, log_rnk, GammaW::CovsType, Sigma::CovsType) = next_theta(T, Y, log_rnk, GammaW, Sigma, 0, nothing, nothing,  nothing, nothing)


# ---------------------------------------------------------------------------------- #
# -------------------------------------- E step ------------------------------------ #
# ---------------------------------------------------------------------------------- #


__sigma_step(data_out::Matrix{T}, d1::Int, d2::Int, Σk::IsoCov{T}) where {T} = (d1 == d2) ? (data_out[d1,d2] = Σk.λ ) : (data_out[d1,d2] = 0)
__sigma_step(data_out::Matrix{T}, d1::Int, d2::Int, Σk::DiagCov{T}) where {T} = (d1 == d2) ? (data_out[d1,d2] = Σk[d1,d1]) : (data_out[d1,d2] = 0)
__sigma_step(data_out::Matrix{T}, d1::Int, d2::Int, Σk::FullCov{T}) where {T} = data_out[d1,d2] = Σk[d1,d2]

function __AGA_step(data_out::Matrix{T}, d1::Int, d2::Int, AkW::Matrix{T}, ΓkW::IsoCov{T}, Lw::Int) where {T} 
    @inbounds for i in 1:Lw
        data_out[d1,d2] += AkW[d1,i] * ΓkW * AkW[d2,i]
    end
end
function __AGA_step(data_out::Matrix{T}, d1::Int, d2::Int, AkW::Matrix{T}, ΓkW::DiagCov{T}, Lw::Int) where {T} 
    @inbounds for i in 1:Lw
        data_out[d1,d2] += AkW[d1,i] * ΓkW[i,i] * AkW[d2,i]
    end
end
function __AGA_step(data_out::Matrix{T}, d1::Int, d2::Int, AkW::Matrix{T}, ΓkW::FullCov{T}, Lw::Int) where {T} 
    @inbounds for i in 1:Lw
        @inbounds for j in 1:Lw
            data_out[d1,d2] += AkW[d1,i] * ΓkW[i,j] * AkW[d2,i]
        end
    end
end

function _compute_AGA_S(AkW, ΓkW, Σk, out)
    D, Lw = size(AkW)
    data_out = out.data
    @inbounds for d1 in 1:D
        @inbounds for d2 in 1:d1
            __sigma_step(data_out, d1, d2, Σk)
            __AGA_step(data_out, d1, d2, AkW, ΓkW, Lw)
            data_out[d2,d1] = data_out[d1,d2]
        end
    end
end


"""
 Compute the density of (x,y). Returns rnk and log(phi(x,y))
"""
function next_rnk(T::MatOrSub{Ty}, Y::MatOrSub{Ty}, log_π::VecOrSub{Ty},
                cT::MatOrSub{Ty}, ΓT::CovsType{Ty}, AT::Vector{Matrix{Ty}}, 
                b::MatOrSub{Ty},  Σ::CovsType{Ty}, Lw::Int,
                AW::Vector{Matrix{Ty}}, cW::MatOrSub{Ty}, ΓW::CovsType{Ty}) where {Ty}

    Lt, K = size(cT)
    D, N = size(Y)
    L = Lt + Lw

    out_log_rnk = zeros(Ty, N, K)
    out_logll = zeros(Ty, N)

    NTHREADS = Threads.nthreads()
    tmp_DNs = zeros(Ty, D, N, NTHREADS)
    tmp_DDs = [Symmetric(zeros(Ty, D, D)) for t in 1:NTHREADS]
    tmp_Ns = zeros(Ty, N, NTHREADS)

    # @inbounds for k in 1:K
    @inbounds Threads.@threads for k in 1:K
        tid = Threads.threadid()
        Probas.log_gaussian_density!(T, view(cT, :, k), ΓT[k], view(out_log_rnk, :, k))

        ATk, AWk = AT[k], AW[k]
        @inbounds for n in 1:N
            @inbounds for d in 1:D
                tmp_DNs[d,n, tid] = 0
                @inbounds for i in 1:Lt
                    tmp_DNs[d,n, tid] += ATk[d,i] * T[i,n]
                end
                @inbounds for i in 1:Lw
                    tmp_DNs[d,n, tid] += AWk[d,i] * cW[k][i]
                end
                tmp_DNs[d,n, tid] += b[d,k]
            end
        end

        _compute_AGA_S(AWk, ΓW[k], Σ[k], tmp_DDs[tid])

        Probas.log_gaussian_density!(Y, view(tmp_DNs, :, :, tid), tmp_DDs[tid], view(tmp_Ns, :, tid))
        log_pik = log_π[k]
        
        @inbounds for n in 1:N
            out_log_rnk[n,k] += tmp_Ns[n, tid] + log_pik  # in log
        end

    end
    Probas.normalize_log!(out_log_rnk, out_logll)
    out_log_rnk, out_logll
end
"""
 Convenience function for standard GLLiM (Lw = 0)
"""
function next_rnk(X, Y, log_π, c, Γ, A, b, Σ) 
    Lw = 0 
    D, K = size(b)
    cW = zeros(Lw, K)
    ΓW = [UniformScaling(1.) for k in 1:K]
    AW = [zeros(D, Lw) for k in 1:K]
    next_rnk(X, Y, log_π, c, Γ, A, b, Σ, Lw, AW, cW, ΓW)
end

# ---------------------------------------------------------------------------------- #
# --------------------------------- Full algorithm --------------------------------- #
# ---------------------------------------------------------------------------------- #

function _has_converged(params::GLLiMTraining, init_logll, previous_logll, current_logll, current_iter)
    isfinite(current_logll) || return true
    current_iter < 3 && return false
    current_iter > params.max_iteration && return true

    delta_total = current_logll - init_logll
    delta = current_logll - previous_logll
    (delta < 0 || delta_total < 0) && return false
    (delta / delta_total) < params.stopping_ratio
end

_init_GW(gllim::GLLiM{T,IsoCovs{T}}, Lw) where {T} = [UniformScaling(T(1)) for k in 1:gllim.K]
_init_GW(gllim::GLLiM{T,DiagCovs{T}}, Lw) where {T} = [ones(gllim.Lw) for k in 1:gllim.K]
_init_GW(gllim::GLLiM{T,FullCovs{T}}, Lw) where {T} = [Symmetric(Matrix(Diagonal(ones(Lw)))) for k in 1:gllim.K]


"""
Run training on an already initialized GLLiM 
"""
function train(gllim::GLLiM{T,GammaT,SigmaT}, X::Matrix{T}, Y::Matrix{T}, 
    params::GLLiMTraining; Lw::Int = 0, return_log_likelihood = false) where {T,GammaT <: CovsType{T},SigmaT <: CovsType{T}}

    Lt, K = gllim.L, gllim.K
    L = Lt + Lw
    @info "Running $(params.max_iteration) iter. GLLiM with $K Gaussians in $(L + gllim.D) dimmensions"

    # Means and Covariances of W fixed for non-identifiability issue
    cW = zeros(Lw, K)
    ΓW::GammaT = _init_GW(gllim, Lw)
    
    # Initialization
    log_π, cT, ΓT, AT, b, Σ = log.(gllim.π), gllim.c, gllim.Γ, gllim.A, gllim.b, gllim.Σ
    AW::Vector{Matrix{T}} = [zeros(gllim.D, Lw) for k in 1:K]
    log_rnk, logll = next_rnk(X, Y, log_π, cT, ΓT, AT, b, Σ, Lw, AW, cW, ΓW)

    init_logll = sum(logll)
    current_logll = init_logll

    _, N = size(X)
    current_iter = 0
    lls = []
    while true
        current_iter += 1
        @info "iter. $(current_iter) average log likelihood : $(current_logll / N)"
        
        # save log_likehood for possible export
        push!(lls, current_logll)

        log_π, cT, ΓT, A, b, Σ = next_theta(X, Y, log_rnk, ΓW, Σ, Lw, AW, AT, b, cW)
        AT, AW = [a[:,1:Lt] for a in A], [a[:,Lt + 1:L] for a in A]
        log_rnk, logll = next_rnk(X, Y, log_π, cT, ΓT, AT, b, Σ, Lw, AW, cW, ΓW)
     
        # update logll trackers
        previous_logll = current_logll 
        current_logll = sum(logll)
        if isnan(current_logll)  
            @warn "degenerate log-likelihood !"
        end
        _has_converged(params, init_logll, previous_logll, current_logll, current_iter) && break
    end
    out = GLLiM{T,GammaT,SigmaT}(exp.(log_π), cT, ΓT, AT, b, Σ)
    if Lw > 0
        if return_log_likelihood
            return out, AW, lls 
        else
            return out, AW
        end
    else
        if return_log_likelihood
            return out, lls
        else
            return out
        end
    end
end
