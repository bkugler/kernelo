using Sobol 

include("gllim_training.jl")

import GaussianMixtures

# ---------------------------------------------------------------------------------- #
# ------------------------------------- Meta --------------------------------------- #
# ---------------------------------------------------------------------------------- #

struct JointGMMTraining <: EMMetaGLLiM
    init::InitParams
    K::Int
    max_iteration::Int
    Ystd::Float64
    Ntrain::Int
end
JointGMMTraining(init::InitParams, K; max_iteration = 100, Ystd = 0., Ntrain = 50000) = JointGMMTraining(init, K, max_iteration, Ystd, Ntrain)

struct SimpleJointGMMTraining <: EMMetaGLLiM
    init::InitParams
    K::Int
    max_iteration::Int
    Ystd::Float64
    Ntrain::Int
end
SimpleJointGMMTraining(init::InitParams, K; max_iteration = 100, Ystd = 0.001, Ntrain = 50000) = SimpleJointGMMTraining(init, K, max_iteration, Ystd, Ntrain)

"""
 The noise std in the forward mode is taken as Y / Ystd_ratio 
"""
struct VariableNoiseSimpleJointGMMTraining <: EMMetaGLLiM
    init::InitParams
    K::Int
    max_iteration::Int
    Ystd_ratio::Float64
    Ntrain::Int
end
VariableNoiseSimpleJointGMMTraining(init::InitParams, K; max_iteration = 100, Ystd_ratio = 10, Ntrain = 50000) = VariableNoiseSimpleJointGMMTraining(init, K, max_iteration, Ystd_ratio, Ntrain)

to_full = GaussianMixtures.covar
to_chol = GaussianMixtures.cholinv


"""
GMM vers GLLiM 
L is X size. gmm is as GaussianMixture on (X,Y)
"""
function GLLiM(gmm::GaussianMixtures.GMM{T,CT}, L::Int) where {T,CT <: GaussianMixtures.FullCov{T}}
    V = [to_full(s) for s in gmm.Σ] # from cholesky
    K, LplusD = gmm.n, gmm.d
    D = LplusD - L
    pi = gmm.w
    c = gmm.μ[:, 1:L]
    Gamma = [Symmetric(m[1:L, 1:L]) for m in V]
    V_xy = [m[1:L, (L + 1):LplusD] for m in V]
    V_xyT = [m' for m in V_xy]
    A = Array{T}(undef, K, D, L)
    b = Array{T}(undef, K, D)
    Sigma = Array{T}(undef, K, D, D)
    for k in 1:K
        v, g, ck = V_xyT[k], Gamma[k], c[k,:]
        Ak = v * inv(g)
        A[k,:,:] = Ak
        b[k,:] = gmm.μ[k, (L + 1):LplusD] .- (Ak * ck)
        Sigma[k,:,:] = V[k][(L + 1):LplusD, (L + 1):LplusD] .- Ak * g * Ak'
    end
    GLLiM{T,FullCovs{T},FullCovs{T}}(pi, Matrix(c'), Gamma, [A[k,:,:] for k in 1:K],
    Matrix(b'), [Symmetric(Sigma[k,:,:]) for k in 1:K])
end

"""
    GLLiM vers GMM 
"""
function GaussianMixtures.GMM(gllim::GLLiM{T}) where {T}
    K, LplusD = gllim.K, gllim.L + gllim.D
    rho = gllim.π
    m = Matrix{T}(undef, K, gllim.L + gllim.D)
    V = Array{T}(undef, K, LplusD, LplusD)
    for k in 1:K
        Ak, Gk = gllim.A[k], gllim.Γ[k]
        m[k,:] = vcat(gllim.c[:,k], Ak * gllim.c[:,k] .+ gllim.b[:,k])
        AGk = Ak * Gk
        Vyk = gllim.Σ[k] + AGk * (Ak')
        V[k,:,:] = cat(cat(Gk, AGk', dims = 2), cat(AGk, Vyk, dims = 2), dims = 1)
    end
    hist = [GaussianMixtures.History("Initialization (from GLLiM) n=$(K), d=$(LplusD), kind=$(:full)")]
    GaussianMixtures.GMM(rho, m, [to_chol(V[k,:,:]) for k in 1:K], hist, 0)
end


"""
Performs joint GMM training on given gllim and returns gllim
"""
function train(gllim::GLLiM{T,GammaT,SigmaT}, X::Matrix{T}, Y::Matrix{T}, 
                params::JointGMMTraining) where {T,GammaT,SigmaT}
    gmm = GaussianMixtures.GMM(gllim)
    L, _  = size(X)
    x = Matrix(vcat(X, Y)')
    @time GaussianMixtures.em!(gmm, x, nIter = params.max_iteration, debug = 0)
    GLLiM(gmm, L)
end



function init_GLLIM(gllim::GLLiM{T,gammaT,sigmaT}, X::MatOrSub{T}, Y::MatOrSub{T}, params::MultiInit) where {T,gammaT <: CovsType{T},sigmaT <: CovsType{T}} 
    K, L = gllim.K, gllim.L
    D, N = size(Y)
    bestll, bestp, bestlogrnk = -Inf, nothing, zeros(N, K)
    dummy_Γ = gammaT(K, L) # to dispatch
    dummy_Σ = sigmaT(K, D) # idem 
    for i in 1:params.starts
        rho = ones(T, K) ./ K
        m = rand(K, L)
        cov_value = sqrt(1 / (K^(1 / L)))
        # GaussianMixtures expects the inverse cholesky decomposition
        Γ = [ UpperTriangular(sqrt.(Matrix(Diagonal(ones(L) ./ cov_value)))) for k in 1:K ]
        gmm = GaussianMixtures.GMM(rho, m, Γ, [], 0)
        GaussianMixtures.em!(gmm, Matrix(X'), nIter = 1, debug = 0)
        rnk = GaussianMixtures.gmmposterior(gmm, Matrix(X'))[1]
        log_rnk, log_ll, p = log.(rnk), nothing, nothing
        p = next_theta(X, Y, log_rnk, dummy_Γ, dummy_Σ)
        @debug "Start $(i) / $(params.starts) : running $(params.max_iteration_gllim) GLLiM iter..." 
        for j in 1:params.max_iteration_gllim
            log_rnk, log_ll = next_rnk(X, Y, p...)
            p = next_theta(X, Y, log_rnk, dummy_Γ, dummy_Σ)
        end
        s = sum(log_ll)
        @debug "Start $(i) : log likelihood : $(s)" 
        if s >= bestll 
            bestp = p
            bestll = s 
            bestlogrnk = log_rnk
        end
    end
    bestp = (exp.(bestp[1]), bestp[2:end]...) # next_theta return log_pi
    GLLiM{T,gammaT,sigmaT}(bestp...), bestlogrnk
end

function init_GLLIM(gllim::GLLiM{T,gammaT,sigmaT}, X::MatOrSub{T}, Y::MatOrSub{T}, params::FixedInit) where {T,gammaT <: CovsType{T},sigmaT <: CovsType{T}}
    K, L, D = gllim.K, gllim.L, gllim.D
    s = SobolSeq(L)
    m = Matrix(hcat([next!(s) for i in 1:K]...)')
    rho = ones(T, K) ./ K
    Γ = [ UpperTriangular(sqrt.(Matrix(Diagonal(ones(L) .* (1 / params.x_precision / K))))) for k in 1:K ]
    gmm = GaussianMixtures.GMM(rho, m, Γ, [], 0)
    # GaussianMixtures.em!(gmm, Matrix(X'), nIter = 1, debug = 0)
    rnk = GaussianMixtures.gmmposterior(gmm, Matrix(X'))[1]
    dummy_Γ = gammaT(K, L) # to dispatch
    dummy_Σ = sigmaT(K, D) # idem 
    log_rnk = log.(rnk)
    p = next_theta(X, Y, log_rnk, dummy_Γ, dummy_Σ)
    p = (exp.(p[1]), p[2:end]...) # next_theta return log_pi
    GLLiM{T,gammaT,sigmaT}(p...), log_rnk
end

function init_GLLIM(gllim::GLLiM{T,gammaT,sigmaT}, X::MatOrSub{T}, Y::MatOrSub{T}, params::XPartFixedInit) where {T,gammaT <: CovsType{T},sigmaT <: CovsType{T}} 
    K, L, D = gllim.K, gllim.L, gllim.D
    dummy_Γ = gammaT(K, L) # to dispatch
    dummy_Σ = sigmaT(K, D) # idem
    p = next_theta(X, Y, params.log_r, dummy_Γ, dummy_Σ)
    p = (exp.(p[1]), p[2:end]...) # next_theta return log_pi
    GLLiM{T,gammaT,sigmaT}(p...), params.log_r
end



# ---------------------------------------------------------------------------------------------- #
# ---- Re implementation, simpler, with multi threading. Add covariance numerical stability ---- #
# ---------------------------------------------------------------------------------------------- #
const REG_COVAR = 1e-7

function _e_step!(X::MatOrSub{T}, pi::Vector{T}, c::Matrix{T}, Sigma::FullCovs{T},
                out_logrnk::MatOrSub{T}, out_rk::VecOrSub{T}) where {T}
    N, K = size(out_logrnk)
    # for k in 1:K
    @inbounds Threads.@threads for k in 1:K
        Probas.log_gaussian_density!(X, view(c, :, k), Sigma[k], view(out_logrnk, :, k)) # so far in log
        @inbounds for n in 1:N
            out_logrnk[n,k] += log(pi[k])
        end
        out_rk[k] = 0. # reset
    end
    
    @inbounds for n in 1:N  # sum et normalisation 
        log_rk = Probas.logsumexp(view(out_logrnk, n, :), K)
        @inbounds for k in 1:K
            out_logrnk[n,k] -= log_rk 
            out_rk[k] += exp(out_logrnk[n,k])
        end
    end
end

function _m_step!(X::MatOrSub{T}, log_rnk::MatOrSub{T}, rk::VecOrSub{T},
                    pi_out, c_out, Sigma_out) where {T}
    N, K = size(log_rnk)
    L, _ = size(c_out)
    # for k in 1:K
    @inbounds Threads.@threads for k in 1:K
        pi_out[k] = rk[k] / N
        tmp_c = zeros(T, L)
        tmp_Sigma = zeros(T, L, L)
        coeffs = zeros(T, N)
        @inbounds for n in 1:N 
            coeff = exp(log_rnk[n,k] - log(rk[k]))
            @inbounds for l in 1:L 
                tmp_c[l] += coeff * X[l,n] 
            end
            coeffs[n] = coeff
        end
        u = zeros(T, L)
        @inbounds for n in 1:N 
            coeff = coeffs[n]
            @inbounds for l in 1:L 
                u[l] = tmp_c[l] - X[l,n]
            end
            @inbounds for l1 in 1:L 
                for l2 in 1:l1  
                    tmp_Sigma[l2,l1] += coeff * u[l2] * u[l1]
                end
            end
        end
        @inbounds for l1 in 1:L  # numerical regularisation
            tmp_Sigma[l1,l1] += REG_COVAR
        end
        c_out[:,k] = tmp_c
        Sigma_out[k] = Symmetric(tmp_Sigma)
    end
    
end

function em(gmm::GaussianMixtures.GMM{T}, X::Matrix{T}, max_iteration::Int) where {T}
    pi, c, Sigma = copy(gmm.w), Matrix(gmm.μ'), [Symmetric(to_full(s)) for s in gmm.Σ]
    _, N = size(X)
    L, K = size(c)
    @info("Running $max_iteration iterations EM with $K Gaussians in $L dimensions")
    out_logrnk, out_rk = zeros(T, N, K), zeros(T, K)
    for i in 1:max_iteration
        _e_step!(X, pi, c, Sigma, out_logrnk, out_rk)
        _m_step!(X, out_logrnk, out_rk, pi, c, Sigma)
    end
    GaussianMixtures.GMM(pi, Matrix(c'), [to_chol(Matrix(S)) for S in Sigma], [], 0)
end

"""
Performs joint GMM training on given gllim and returns gllim
"""
function train(gllim::GLLiM{T}, X::Matrix{T}, Y::Matrix{T}, 
                params::Union{SimpleJointGMMTraining,VariableNoiseSimpleJointGMMTraining}) where {T}
    gmm = GaussianMixtures.GMM(gllim) # on passe de gllim à gmm 
    L, _  = size(X)
    x = vcat(X, Y)
    @time gmm = em(gmm, x, params.max_iteration) # on applique EM pour les gmm
    GLLiM(gmm, L) # on récupère gllim
end

function _test()
    L, D, K, N  = 4, 10, 40, 40000
    X = rand(L, N)
    Y = rand(D, N) .+ 2
    
    pi = rand(K) 
    pi /= sum(pi)
    c = rand(L + D, K)
    Sigma = [ (u = LowerTriangular(rand(L + D, L + D)) .+ 1; Symmetric(u * u')) for k in 1:K ]
    out_logrnk = zeros(N, K)
    out_rk = zeros(K)
    _e_step!(vcat(X, Y), pi, c, Sigma, out_logrnk, out_rk)
    @time _e_step!(vcat(X, Y), pi, c, Sigma, out_logrnk, out_rk)

    _m_step!(X, out_logrnk, out_rk, pi, c, Sigma)
    @time _m_step!(X, out_logrnk, out_rk, pi, c, Sigma)
    
    # gllim = GLLiM(K, X, Y)
    # pg = GLLiMParams{JointGMMTraining}(max_iteration = 10)
    # pg2 = GLLiMParams{SimpleJointGMMTraining}(max_iteration = 10)
    # train(gllim, X, Y, pg)
    # g1 = train(gllim, X, Y, pg);
    # train(gllim, X, Y, pg2)
    # g2 = train(gllim, X, Y, pg2);
 
end

# _test();