using LinearAlgebra
using Distributions
using Random

using ..Probas

export IsProposition, IsModel, IsInversion, IsDiagnostics

include("proposition.jl")

"""
    Define the target and proposition densities 
"""
abstract type IsModel end 

"""
    Returns the dimension of the spaces X and Y
"""
get_L(m::IsModel) = error("get_L not implemented")

"""
    Returns the parameters that define the proposition density 
"""
get_proposition(m::IsModel)::IsProposition = error("get_proposition not implemented")

"""
    Computes the target density at `x`, the sampled point.
    `j` is the index of the sample, used for example to cache
    intermediate values
"""
log_p(m::IsModel, x::VecOrSub, j::Int) = error("log_p not implemented")


struct IsDiagnostics
    ess::Number
    es::Number
    qn::Number
end

"""
 Detailled result of the inversion procedure, for one observation, 
 and one variant
"""
struct IsInversion 
    mean::Vector
    cov::Vector # diagonal only 
    diagnostics::IsDiagnostics
end

"""
    Performs importance sampling.

    Write in `samples` and `weights` and returns diagnostics. 
"""
function importance_sampling!(model::IsModel, Xs::MatOrSub{T}, weights::VecOrSub{T})::IsDiagnostics where {T <: Number}
    prop = get_proposition(model)
    sum_ws, es, ess = 0., 0, 0. 
    L, Ns = size(Xs)


    tmp = zeros(T, L)
    log_qx = zeros(T, Ns) 
    max_log_px = -Inf # re scaling to avoid numerical issues

    @inbounds for j in 1:Ns
        x = view(Xs, :, j)

        # sample 
        sampling!(prop, x, tmp, L)

        # compute target density 
        weights[j] = log_p(model, x,  j)
        if weights[j] != -Inf
            es += 1
        end
        if weights[j] > max_log_px
            max_log_px = weights[j]
        end
        log_qx[j] = log_q(prop, x, tmp)
    end

    max_w, sum_w2 = 0., 0.
    # for j in 1:Ns 
    @fastmath @inbounds for j in 1:Ns 
        w = exp(weights[j] -  max_log_px - log_qx[j])
        weights[j] = w    # weight computation in log_px to save memory
        sum_ws += w
        sum_w2 += w^2
        if w >= max_w
            max_w = w 
        end
    end
    
    ess = sum_ws^2 / sum_w2 
    qn = max_w / sum_ws
    weights ./= sum_ws

    IsDiagnostics(ess, es == 0 ? 0.1 : es, qn)
end

"""
    Use importance sampling to compute the mean and the covariance of the target density
"""
function importance_sampling(model::IsModel, Ns::Int)::IsInversion
    L = get_L(model)
    Xs = zeros(L, Ns)
    weights = zeros(Ns)
    diagnostic = importance_sampling!(model, Xs, weights)

    outMean, outCov = zeros(L), zeros(L)
    
    # Mean predictor 
    for j in 1:Ns
        for l in 1:L
            outMean[l] += weights[j] * Xs[l, j]
        end
    end
    
    # Variance predictor (diagonal)
    tmp = zeros(L)
    @fastmath @inbounds for j in 1:Ns 
        for l in 1:L
            tmp[l] = Xs[l, j] - outMean[l]    
        end
        for l in 1:L 
            outCov[l] += weights[j]  * tmp[l]^2 
        end   
    end
    IsInversion(outMean, outCov, diagnostic)
end

