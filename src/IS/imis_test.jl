using LinearAlgebra
using ..Probas
using ..Is

struct SimpleGaussianModel <: Is.IsModel
end

Is.get_L(m::SimpleGaussianModel) = 1
Is.get_proposition(m::SimpleGaussianModel) = Is.UniformProposition(1)
Is.log_p(m::SimpleGaussianModel, x::VecOrSub, _::Int) = Probas.log_gaussian_density(x, [0.5], UniformScaling(0.05), 1)

Xs, weigths = Is.imis(Is.ImisParams(100, 50, 10), SimpleGaussianModel())
@show size(Xs)
@show sum(weigths)

