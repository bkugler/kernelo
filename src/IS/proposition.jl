using ..Probas

"""
    Define the proposition density
    Must implement `log_q` and `sampling!`.
    Should implement `covariance` to be use in IMIS.
"""
abstract type IsProposition end 

"""
    Returns the log of the proposition density.

    `tmp` may be used a storage to avoid allocation
"""
log_q(prop::IsProposition, x::VecOrSub, tmp::VecOrSub) = error("log_q not implemented")

"""
    Sample one proposition point.

    `tmp` may be used a storage, `L` is given to avoid calling `length` or `size`
"""
sampling!(p::IsProposition, out, tmp, L::Int) = error("sampling! not implemented")

"""
    Returns the matrix of covariance of the proposition law
"""
covariance(prop::IsProposition) = error("covariance not implemented")



struct GaussianRegularizedProposition{T <: Number} <: IsProposition
    mean::Vector{T}
    chol_cov::CholCov{T}
end

function log_q(prop::GaussianRegularizedProposition, x::VecOrSub, tmp::VecOrSub) 
    ## The prior is no more uniform, hence the simplification
    return 0. # log(1)
end

function sampling!(p::GaussianRegularizedProposition{T}, out::VecOrSub{T}, tmp::VecOrSub{T}, L::Int) where {T}
    randn!(tmp)
    @inbounds for l in 1:L 
        out[l] = p.mean[l]
        @inbounds for l2 in 1:L 
            out[l] += p.chol_cov[l, l2] * tmp[l2]
        end
    end
end




struct StudentRegularizedProposition{T <: Number} <: IsProposition
    mean::VecOrSub{T}
    chol_cov::CholCov{T}
    ν::T
end

function log_q(prop::StudentRegularizedProposition, x::VecOrSub, tmp::VecOrSub) 
    ## The prior is no more uniform, hence the simplification
    return 0. # log(1)
end

function sampling!(p::StudentRegularizedProposition, out::VecOrSub{T}, tmp::VecOrSub{T}, L::Int) where {T}
    randn!(tmp)
    chi = rand(Distributions.Chisq(p.ν))
    f = sqrt(chi / p.ν)
    for l in 1:L 
        u = 0.
        for l2 in 1:L 
            u += p.chol_cov[l, l2] * tmp[l2]
        end
        out[l] = u / f + p.mean[l]
    end
end

"""
    GMM density

    Must not be mutated
"""
struct GaussianMixtureProposition{T <: Number} <: IsProposition
    gmm::Gmm{T}
    cumweights::Vector{T} # computed
    function GaussianMixtureProposition{T}(gmm::Gmm{T}) where {T}
        new{T}(gmm, cumsum(gmm.weights))
    end
end
GaussianMixtureProposition{T}(w, m, c) where {T} = GaussianMixtureProposition{T}(Gmm{T}(w, m, c))

function log_q(prop::GaussianMixtureProposition{T}, x::VecOrSub, tmp::VecOrSub) where {T}
    log(Probas.densite_melange(x, prop.gmm, tmp))
end

function sampling!(p::GaussianMixtureProposition{T}, out::VecOrSub{T}, tmp::VecOrSub{T}, L::Int) where {T}
    Probas.gmmsampling!(p.cumweights, p.gmm.means, p.gmm.chol_covs, out, tmp, p.gmm.K, L)
end

function covariance(p::GaussianMixtureProposition{T}) where {T}
    Probas.covariance_mixture(p.gmm.weights, p.gmm.means, [Symmetric(C * C') for C in p.gmm.chol_covs])
end


"""
 Uniform distribution on [0,1] 
"""
struct UniformProposition <: IsProposition
    L::Int
end

function sampling!(p::UniformProposition, out::VecOrSub{T}, tmp::VecOrSub{T}, L::Int) where {T}
    rand!(out)
end

log_q(prop::UniformProposition, x::VecOrSub, tmp::VecOrSub) = all((0 .<= x) .& (x .<= 1)) ? 0 : -Inf

covariance(prop::UniformProposition) = (1 / 12) .* Diagonal(ones(prop.L))

