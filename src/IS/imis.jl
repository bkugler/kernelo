# Implémentation of basic IMIS algorithm
# taken from Estimating and Projecting Trends in HIV/AIDS Generalized Epidemics Using Incremental Mixture Importance Sampling
# by Raftery & Bao (2010)
# The step are from part 3.1 

include("importance_sampling.jl")

using ..Probas

struct ImisParams 
    N0::Int 
    B::Int
    Kmax::Int
end

function imis(params::ImisParams, m::IsModel)
    prop, L = get_proposition(m), get_L(m)
    T = Float64
    N0, B, Kmax = params.N0, params.B, params.Kmax
    # memory setup ; we allocate the maximum needed, but
    # only a sub part may be returned 
    Nmax = N0 + Kmax * B
    x, tmp = zeros(L), zeros(L)
    X = zeros(L, Nmax) # samples
    ws = zeros(Nmax) # weigths
    log_px, log_qx = zeros(Nmax), zeros(Nmax) # density values

    # 1 - initial stage 
    max_log_p = -Inf # re scaling to avoid numerical issues
    for j in 1:N0
        v = view(X, :, j)
        sampling!(prop, v, tmp, L)
        log_px[j] = log_p(m, v, j) # first part
        if ws[j] > max_log_p 
            max_log_p = log_px[j]
        end
        log_qx[j] = log_q(prop, v, tmp) # snd part
    end 
    sum_w = 0 # normalization factor
    for j in 1:N0 
        ws[j] = exp(log_px[j] - max_log_p - log_qx[j]) 
        sum_w += ws[j]
    end
    ws ./= sum_w

    # 2 - iterative importance sampling 
    precision_prop = inv(covariance(prop))

    # at each step we need the full mixture to compute qk for the new samples 
    # the weigths are constant (thus easy to compute), dont need to store them
    means, chols::CholCovs{T} = zeros(T, L, Kmax), [CholCov{T}(L) for k in 1:Kmax]

    # we do a maximum of Kmax iteration, maybe less
    for K in 1:Kmax  
        Nk = N0 + (K - 1)B # before new samples 
        Nk1 = Nk + B # with new samples

        # a) find highest weigths and its neighboors ...
        _, jmax = findmax(view(ws, 1:Nk))
        xk = X[:, jmax]
        ds = zeros(Nk)
        for j in 1:Nk 
            u = xk - X[:,j]
            ds[j] = dot(u, precision_prop * u) # Mahalanodis distance
        end
        neighboors_idx = sortperm(ds)[1:B] # sortperm is in increasing order
        # ... and compute associated covariance
        Sigmak, sum_w = zeros(L, L), 0.
        for id in neighboors_idx
            u = x - X[:, id]
            # w = (ws[id] + (1 / Nk)) / 2 # average between importance and 1/Nk
            w = 1 # according to Fasalio et al 2016, this increase stability
            Sigmak .+= w .* u * u'
            sum_w += w
        end
        Sigmak ./= sum_w # normalization
        Cholk = cholesky(Sigmak).L
        means[:, K] = xk # save the mean ...
        chols[K] = Cholk # and the cholesky decomposition of the variance 

        # b) new samples 
        X[:, (Nk + 1):Nk1] .= xk .+ Cholk * randn(L, B)
     
        # c) update weigths 
        sum_w = 0
        # for existing points, we can update the weigths without computing the whole
        # mixture : q_k+1 = (N_k / N_k+1) * q_k + (B / N_k+1) * phi_k+1
        for j in 1:Nk 
            x = view(X, :, j)
            log_phi_k1 = Probas.log_gaussian_density(x, xk, Cholk, L)
            log_qx[j] = log((Nk * exp(log_qx[j]) + B * exp(log_phi_k1)) / Nk1) 
            ws[j] = log_px[j] - log_qx[j] # at this point, we store log w
            sum_w += exp(ws[j])
        end 
        # for new points, we have to compute the whole mixture
        K = length(means)
        for j in Nk + 1:Nk1
            x = view(X, :, j)
            q = log_q(prop, x, tmp) # initial part
            dm = Probas.densite_melange(x, means, chols) # mixture (weights = 1)
            log_qx[j] = log((N0 * exp(q) + B * dm) / Nk1) # resultant mixture
            log_px[j] = log_p(m, x, j)
            ws[j] = log_px[j] - log_qx[j] # at this point, we store log w
            sum_w += exp(ws[j])
        end
        # now we can apply exp and normalize ws 
        for j in 1:Nk1 
            ws[j] = exp(ws[j] - log(sum_w))
        end 

        # TODO: Apply a stoping criteria ?
    end
    X, ws
end