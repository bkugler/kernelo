module Kernelo

export Probas, Gllim, Is, Pred, Models, ModeFinding, Sobol

include("probas.jl")

module Gllim
export GLLiM, MetaGLLiM, EMMetaGLLiM, VariableNoiseSimpleJointGMMTraining, SimpleJointGMMTraining, JointGMMTraining
include("GLLiM/gmm_training.jl")
end # module

module Is
include("IS/importance_sampling.jl")
include("IS/imis.jl")
# export
end # module

module Pred
export SerieConfig, Prediction
include("PREDICTIONS/predictions.jl")
include("PREDICTIONS/results.jl")
end

"""
 Implements the mode-finding algorithm for GMM proposed by Carreira-Perpinan (2000)
"""
module ModeFinding
export Gmm, ModeResult
include("PREDICTIONS/mode_finding.jl")
end

module Archive
include("IO/io.jl")
end

module Models
include("MODELS/contexts.jl")
end

module Noise
export GradientParams, EmIsGLLiMParams, Bias
include("NOISE/em_is.jl")
include("NOISE/gradient.jl")
end


module Sobol 
include("SOBOL/sobol.jl")
end

module Perf 
include("PERF/diagnostic.jl")
end

end # module
