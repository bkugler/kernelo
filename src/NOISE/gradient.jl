using LinearAlgebra
using Printf


const epsilon = eps()

mutable struct GradientParams 
    max_iteration::Int
    TOL::Float64
    alpha::Float64
    beta::Float64
    fixed_a::Float64
    init_mu_noise::Float64
    line_search_max_iteration::Int
    Ntrain::Int
    Nobs::Int
end
GradientParams(; max_iteration = 300, TOL = 10e-10, alpha = 0.0001, beta = 0.7, fixed_a = 1., init_mu_noise = 0, 
                line_search_max_iteration = 10, Ntrain = 100000, Nobs = 0) = GradientParams(max_iteration, TOL, alpha, beta, fixed_a, init_mu_noise, 
                                                                line_search_max_iteration, Ntrain, Nobs)

Base.string(p::GradientParams) = string(p.max_iteration, p.TOL, p.alpha, p.beta, p.fixed_a, p.init_mu_noise, p.Ntrain, p.Nobs)
Base.repr(m::MIME"text/label", p::GradientParams) = "gradient-descent"
Base.repr(m::MIME"text/plain", p::GradientParams) = """
    Gradient descent 
    Covariance constraint : diag
    Nobs = $(p.Nobs)
    Ntrain = $(p.Ntrain)
    Initial mean noise : $(p.init_mu_noise)
    Max iteration : $(p.max_iteration)
"""

Base.repr(m::MIME"text/graph", p::GradientParams) = """
    Nobs = $(p.Nobs) ; Ntrain = $(p.Ntrain)
    Covariance constraint : diag
"""


# ------------------------- Calculations ------------------------- #

function _dist(y, yobs, a, mu, D)
    s = 0.
    @inbounds for d in 1:D
        s += (a * y[d] - yobs[d] + mu[d])^2
    end
    s
end

function _best_dist(a, mu, Ytrain, Yobs)
    D, N = size(Ytrain)
    _, Nobs = size(Yobs)
    argmins = zeros(Int, Nobs)
    Threads.@threads for i in 1:Nobs
        dist_min = Inf
        n_min = 0
        @inbounds for n in 1:N
            dist = _dist(view(Ytrain, :, n), view(Yobs, :, i), a, mu, D)
            if dist < dist_min
                dist_min = dist
                n_min = n
            end
        end
        @inbounds argmins[i] = n_min
    end
    argmins
end


function J(a, mu, Ytrain, Yobs)
    arg_mins = _best_dist(a, mu, Ytrain, Yobs)
    J(arg_mins, a, mu, Ytrain, Yobs)
end
function J(arg_mins, a, mu, Ytrain, Yobs)
    D, Nobs = size(Yobs)
    s = 0.
    @inbounds for i in 1:Nobs
        s += _dist(view(Ytrain, :, arg_mins[i]), view(Yobs, :, i), a, mu, D)
    end
    return s / Nobs
end


function dJ(arg_mins, a, mu, Ytrain, Yobs)
    D, Nobs = size(Yobs)
    s = zeros(D)
    @inbounds for i in 1:Nobs
        for d in 1:D
            s[d] += a * Ytrain[d,arg_mins[i]] - Yobs[d,i] + mu[d] 
        end
    end
    return 2 .* s ./ Nobs
end


"""
 Implementation for diagonal covariance 
"""
function _sigma_estimator(arg_mins, a, mu, Ytrain, Yobs)
    D, Nobs = size(Yobs)
    s = zeros(D)
    for i in 1:Nobs
        n_min = arg_mins[i]
        @views s .+= (a * Ytrain[:,n_min] .- Yobs[:,i] .+ mu).^2
    end
    return Diagonal(s / Nobs)
end


function backtrackingLineSearch(params::GradientParams, mu::Vector{T}, direction, f_x, df_x, Ytrain, Yobs) where {T}
    D, = size(mu)
    alpha, beta = params.alpha, params.beta
    derphi = dot(df_x, direction)
    stp = 1
    len_p = norm(direction)

    tmp = mu .+ stp .* direction
    arg_mins = _best_dist(params.fixed_a, tmp, Ytrain, Yobs)
    new_J = J(arg_mins, params.fixed_a, tmp, Ytrain, Yobs)
    #Loop until Armijo condition is satisfied
    while stp > 0.001 || new_J > f_x + alpha * stp * derphi
        stp *= beta
        # stp * len_p < epsilon && break  # Step is  too small, stop
        for d in 1:D
            tmp[d] = mu[d] + stp * direction[d]
        end
        arg_mins = _best_dist(params.fixed_a, tmp, Ytrain, Yobs)
        new_J = J(arg_mins, params.fixed_a, tmp, Ytrain, Yobs)
    end
    stp, arg_mins, tmp
end


function constantLineSearch(params, current_mu, direction, current_J, current_dJ, Ytrain, Yobs)
    D, = size(current_mu)
    stp = 0.3 
    new_mu = current_mu .+ stp * direction
    arg_mins = _best_dist(params.fixed_a, new_mu, Ytrain, Yobs)
    new_J = J(arg_mins, params.fixed_a, new_mu, Ytrain, Yobs)
    stp, arg_mins, new_mu
end

"""
 Implement a gradient descent with stochastic objective function.
    Some preliminary tests show that a estimation is difficult : the component of the gradient may explode.
    Thus, we only implement the estimation of mu. 
    Gradient descent is meant to be a fist order method, em-is-gllim being the favorite one.
"""
function run(params, Yobs, Y_generator)
    D, Nobs = size(Yobs)

    Ytrain = Y_generator(params.Ntrain)
    current_mu = params.init_mu_noise * ones(D)
    arg_mins = _best_dist(params.fixed_a, current_mu, Ytrain, Yobs)
    direction = - dJ(arg_mins, params.fixed_a, current_mu, Ytrain, Yobs)

    Jinit = J(arg_mins, params.fixed_a, current_mu, Ytrain, Yobs)  
    current_J = Jinit
    history = []
    iter = 1
    while iter <= params.max_iteration
        @time alpha, arg_mins, new_mu = constantLineSearch(params, current_mu, direction, current_J, - direction,  Ytrain, Yobs)
        new_J = J(arg_mins, params.fixed_a, new_mu, Ytrain, Yobs)
        
        diff = (current_J - new_J) / Jinit
        @debug "J relative progress : $(@sprintf "%.6f" diff)"

        current_mu = new_mu
        current_J = new_J
        sigma = _sigma_estimator(arg_mins, params.fixed_a, current_mu, Ytrain, Yobs)
        log = """
            Iteration $(iter)/$(params.max_iteration)
                New estimated ADDITIVE offset : $current_mu
                New estimated COVARIANCE : $(diag(sigma))
            """
        @debug log

        push!(history, (current_J, current_mu, sigma))
        if (0 <= diff) && diff < params.TOL
            break
        end 

        if iter % 30 == 0
            Ytrain = Y_generator(params.Ntrain)
        end
        direction = - dJ(arg_mins, params.fixed_a, current_mu, Ytrain, Yobs)
        iter += 1
    end
    history
end
