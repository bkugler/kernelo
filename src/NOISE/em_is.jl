using Statistics
using LinearAlgebra
using Logging

using ..Probas
using ..Gllim
using ..Is
using ..Models

# ---------------------------------------------------------------------------------------------------- #
# ---------------------------------- Context - parameters - data ------------------------------------- #
# ---------------------------------------------------------------------------------------------------- #


"""
 Store the shifting parameters 
"""
struct Bias{T <: Number}
    a::T 
    mu::Vector{T}
    sigma::CovType{T}
end
"""
 Default values 
"""
Bias{T}(D::Int) where {T} = Bias{T}(1., zeros(T, D), UniformScaling(1.))

struct Targets 
    a::Bool
    mu::Bool 
    sigma::Bool
end 

"""
Holds em_is_gllim parameters.
"""
mutable struct EmIsGLLiMParams 
    max_iteration::Int  
    init::Bias
    Ns::Int
    targets::Targets
end

"""
 Default values
"""
function EmIsGLLiMParams(; max_iteration = 100, init = nothing, D = nothing, Ns = 10000, targets = Targets(false, true, true))
    init = init == nothing ? Bias{Float64}(D) : init

    if targets.a && !(init.sigma isa IsoCov)
        error("For `a` estimation, only isotropic covariance is allowed !")
    end

    EmIsGLLiMParams(max_iteration, init, Ns, targets)
end

# ---------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- Computations ---------------------------------------- #
# ---------------------------------------------------------------------------------------------------- #

function theorical_estimators(c::AbstractContext, Xobs::Matrix{T}, Yobs::Matrix{T})::Bias{T} where {T}
    D, N = size(Yobs)
    FX = Models.F(c, Xobs)
    Ymean = Statistics.mean(Yobs, dims = 2)[:,1]
    Fmean = Statistics.mean(FX, dims = 2)[:,1]
    a = sum(dot(FX[:,i], Yobs[:,i] - Ymean)  for i in 1:N) / sum(dot(FX[:,i], FX[:,i] - Fmean) for i in 1:N)
    mu = Ymean - a * Fmean
    Sigma = zeros(T, D, D)
    for n in 1:N
        u = Yobs[:,n] - Ymean - a * (FX[:,n] - Fmean)
        Sigma .+= (u * u') ./ N
    end
    Bias{T}(a, mu, Symmetric(Sigma))
end


# --------------------------- E step : computation of weights and images --------------------------- #

"""
    The proposition is a GMM.
    The target density if a gaussian N(a * F(x) + b, y, sigma)
    Since F(x) will be reused, log_p caches the result.
"""
struct EStepIsModel{T <: Number} <: IsModel 
    model::AbstractContext
    proposition::Is.GaussianMixtureProposition{T}
    y::Vector{T}
    bias::Bias{T}

    cov::Union{IsoCov{T},DiagCov{T},CholCov{T}} # computed from bias.sigma
    pre_factor::T # computed from bias.sigma
    FXs::MatOrSub{T}
    tmp_D::Vector{T}
    tmp_D2::Vector{T}
    L::Int # computed from `proposition`
    D::Int # computed from `y`
    function EStepIsModel{T}(model::AbstractContext, proposition::Is.GaussianMixtureProposition{T}, y::Vector{T}, bias::Bias{T}, FXs::MatOrSub{T}) where {T <: Number}
        L, D = proposition.gmm.L,   length(y)
        tmp_D, tmp_D2 = zeros(T, D), zeros(T, D)
        cov = _convert_cov(bias.sigma)
        pf = Probas.pre_factor(cov, D)
        new(model, proposition, y, bias, cov, pf,  FXs, tmp_D, tmp_D2, L, D)
    end
end

_convert_cov(cov::Union{DiagCov,IsoCov}) = cov
_convert_cov(cov::FullCov) = cholesky(cov).L


@inline Is.get_L(m::EStepIsModel) = m.L
@inline Is.get_proposition(m::EStepIsModel) = m.proposition
function Is.log_p(m::EStepIsModel{T}, x::VecOrSub{T}, j::Int) where {T <: Number}
    if !Models.mask(m.model, x, m.L)
        # if x is invalid, we can't compute F(x), so we let it as zero and we set w = 0
        return - Inf
    end

    # compute F and store the result
    Models.f!(m.model, x, view(m.FXs, :, j), m.D)

    @inbounds for d in 1:m.D
        m.tmp_D[d] = m.bias.a * m.FXs[d,j] + m.bias.mu[d]
    end
    return Probas.log_gaussian_density!(m.tmp_D, m.y, m.cov, m.D, m.pre_factor, m.tmp_D2)
end



function e_step(Yobs::Matrix{T}, Xs::Array{T,3}, model::AbstractContext,
    gmms::Vector{Gmm{T}}, bias::Bias{T}) where {T}

    L, Ns, Ny = size(Xs)
    D, _ = size(Yobs)

    ws = zeros(T, Ns, Ny)
    FXs = zeros(T, D, Ns, Ny)
    Fn = zeros(T, D, Ny)
    is_valid = zeros(Bool, Ny)

    # for i in 1:Ny
    Threads.@threads for i in 1:Ny
        gmm = gmms[i]
        y = Yobs[ :, i]

        # setup the importance sampling model
        is_model = EStepIsModel{T}(model, Is.GaussianMixtureProposition{T}(gmm), y, bias, view(FXs, :, :, i))

        diagnostic = Is.importance_sampling!(is_model, view(Xs, :, :, i), view(ws, :, i))
            
        is_valid[i] = diagnostic.es >= 1 # this yn is valid
        @inbounds for j in 1:Ns 
            for d in 1:D
                Fn[d, i] += ws[j,i] * FXs[d,j,i]
            end
        end
    end

    ymean, Fmean = zeros(T, D), zeros(T, D)
    Nvalid = 0
    @inbounds for i in 1:Ny
        if is_valid[i]
            Nvalid += 1
            ymean .+= Yobs[:,i]
            Fmean .+= Fn[:,i]
        end
    end
    ymean ./= Nvalid
    Fmean ./= Nvalid
    is_valid, ymean, Fmean, Fn, FXs, ws
end


# --------------------------- M step : computation of a, mu, Sigma --------------------------- #

function a_step(Yobs, Fn, ymean, Fmean, ws, FXs)
    D, Ns, Ny = size(FXs)
    num, den = 0., 0.
    for n in 1:Ny
        for d in 1:D
            num += Fn[d,n] * (Yobs[d,n] - ymean[d])
        end

        for i in 1:Ns
            ps2 = 0.
            for d in 1:D
                ps2 += FXs[d,i,n] * (FXs[d,i,n] - Fmean[d])
            end
            den += ws[i,n] * ps2
        end
    end
    (den == 0) ?  1. : num / den
end

mu_step(ymean, Fmean, a) = ymean .- a .* Fmean


"""
 Isometric covariance 
"""
function _sigma_estimator!(a::T, fx::VecOrSub{T}, mu::VecOrSub{T}, y::VecOrSub{T}, out::IsoCov{T},  D::Int) where {T}
    out = 0.
    @inbounds for d in 1:D
        out += (a * fx[d] + mu[d] - y[d])^2
    end
    UniformScaling(out / D)
end

function _sigma_estimator!(a::T, fx::VecOrSub{T}, mu::VecOrSub{T}, y::VecOrSub{T}, out::DiagCov{T}, D::Int) where {T}
    @inbounds for d in 1:D
        out[d,d] = (a * fx[d] + mu[d] - y[d])^2
    end
    out
end 

function _sigma_estimator!(a::T, fx::VecOrSub{T}, mu::VecOrSub{T}, y::VecOrSub{T}, out::FullCov{T}, D::Int) where {T} 
    out_data = out.data
    @. out_data[:,1] = a * fx + mu - y # using as tmp storage
    @inbounds for d1 in 1:D
        for d2 in 2:D
            out_data[d1,d2] = out_data[d1,1] * out_data[d2,1]
        end
    end 
    @inbounds for d in 2:D
        out_data[d,1] = out_data[1,d] # using symetry
    end
    out_data[1,1] = out_data[1,1]^2
    out
end

_allocate_sigma(c::IsoCov{T}, D::Int) where {T} = UniformScaling(0.)
_allocate_sigma(c::DiagCov{T}, D::Int) where {T} = Diagonal(zeros(T, D))
_allocate_sigma(c::FullCov{T}, D::Int) where {T} = Symmetric(zeros(T, D, D))

"""
 current_cov to broadcast 
"""
function sigma_step(Yobs, is_valid, a::T, mu, FXs, ws, current_cov::CovType{T}) where {T}
    D, Ns, Ny = size(FXs)
    tmp = _allocate_sigma(current_cov, D)
    sigma = _allocate_sigma(current_cov, D)
    Nvalid = 0
    for n in 1:Ny
        if is_valid[n]
            Nvalid += 1
            for i in 1:Ns
                # Mutate tmp and return value
                sigma += ws[i,n] *  _sigma_estimator!(a, view(FXs, :, i, n), mu, view(Yobs, :, n), tmp, D)
            end
        end
    end
    sigma / Nvalid
end

# ---------------------------------------------------------------------------------------------------- #
# -------------------------------------------- GLLiM step -------------------------------------------- #
# ---------------------------------------------------------------------------------------------------- #


"""
    Update and inverse gllim according to given mean and covariance.
    (Thanks to gaussian vector properties, no additionnal training is required)
    Return gllim_star
"""
function _gllim_step(initial_gllim::GLLiM, bias::Bias)
    noisy_gllim = Gllim.add_bias(initial_gllim, bias.a, bias.mu, bias.sigma)
    gllim_star = Gllim.inversion(noisy_gllim)
end

# ---------------------------------------------------------------------------------------------------- #
# -------------------------------------------- Main algorithm ---------------------------------------- #
# ---------------------------------------------------------------------------------------------------- #

const History{T} = Vector{Tuple{Bias{T},Float64}}


_log_sigma(s::UniformScaling) = s.λ
_log_sigma(s) = diag(s)

"""
 run(params, Yobs, model, perfect_gllim)

    Implement EM-like algorithm to estimate model noise, with Importance Sampling.
    Start with a perfect gllim, then estimate by iterations the mean and cov of the noise.
"""
function run(params::EmIsGLLiMParams, Yobs::Matrix{T},
   model::AbstractContext, perfect_gllim::GLLiM) where {T <:  Number}

    D, Nobs = size(Yobs)
    L = perfect_gllim.L
    Ns = params.Ns

    bias = params.init
    history = [(bias, 1.)]
    Xs = zeros(T, L, Ns, Nobs)

    for current_iter in 1:params.max_iteration
        gllim_star = _gllim_step(perfect_gllim, bias)

        weightss, meanss  = Gllim.conditionnal_density(gllim_star, Yobs)
        gllim_chol_covs = [cholesky(c).L for c in gllim_star.Σ]
        gmms = [Gmm{T}(weightss[:,i], meanss[:,:,i], gllim_chol_covs)  for i in 1:Nobs ]
        
        @time is_valid, ymean, Fmean, Fn, FXs, ws = e_step(Yobs, Xs, model, gmms,   bias)


        effective_sample_size = sum(ws; dims = 1).^2 ./ sum(ws.^2; dims = 1)
        effective_sample_size[isnan.(effective_sample_size)] .= 0
        MESS = Statistics.mean(effective_sample_size)

        a::T = params.targets.a ? a_step(Yobs, Fn, ymean, Fmean, ws, FXs) : params.init.a
        mu = params.targets.mu ? mu_step(ymean, Fmean, a) : params.init.mu
        sigma = params.targets.sigma ? sigma_step(Yobs, is_valid, a, mu, FXs, ws, bias.sigma) : params.init.sigma
    
        @debug """
        Iteration $(current_iter)/$(params.max_iteration)
            New estimated MULTIPLICATIVE offset : $a
            New estimated ADDITIVE offset : $mu
            New estimated COVARIANCE : $(_log_sigma(sigma))
        """

        bias = Bias{T}(a, mu, sigma)
        push!(history, (bias, MESS))
    end
    history
end



