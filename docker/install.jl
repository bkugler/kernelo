using Pkg

Pkg.add(PackageSpec(url="https://github.com/lstagner/ReadSav.jl"))
Pkg.add(PackageSpec(path="/kernelo"))
Pkg.add("MAT")
using Kernelo