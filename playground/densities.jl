using Plots
using Kernelo.Probas
using LinearAlgebra
using Kernelo.Archive 

function drawDensitites(; savepath = nothing)
    gmm = Probas.Gmm{Float64}([0.5, 0.5], [.2 .7], [LowerTriangular(0.1 .* ones(1,1)), LowerTriangular(0.1 .* ones(1,1))] )
    mu = [0.5]
    cov = LowerTriangular(0.2 .* ones(1,1))

    cov2 = LowerTriangular(0.04 .* ones(1,1))
    gmm2 = Probas.Gmm{Float64}([0.3, 0.4, 0.2, 0.1], [.2 .7 .3 .45], [cov2, cov2, cov2, cov2] )

    xs = range(0,1; step= 0.01)
    d1 = [exp(Probas.log_gaussian_density([x], mu, cov, 1)) for x in xs]
    allocated = zeros(1)
    d2 = [Probas.densite_melange([x], gmm, allocated) for x in xs]
    d3 = [Probas.densite_melange([x], gmm2, allocated) for x in xs]

    p = plot()
    plot!(p, xs, d2, label = "Target distribution")
    plot!(p, xs, d1, label = "Unidmodal distribution", linestyle = :dash)
    plot!(p, xs, d3, label = "Full mixture")
    if savepath !== nothing 
        png(p, savepath)
        @info "Saved in $savepath"
    end
    p
end
savepath = Archive.get_path(Archive.FOLDER_GRAPHS, "density")
drawDensitites(savepath = savepath)