using Kernelo.Models 
using Kernelo.Perf
using Kernelo.Probas
using Kernelo.Gllim

include("setup.jl")


struct KSelection
    direct_error::Vector{Float64}
    sigma_error::Vector{Float64}
    bic::Vector{Float64}
    log_likelihood::Vector{Float64} # additionnal bic info
    dims::Vector{Int} # additionnal bic info
    bic_test::Vector{Float64}
    models::Vector{Gllim.GLLiM}
    KSelection(Nk::Int) = new(zeros(Nk), zeros(Nk), zeros(Nk), zeros(Nk), zeros(Int, Nk), zeros(Nk), Vector{Gllim.GLLiM}(undef, Nk))
end

function train_select_K(context::Models.AbstractContext, Ks::Vector{Int}, std_train::Float64)
    # generate one training set, suitable for all choices ok K 
    Kmax = maximum(Ks)
    Ntrain = Kmax * 1_000
    X, Y = Models.data_training(context, Ntrain; noise_std = std_train)
    # separated test dataset to assess overfitting
    Xtest, Ytest = Models.data_training(context, Ntrain; noise_std = std_train)
    L, _ = size(X)
    D, _ = size(Y)
    init_params = Gllim.MultiInit()

    out = KSelection(length(Ks))
    Log.indent_log()
    for (i, K) in enumerate(Ks)
        @info "Starting model $i (K = $K)..."
        gllim_params = Gllim.GLLiMTraining(init_params, K)

        Log.indent_log()
        # training
        base_gllim = Gllim.GLLiM{Float64,FullCovs{Float64},DiagCovs{Float64}}(K, L, D)
        base_gllim, _ = Gllim.init_GLLIM(base_gllim, X, Y, gllim_params.init)
        gllim = Gllim.train(base_gllim, X, Y, gllim_params)
        Log.deindent_log()

        # criteria computation
        out.direct_error[i] = Perf.direct_error(context, gllim)
        out.sigma_error[i] = Perf.sigma_error(gllim)
        bic, ll, dimension = Perf.bic(gllim, X, Y)
        out.bic[i] = bic
        out.log_likelihood[i] = ll
        out.dims[i] = dimension
        bic_test, _, _ = Perf.bic(gllim, Xtest, Ytest)
        out.bic_test[i] = bic_test

        # save the learned model 
        out.models[i] = gllim 
        
        flush(Base.stdout) # useful in batch mode 
    end
    Log.deindent_log()
    out 
end
