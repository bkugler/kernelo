using Dates

const ComputationInvertion = :invertion
const ComputationNoise = :noise
const ComputationMCMC = :mcmc

"""
    Stores computation time and meta data describing the computation
"""
mutable struct ComputationTime 
    exp_index::Int
    kind::Symbol 
    learning::Millisecond 
    prediction::Millisecond
    gllim_only_prediction::Millisecond
    label::String 
    L::Int
    D::Int
    K::Int
    Ntrain::Int
    Nobs::Int
    Nis::Int
end