using Kernelo.Probas
using Kernelo.Models
using Kernelo.Archive
using MAT 


include("procedure.jl")


# entry size N x D x W -> out size D x NW 
function _cube_to_mat(data::CubeOrSub)
    _, _, W = size(data)
    datat = vcat([data[:,:, w] for w in 1:W]...)
    return Matrix(datat'), W
end
# entry size : L x NW -> out size N x L x W
function _mat_to_cube(data::Matrix, W::Int)
    L, NW = size(data)
    N = Int(NW / W)
    out = zeros(N, L, W)
    for w in 1:W 
        start = 1 + (w - 1) * N
        stop = start +  N - 1 # = w x N
        out[:,:, w] = data[:, start:stop]'
    end
    out
end

function _package_res(mat_preds::Matrix, W::Int, mask::Vector, geometries::Matrix)
    series = extract_series(mat_preds, [Methods.Best, Methods.IsMean])
    pred_is_mean = aggregate(series[Methods.IsMean]) # size D x NW
    pred_best = aggregate(series[Methods.Best]) # size D x NW
    std_is_mean = aggregate_std(series[Methods.IsMean]) # size D x NW
    std_best = aggregate_std(series[Methods.Best]) # size D x NW

    # we split according to wavelengths
    pred_is_mean_cube = _mat_to_cube(pred_is_mean, W)
    pred_best_cube = _mat_to_cube(pred_best, W)
    std_is_mean_cube = _mat_to_cube(std_is_mean, W)
    std_best_cube = _mat_to_cube(std_best, W)

    Dict("geometries" => geometries,
        "mask" => mask,
        "x_is_mean" => pred_is_mean_cube,
        "std_is_mean" => std_is_mean_cube,
        "x_best" => pred_best_cube,
        "std_best" => std_best_cube,
    ) 
end

function inverse_export_cube(run, context, gllim_params, Nis, Yobs_cube, Ystd_cube, mask; label = "") 
    geometries = Models._load_geometries(context) # D x 3 
    _, _, Wtot = size(Yobs_cube)
 
    # to invert all observations at once, we group them
    Yobs, W = _cube_to_mat(Yobs_cube)
    Ystd, _ = _cube_to_mat(Ystd_cube)

    is_params = FixedCovIs(gllim_params.Ystd, Ystd, Nis)
    mat_preds, _,  ct = train_inverse(run, context, Yobs, gllim_params, is_params)

    path = Archive.get_path(Archive.FOLDER_PREDICTIONS, context, gllim_params, is_params, Yobs_cube; label = label)
    # we export raw preds for external plots
    d = _package_res(mat_preds, W, mask, geometries)
    matwrite(path * ".mat", d) # compress doesn't do a lot here
    @debug "Exported in $path"
    return ct
end


function hapke_massive_B385()
    Nis = 20_000
    context = Models.B385Context()
    gllim_params = Gllim.GLLiMTraining(Gllim.MultiInit(), 50; Ystd = 0.001)
    Yobs_cube, Ystd_cube, mask = Models.load_observations(context)
    ct =  inverse_export_cube(2, context, gllim_params, Nis, Yobs_cube, Ystd_cube, mask) 
    Archive.save(ct, Archive.FOLDER_MISC, context, gllim_params, :time)
end

function hapke_massive_Mars2020()
    Nis = 20_000
    context = Models.Mars2020Context()
    gllim_params = Gllim.GLLiMTraining(Gllim.MultiInit(), 50; Ystd = 0.001)

    exp = :FRT
    Yobs_cube, Ystd_cube, mask = Models.load_observations(context, exp)
    ct1 = inverse_export_cube(2, context, gllim_params, Nis, Yobs_cube, Ystd_cube, mask; label = "mars2020_$(exp)")
    
    exp = :HRL
    Yobs_cube, Ystd_cube, mask = Models.load_observations(context, exp)
    ct2 = inverse_export_cube(1, context, gllim_params, Nis, Yobs_cube, Ystd_cube, mask; label = "mars2020_$(exp)")
    
    Archive.save([ct1, ct2], Archive.FOLDER_MISC, context, gllim_params, Nis, :time)
end

function hapke_massive_glace(exp::Symbol)
    Nis = 20_000
    context = Models.GlaceContext()
    gllim_params = Gllim.GLLiMTraining(Gllim.MultiInit(), 50; Ystd = 0.001)

    Yobs_cube, _, Ystd_cube, mask = Models.load_observations(context, exp)
    ct = inverse_export_cube(2, context, gllim_params, Nis, Yobs_cube, Ystd_cube, mask; label = "glace_$(exp)")

    Archive.save(ct, Archive.FOLDER_MISC, context, gllim_params, Nis, :time)
end
