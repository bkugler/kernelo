using Kernelo.Models
using Kernelo.Archive
using Kernelo.Gllim
using Kernelo.Pred
using Kernelo.Is
using Plots

include("procedure.jl")
include("../mcmc/procedure.jl")

context = Models.NontroniteHapkeContext()
Yobs, wavelengths, Ystd  = Models.load_observations(context)

std_train, std_ratio = 0.001, 20
mcmc_params = MCMCParams(std_train, std_ratio)      # withouh noise floor
gllim_params = Gllim.GLLiMTraining(Gllim.MultiInit(), 50; Ystd = std_train)

invs_mcmc, _ = Archive.load(Archive.FOLDER_PREDICTIONS, context, Yobs, mcmc_params)
Xmcmc = aggregate(invs_mcmc)
Xmcmc = Models.from_physique(context, Xmcmc)
n = 33

imis_params = Is.ImisParams(2000, 1000, 100)

function inverse_one(trained_gllim::Gllim.GLLiM, Ns)
    D, _ = size(Yobs)

    yobs = Yobs[:, n]
    obs_std = Ystd[:, n]

    # we shift the covariance of the GLLiM model
    sigma_obs = Diagonal(obs_std.^2)    # we want the covariance
    gllim_shifted = Gllim.add_bias(trained_gllim, 1., 0., sigma_obs)
    gllim_star = Gllim.inversion(gllim_shifted)

    # we get the inverse density from the shifted inverse
    weights_all_i, means_all_i = Gllim.conditionnal_density(gllim_star, yobs)

    # we merge the mixture
    mean_mean_gllim, mean_cov_gllim,  weights_i, centroids_mean_merged_gllim, centroids_cov_gllim = Pred.merge_predict(weights_all_i, means_all_i, gllim_star.Σ)

    chol_covs = _safe_cholesky(centroids_cov_gllim)

    # we choose the stat model : Sigma = Sigma_train + Sigma_obs
    Sigma = Diagonal(std_train^2 .* ones(D)) + sigma_obs
    model = FixedObsCovariance{Float64}(context, yobs, Sigma)

    # we apply ImportanceSampling
    model.proposition = Is.GaussianMixtureProposition{Float64}(weights_i, centroids_mean_merged_gllim, chol_covs)

    Xs, weigths = Is.imis(imis_params, model)
    # Xs, weigths = zeros(context.L, Ns), zeros(Ns)
    # Is.importance_sampling!(model, Xs, weigths)
    # @show sort(weigths, rev = true)[1:20]

    # p1 = scatter(Xs[3,:], Xs[4,:], xlabel = "b", ylabel = "c", label = "GLLiM - IS - Mean",
    # xlims = (0, 1), ylims = (0, 1),
    # alpha = weigths .* 10, # to be able to see points
    # markerstrokewidth = 0, markersize = 3)
    # scatter!(p1, mean_mean_gllim[3:3], mean_mean_gllim[4:4], label = "GLLiM - Mean")
    # # scatter!(p1, Xmcmc[3, n:n], Xmcmc[4, n:n], label = "MCMC Ref")

    # p2 = scatter(Xs[1,:], Xs[2,:], xlabel = "omega", ylabel = "theta", label = "GLLiM - IS - Mean",
    # xlims = (0, 1), ylims = (0, 1),
    # alpha = weigths .* 10, # to be able to see points
    # markerstrokewidth = 0, markersize = 3)
    # scatter!(p2, mean_mean_gllim[1:1], mean_mean_gllim[2:2], label = "GLLiM - Mean")
    # # scatter!(p2, Xmcmc[1, n:n], Xmcmc[2, n:n], label = "MCMC Ref")

    # p = plot(p1, p2)

    L, Ns = size(Xs)
    mean = zeros(L)
    for j in Ns
        mean .+= weigths[j] .+ Xs[:, j] # sum(weights) = 1
    end

    mean_mean_gllim, mean
end

function repeat_inversion(run_gllim::Bool, nb_try = 10, Ns = 1000)
    if run_gllim
        trained_gllim = inference(context, gllim_params)
        Archive.save(trained_gllim, Archive.FOLDER_GLLIM, context, gllim_params, :solo)
    end
    trained_gllim::Gllim.GLLiM = Archive.load(Archive.FOLDER_GLLIM, context, gllim_params, :solo)

    means = zeros(context.L, nb_try)
    means_is = zeros(context.L, nb_try)
    Threads.@threads for i in 1:nb_try
        mean, mean_is = inverse_one(trained_gllim, Ns)
        means[:, i] = mean
        means_is[:, i] = mean_is
    end

    p1 = scatter(means_is[3,:], means_is[4,:], xlabel = "b", ylabel = "c", label = "GLLiM - IS - Mean",
    xlims = (0, 1), ylims = (0, 1),
    markerstrokewidth = 0, markersize = 3)
    scatter!(p1, means[3, :], means[4, :], label = "GLLiM - Mean")
    scatter!(p1, Xmcmc[3, n:n], Xmcmc[4, n:n], label = "MCMC Ref")

    p2 = scatter(means_is[1,:], means_is[2,:], xlabel = "omega", ylabel = "theta", label = "GLLiM - IS - Mean",
    xlims = (0, 1), ylims = (0, 1),
    markerstrokewidth = 0, markersize = 3)
    scatter!(p2, means[1, :], means[2, :], label = "GLLiM - Mean")
    scatter!(p2, Xmcmc[1, n:n], Xmcmc[2, n:n], label = "MCMC Ref")

    p = plot(p1, p2)
end

Plots.pyplot()
trained_gllim = Archive.load(Archive.FOLDER_GLLIM, context, gllim_params, :solo)
# inverse_one(trained_gllim, 100_000)
repeat_inversion(false, 20, 20_000)

