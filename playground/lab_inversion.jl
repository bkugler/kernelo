include("procedure.jl")
include("plotting/base_plots.jl")
include("plotting/plots.jl")

# Detailled configuration of inversion parameters 
const std_train = 0.01
const K = 50
const Ntrain = 50_000
const max_iteration = 100
const N_is = 50000
# hard coded in training : cov_floor = 1e-10 
# init 
const max_iteration_gllim = 30
const starts = 10
const init_params = Gllim.MultiInit(; max_iteration_gllim = max_iteration_gllim, starts = starts)
const gllim_params = Gllim.GLLiMTraining(init_params, K; Ystd = std_train, Ntrain = Ntrain, max_iteration = max_iteration)


function exp_lab(run)
    contexts = [
        Models.MukundpuraBlocContext(), 
        Models.MukundpuraPoudreContext(), 
        # Models.BlackyBlocContext(), 
        # Models.BlackyPoudreContext(), 
        Models.HowarditeContext(),
        # Models.NontroniteHapkeContext()
    ]

    all_preds::Vector{Vector{Prediction}} = []

    is_paramss = []
    for context in contexts 
        L, D = context.L, context.D        
        Yobs, wavelengths, Yobs_std = Models.load_observations(context)
        
        is_params = FixedCovIs(std_train, Yobs_std, N_is)
        push!(is_paramss, is_params)
        
        mat_preds, _,  _ = train_inverse(run, context, Yobs, gllim_params, is_params)
        preds = format_inversions(mat_preds, [
            Methods.GllimMean => SerieConfig(L"$\hat{x}_{GLLiM}$", :basic, "red"),
            Methods.IsMean => SerieConfig(L"$\hat{x}_{IS-G}$", :basic, "orange"),
            Methods.Best => SerieConfig("Best residual", :dashed, "blue"),
        ])

        # export exact prediction to be further processed
        savepath = Archive.get_path(Archive.FOLDER_PREDICTIONS, context, gllim_params, is_params; label = context.LABEL) * ".json"
        export_predictions(context, preds, savepath)

        # plot one spectrum
        savepath = Archive.get_path(Archive.FOLDER_GRAPHS, context, gllim_params, is_params; label = context.LABEL * "_$(std_train)") * ".png"
        plot_components(context, preds;  savepath = savepath , 
            varnames = context.variables_names, x_ticks = 1000 .* wavelengths, xlabel = "wavelength (nm)")

        # push!(preds, Prediction(Xobs; config = SerieConfig(L"$x_{obs}$", :basic, "green")))
        push!(all_preds, preds)
    end

    # we plot all the invertions in one Hockey Stick 
    path = Archive.get_path(Archive.FOLDER_GRAPHS, gllim_params, Archive.get_hash(contexts...), Archive.get_hash(is_paramss...); label = "HockeyStick_$(std_train)")
    plot_hockey_stick(contexts, all_preds, path)
end

exp_lab(0)


function assert_convergence() 
    contexts = [
        Models.MukundpuraBlocContext(), 
        Models.MukundpuraPoudreContext(), 
        # Models.BlackyBlocContext(), 
        # Models.BlackyPoudreContext(), 
        Models.HowarditeContext(),
        # Models.NontroniteHapkeContext()
    ]

    for context in contexts 
        L, D = context.L, context.D        
        
        X, Y = Models.data_training(context, gllim_params.Ntrain; noise_std = gllim_params.Ystd)
        L, _ = size(X)
        D, _ = size(Y)
        base_gllim = Gllim.GLLiM{Float64,FullCovs{Float64},DiagCovs{Float64}}(gllim_params.K, L, D) 
        base_gllim, _ = Gllim.init_GLLIM(base_gllim, X, Y, gllim_params.init)
        _, lls = Gllim.train(base_gllim, X, Y, gllim_params; return_log_likehood = true)

        # plot the likehood over the iterations
        savepath = Archive.get_path(Archive.FOLDER_GRAPHS, context, gllim_params; label = context.LABEL * "_$(std_train)") * ".png"
        p = plot(1:length(lls), lls; yscale = :log10, xlabel = "iterations", ylabel = "log-likehood")
        png(p, savepath)
        @info "Plotted in $savepath"
    end
end