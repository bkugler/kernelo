module Methods 

const GllimMean =  "gllim_mean"
const IsMean =  "is_mean"
const GllimBestCentroid(k) =  "gllim_best_centroid_$k"
const GllimModeFromBestCentroid(k) =  "gllim_mode_from_best_centroid_$k"
const GllimMergedCentroid(k) =  "gllim_merged_centroid_$k"
const GllimModeFromMergedCentroid(k) =  "gllim_mode_from_merged_centroid_$k"
const IsCentroid(k) =  "is_centroid_$k"
const BestNoIs = "best_no_is"
const Best = "best"
const MCMC = "mcmc"

end

