using Plots
using Kernelo.Models 
using Kernelo.Gllim
using LaTeXStrings

include("procedure.jl")
include("setup.jl")

function drawMapping(run)
    c = Models.ExampleContext()

    gp = Gllim.JointGMMTraining(Gllim.FixedInit(), 30)
    if run 
        trained_gllim = inference(c, gp)
        save(trained_gllim, FOLDER_GLLIM, c, gp)
    end
    trained_gllim = load(FOLDER_GLLIM, c, gp)

    Xobs, Yobs = Models.data_training(c, 10_000; noise_std = 0.00001)
    # the weights define the clusters on X
    weights, _ = Gllim.conditionnal_density(trained_gllim, Xobs)
    
    Ypred, _ = Gllim.predict(trained_gllim, Xobs)
   
   path = get_path(FOLDER_GRAPHS, c, gp; label = "clusters")
    _plot_clusters(Xobs, weights, trained_gllim.c; savepath = path)
    _plot_Festim(Xobs, Yobs, Ypred, weights; savepath = path * "F")
    return
end

function _colors(weights) 
    K, N = size(weights)
    # XYZ is a linear space, not RGB
    colors = XYZ.(palette(:rainbow1, K))
    x_colors = Vector{RGB}(undef, N)
    for n in 1:N 
        x_colors[n] = RGB(sum(weights[:, n] .* colors))
    end
    c_colors = RGB.(colors)
    c_colors, x_colors
end

function _plot_clusters(X::Matrix, weights::Matrix, c::Matrix;savepath = nothing)
    c_colors, x_colors = _colors(weights)
    p = scatter(X[1,:], X[2,:]; c = x_colors,  markersize=6, markeralpha = 0.5, markerstrokewidth = 0, label = "X") # points
    scatter!(p, c[1, :], c[2, :]; c = c_colors, markersize=10, markerstrokewidth = 3, labels= L"c_k") # centers
    if savepath !== nothing 
        png(p, savepath)
        @debug "Saved in $savepath"
    end
    p 
end

function _plot_Festim(Xobs, Yobs, Ypred, weights;savepath = nothing)
    # exact graph
    p = plot()
    _, x_colors = _colors(weights)
    scatter!(p, Xobs[1, :], Xobs[2, :], Ypred[1, :], c = x_colors, colorbar=false,markerstrokewidth = 0, alpha = 0.7, label=L"F_\theta" )
    surface!(p, Xobs[1, :], Xobs[2, :], Yobs[1, :], c = "grey", colorbar = false, alpha = 0.02, label = L"F")
    if savepath !== nothing 
        png(p, savepath)
        @debug "Saved in $savepath"
    end
    p
end
drawMapping(false);