using Kernelo.Archive

Archive.PATHS.ROOT_PATH_ARCHIVE = "/Users/kuglerb/Documents/Work/"
# Archive.PATHS.ROOT_PATH_ARCHIVE = "/scratch/WORK/DATA/"
Archive.PATHS.ROOT_PATH_DATA = "/Users/kuglerb/Documents/docs/DATA/"
# Archive.PATHS.ROOT_PATH_DATA = "/home/bkugler/Documents/DATA/"

"""
 Expose a custom logger with support for indendation
"""
module Log
using Logging

function formatter(level, _module, group, id, file, line)
    color, prefix, suffix = Logging.default_metafmt(level, _module, group, id, file, line)
    return color, prefix, ""
end

mutable struct Logger <: AbstractLogger
    _logger::ConsoleLogger
    indent::Int
    _lock::ReentrantLock
    Logger() = new(ConsoleLogger(stdout, Logging.Debug, meta_formatter = formatter), 0, ReentrantLock())
end

function Logging.handle_message(logger::Logger, level, message, args...; kwargs...) 
    prefix = repeat("  ", logger.indent) 
    message = prefix * message
    Logging.handle_message(logger._logger, level, message, args...; kwargs...)
end

Logging.min_enabled_level(logger::Logger) = Logging.min_enabled_level(logger._logger)

function Logging.shouldlog(logger::Logger, level, _module, group, id) 
    # filter annoying TranscodingStreams
    if group == :stream
        return false 
    end
    return Logging.shouldlog(logger._logger, level, _module, group, id)
end

const _logger = Logger()
global_logger(_logger)


"""
 Add a level of indentation to the log. Should be followed by a `deindent_log` call 
"""
function indent_log()
    lock(_logger._lock)
    _logger.indent += 1 
    unlock(_logger._lock)
end
function deindent_log()
    lock(_logger._lock)
    _logger.indent -= 1
    unlock(_logger._lock)
end

@info "Started with $(Threads.nthreads()) threads"

end



function _wave_sample(Nobs, phase = 0)
    0.4 * sin.(collect(1:Nobs) / Nobs * 2 * pi  .+ phase * pi / 4) .+ 0.5
end
