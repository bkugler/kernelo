using Kernelo.Probas
using Kernelo.ModeFinding
using Plots

function test_plot_mode_finding()
    L, K = 2, 30
    gmm = Gmm{Float64}(rand(K), rand(L, K), [CholCov{Float64}(L) for k in 1:K])
    gmm.weights ./= sum(gmm.weights)

    starts = gmm.means

    res = ModeFinding.search_modes(gmm, starts, 1e-10)
    
    modes = hcat([r.x for r in res if r.is_maximum]...)
    @info "found $(size(modes)[2]) modes"

    xs = range(-1, stop = 2, length = 100)
    ys = range(-1, stop = 2, length = 100)
    dens = (x, y)->Probas.densite_melange([x,y], gmm, zeros(L))
    p = contour(xs, ys, dens; levels = 200, colorbar = false, dpi  = 200, size = (1000, 1000))
    scatter!(p, modes[1,:], modes[2,:], label = "modes", markersize = 10)
    for k in 1:K
        scatter!(p, gmm.means[1,k:k], gmm.means[2,k:k], label = k <= 4 ? "centroid_$k" : "", markershape = :square, markersize = 6)
        plot!(p, res[k].history[1,:], res[k].history[2,:], label = k <= 4 ? "path_$k" : "", color = p.series_list[end].plotattributes[:linecolor], linestyle = :dash, markersize = 2) 
    end
    png(p, "/scratch/WORK/gmm.png")
    p = surface(xs, ys, dens)
    p
end
test_plot_mode_finding()

