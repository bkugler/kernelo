using Kernelo.Models
using Kernelo.Pred
using Plots 
using LaTeXStrings
using RCall

include("../k_selection.jl")
include("base_plots.jl")

function plot_hockey_stick(contexts::Vector, preds::Vector{Vector{Prediction}}, savepath::String)
    # target empirical relation 
    bref = collect(range(0, 1; length = 100))
    cref = Models.Crelation(bref)

    subplots = [ ]
    for pred in preds[1]
        subp = plot(; xlabel = contexts[1].variables_names[3], ylabel = contexts[1].variables_names[4], ylims = (-0.2, 1.2),  title = pred.config.label)
        plot!(subp, bref, cref, label = "Empirical Hockey stick relation")
        push!(subplots, subp)
    end

    shapes = [ :circle, :rect, :star5, :diamond, :hexagon, :cross, :xcross, :utriangle, :dtriangle, :rtriangle, :ltriangle, :pentagon, :heptagon, :octag]
    for (context, context_preds, shape) in zip(contexts, preds, shapes) 
        for (i, pred) in enumerate(context_preds) # for different methods 
            _, imax = size(pred.Xmean)
            colors = [RGBA(0.1, 1 - i, i, 1) for i in range(0, 1; length = imax)]
            scatter!(subplots[i], pred.Xmean[3,:], pred.Xmean[4,:], label = context.LABEL, markershape = shape, markercolor = colors)
        end
    end

    p = plot(subplots..., layout = (1, length(subplots)), size = (1000, 600))
    png(p, savepath)
    @debug "Save in $(savepath)"
end

function plot_k_selection(Ks::Vector, res::KSelection, Ystd::Float64, D::Int; savepath = nothing)
    # different scales 
    p1 = plot(Ks, hcat(res.direct_error, res.sigma_error), 
    labels = ["Reconstruction error" L"\overline{\mathrm{Tr}\Sigma}"],
    background_color_legend = Config.LEGEND_BACKGROUND)
    plot!(p1, Ks, ones(length(Ks)) .* Ystd^2 * D, label = L"D \sigma^2")

    p2 = plot(Ks, hcat(res.bic, res.bic_test), 
    labels = ["BIC" "BIC - Test dataset"],
    background_color_legend = Config.LEGEND_BACKGROUND)
    _, i_best_1 = findmin(res.bic)
    _, i_best_2 = findmin(res.bic_test)
    K_best1, K_best2 = Ks[i_best_1], Ks[i_best_2]
    vline!(p2, [K_best1] ; label = "\$K_{best, BIC} = $(K_best1)\$")
    vline!(p2, [K_best2] ; label = "\$K_{best, BIC-Test} = $(K_best2)\$")


    p3, i_best_slope = plot_slope_heuristic(Ks, res)
    p = plot(p1, p2, p3, plot(title = "\$\\sigma\$ = $Ystd", framestyle = :none); layout = (2, 2))    
    if savepath !== nothing 
        png(p, savepath)
        @info "Saved in $savepath"
    end
    i_best_1, i_best_slope
end


function plot_slope_heuristic(Ks::Vector, res::KSelection)
    ll = res.log_likelihood
    dims = res.dims

    a, b = robust_regression(dims, ll)
    ll_pen = ll - 2 * a * dims
    _, i_best = findmax(ll_pen)
    Kbest = Ks[i_best]
    p = plot(dims, hcat(ll, ll_pen, a .* dims .+ b), 
        dpi = 300,
        labels = [L"L"  L"L_{pen}"  "Robust linear fit"],
        xlabel = "Number of parameters",
        background_color_legend = Config.LEGEND_BACKGROUND,
        )
    vline!(p, [dims[i_best]] ; label = "\$K_{best} = $Kbest\$")
    p, i_best
end

"""
 Use R function rlm 
https://www.rdocumentation.org/packages/MASS/versions/7.3-51.6/topics/rlm
"""
function robust_regression(dims::Vector, log_ll::Vector)
    @debug "Calling R from Julia ..."
    r_out = RCall.R"""
    library(MASS)

    SH <- function (dims,log_ll,disp=FALSE) {  
        dd=data.frame(nbp=dims,ll=log_ll)
        fit=rlm(ll  ~ nbp,data=dd,method='M')
        if(fit$coefficients[2]<0) fit$coefficients[2]=0
        if (disp){
            dd$llpen = dd$ll- 2* fit$coefficients[2]*dd$nbp
            plot(dd$nbp,dd$ll,type='p')
            abline(fit,col='red')
        }
        fit$coefficients
    }

    SH($dims, $log_ll)
    """
    @debug "Done."
    v = RCall.rcopy(r_out)
    intercept, slope = v[1], v[2]
    slope, intercept
end
