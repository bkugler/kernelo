const precision_gaussian_kernel = 0.01
const step_size = 0.01
const levels = 100

_norm2(x) = sum(x.^2)
_to_cart(x) = [x[2] * cos(x[1]), x[2] * sin(x[1]) ]

function interpolate(x, xrefs::Matrix, yrefs::Vector)
    L, N = size(xrefs)
    s = 0
    out = 0
    x = _to_cart(x)
    for i in 1:N
        li = exp(- precision_gaussian_kernel * _norm2(x .- _to_cart(xrefs[:, i])))
        s += li 
        out += li * yrefs[i]
    end
    out / s
end

function plot_sobol(geoms::Matrix, sobols::Matrix; varnames = nothing, normalize = true)
    L, D = size(sobols)
    incidences = unique(geoms[:, 1])
    pls = []
    varnames = varnames == nothing ? ["x$i" for i in 1:L] : varnames
    max = maximum(sobols)
    for i in 1:L 
        for inc in incidences 
            ids = geoms[:, 1] .== inc
            xrefs = geoms[ids, [3,2]] # inversion due to polar plot
            xrefs[:, 1] = xrefs[:, 1] * pi / 180 # azimuth en radians
            xrefs = Matrix(xrefs')
            yrefs = sobols[i, ids]
            f = (x, y)->x >= 0 ? interpolate([x, y], xrefs, yrefs) : interpolate([-x, y], xrefs, yrefs)
            p = contour(-pi:step_size:pi, 0:90, f, fill = true, proj = :polar, levels = levels, c = :rainbow, title = "$(varnames[i]) ; inc = $(inc)",
                clims = normalize ? (0, max) : :auto,
                # colorbar = i == 1,
                tickfontsize = 14,
                bg_legend = RGBA(1, 1, 1, 0.2))
            scatter!(p, xrefs[1,:], xrefs[2,:], proj = :polar, marker_z = yrefs, c = :rainbow, label = "samples", legendfontsize = 12)
            push!(pls, p)
        end 
    end
    plot(pls..., layout = (L, length(incidences)), size = (2000, 2200))
end