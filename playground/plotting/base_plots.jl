using Plots
using LinearAlgebra
using LaTeXStrings
import Colors

using Kernelo.Pred
using Kernelo.Noise 
using Kernelo.Models 

Plots.pyplot()

mutable struct _config 
    LEGEND_FONT_SIZE::Int
    LEGEND_BACKGROUND::Colors.RGBA
    DPI::Int
    CIRCLE_SIZE::Int
end
const Config = _config(13, Colors.RGBA(0.9, 0.9, 0.9, 0.3), 100, 4) 

struct PlotsParams 
    solutions::Vector{Symbol}
    hockey_stick::Bool
    observables::Bool 
    density_1D::Bool 
    density_2D::Bool 
    true_density_2D::Bool
    indexes::Union{Vector{Int},Function}
end
PlotsParams(solutions; hockey_stick = false, observables = false, density_1D = false, density_2D = false,  true_density_2D = false, indexes = []) = PlotsParams(solutions, hockey_stick, observables, density_1D, density_2D, true_density_2D, indexes)

axe_formatter(x) = string(round(x, digits = 3))



"""
 Returns last colors 
"""
last_color(p::Plots.Plot) = p.series_list[end].plotattributes[:linecolor]
all_colors(p::Plots.Plot) = [ l.plotattributes[:linecolor] for l in p.series_list ]

"""
 Plot a fill zone around mean 
"""
function _plot_cov!(p::Plots.Plot, mean, cov, x_labels, label)
    plot!(p, x_labels, mean + sqrt.(cov), fillrange = mean - sqrt.(cov), fillalpha = 0.10, label = label * " (Std)",
    linealpha = 0, fillcolor = last_color(p))
end



# ---------------------------------------------------------------------------------------------------------- #
# ----------------------------------------------- x Predictions -------------------------------------------- # 
# ---------------------------------------------------------------------------------------------------------- #




"""
 Plot Xmean (and X2mean)component by component.Xcovs (and X2covs)should be diagonal
 If given, add Xcenters 
"""
function _plot!(subplot, x, x_ticks, config::SerieConfig)
    kwargs = Dict{Symbol,Any}(:label => config.label, :color => config.color, :linewidth => 2, :yformatter => axe_formatter, :linealpha => config.alpha, :markeralpha => config.alpha)
    if config.format == :basic 
        plot!(subplot, x_ticks, x; kwargs...)
    elseif config.format == :circle
        kwargs[:markersize] = Config.CIRCLE_SIZE
        kwargs[:markerstrokewidth ] = 0.1
        scatter!(subplot, x_ticks, x; kwargs...)
    elseif config.format == :dashed 
        kwargs[:linewidth], kwargs[:linestyle] = 2, :dot
        plot!(subplot, x_ticks, x; kwargs...)
    else
        error("Unknown format $(config.format)")
    end
end

"""
 If `hide_diagnostics`, the legend is plotted in place of diagnostics 
"""
function plot_components(c::AbstractContext, predictions::Vector{Prediction}; savepath = nothing,
                         x_ticks = nothing, xlabel = "observation index", varnames = nothing, main_title = "", hide_diagnostics = true,
                         y_lims = nothing)
    L, N = size(predictions[1].Xmean)
    x_ticks = (x_ticks === nothing) ? collect(1:N) : x_ticks
    varnames = (varnames === nothing) ? ["x$i" for i in 1:L] : varnames

    # diagnostics plot 
    subp_diagnostics = plot(xlabel = xlabel, ylabel = "diagnostic",
                     yscale = :log10,
                    title = "$(main_title) - Diagnostics", 
                    background_color_legend = Config.LEGEND_BACKGROUND,
                    legendfontsize = Config.LEGEND_FONT_SIZE,
                    )
    linestyles = [:solid, :dash, :dot, :dashdot, :dashdotdot]
    for pred in predictions
        for ((label, diagnos), ls) in zip(pairs(pred.diagnostics), Iterators.cycle(linestyles))
            plot!(subp_diagnostics, x_ticks, diagnos; color = pred.config.color, linestyle = ls,
                    label = "$label - $(pred.config.label)")
        end
    end

    reconstruction_legend = raw"$\frac{|| y_{obs} - F(x) ||}{|| y_{obs} ||} $"

    subpY_relative_err = plot(xlabel = xlabel, ylabel = reconstruction_legend, 
                        yscale = :log10, legend = false,
                        title = "Relative reconstruction error")
    for pred in predictions
        if pred.Yerr !== nothing 
            _plot!(subpY_relative_err, pred.Yerr, x_ticks, pred.config)
        end
    end

  
    subp_Xerr = plot(xlabel = xlabel, ylabel = raw"$|| x_{obs} - x_{pred} ||$", yscale = :log10,
    title = "Error", background_color_legend = Config.LEGEND_BACKGROUND,
    legendfontsize = Config.LEGEND_FONT_SIZE,
    )
    for pred in predictions
        if pred.Xerr !== nothing 
            _plot!(subp_Xerr, pred.Xerr, x_ticks, pred.config)
        end
    end
  
    # outer legend plot : plot without data
    subp_legend = plot(; framestyle = :none, legend = :right, background_color_legend = Config.LEGEND_BACKGROUND,  legendfontsize = Config.LEGEND_FONT_SIZE)

    subplots_X = []
    for l in 1:L 
        ylim = y_lims === nothing ? :auto : y_lims[l]
        subp = plot(xlabel = xlabel, legend = false, ylabel = varnames[l],
                    title = (l == 1) ? "Prediction by components" : "", color_palette = :inferno,
                    ylims = ylim, background_color_legend = Config.LEGEND_BACKGROUND,
                    legendfontsize = Config.LEGEND_FONT_SIZE,
                    )
        for pred in predictions
            Xmean = pred.Xmean
            _plot!(subp, Xmean[l,:], x_ticks, pred.config)
            if l == 1  # we add the legend on a blank plot
                _plot!(subp_legend, [1], [1], pred.config)
            end
            if pred.Xcovs !== nothing && !pred.config.hide_std
                _plot_cov!(subp, Xmean[l,:], pred.Xcovs[l,:], x_ticks, pred.config.label)
                if l == 1  # we add the legend on a blank plot
                    _plot_cov!(subp_legend, [1], [1], [1], pred.config.label)
                end
            end
        end
        
        push!(subplots_X, subp)
    end

    # we eventually aggregate the subplots
    # for 4 variable, with legend and Y err, we want the following layout 
    #   | X X | | Y | 
    #   | X X | | L |

    subplots = []

    first_row_X = length(subplots_X) >= 2 ? 2 : 1
    push!(subplots, subplots_X[1:first_row_X]...)

    if length(subp_Xerr.series_list) > 0
        push!(subplots, subp_Xerr)
    end

    if length(subpY_relative_err.series_list) > 0
        push!(subplots, subpY_relative_err)
    end

    if length(subplots_X) >= 3 
        push!(subplots, subplots_X[3:end]...)  
    end

    if hide_diagnostics 
        push!(subplots, subp_legend)
    elseif length(subp_diagnostics.series_list) > 0
        push!(subplots, subp_diagnostics)
    end

    if length(subplots) % 2 == 1
        push!(subplots, plot(; framestyle = :none))
    end
    mainplot = plot(subplots..., layout = (2, Int(length(subplots) / 2)), 
        dpi = Config.DPI,  size = (length(subplots) * 350, 1000))  
    if savepath !== nothing
        Plots.png(mainplot, savepath)
        @debug "Saved in $(savepath)"
    end
    mainplot
end



# ---------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- Noise estimation ------------------------------------------ #
# ---------------------------------------------------------------------------------------------------------- #

__cov_to_vec(m::Union{UniformScaling,Number}) = m * [1]
__cov_to_vec(m) = diag(m)

"""
Plot noise estimation iterations, by comparing with the ground truth
"""
function plot_noise_estimation(context, params, history::Noise.History{T},
                                synthetic::Noise.Bias,  estimator::Noise.Bias; savepath = nothing, colors::Pair{String,String} = "red" => "blue") where {T}

    true_std = __cov_to_vec(synthetic.sigma)                            
    errs = [norm(sqrt.(__cov_to_vec(t[1].sigma)) .- true_std) for t in history]
    Niter = length(errs)
    p2 = plot(errs, xlabel = "Iteration \$r\$", 
        label = L"|| \sigma^r_D - \sigma_D ||_{2}",
        legendfontsize = Config.LEGEND_FONT_SIZE,
        color = colors.second,
        # ylims = (1e-4, 1)
        ylabel = "standard deviation error", 
        yscale = :log10)

    estim_std = sqrt.(__cov_to_vec(estimator.sigma))
    err_estim = norm(estim_std .- true_std)
    plot!(p2, 1:Niter, err_estim .* ones(Niter), linestyle = :dash, 
        color = colors.first,
        label = L"|| \sigma^{est}_D - \sigma_D ||_{2}")

    p = plot(p2, dpi = Config.DPI)
    if savepath !== nothing
        Plots.png(p, savepath)
        @info "Figure wrote in $(savepath)"
    end
    p
end



# ---------------------------------------------------------------------------------------------------------- #
# -------------------------------------------- RTLS representation ----------------------------------------- #
# ---------------------------------------------------------------------------------------------------------- #

function plot_rtls(context::HapkeContext, rtlss::Vector{Matrix{T}}, labels = nothing; add_title = "", savepath = nothing) where {T}
    I = length(rtlss)
    labels = (labels === nothing) ? fill("Observations", I) : labels
    p = plot(title = context.LABEL * " - RTLS space - " * add_title, xlabel = "\$k_L\$", ylabel = "\$k_G\$", zlabel = "\$k_V\$")
    for i in 1:I
        rtls = rtlss[i]
        scatter!(p, rtls[1,:], rtls[2,:], rtls[3,:], markershape = :auto, label = labels[i])
    end
    if savepath !== nothing
        Plots.png(p, savepath)
        @info "Figure wrote in $(savepath)"
    end
    p
end
plot_rtls(c, rtls::Matrix) = plot_rtls(c, [rtls])


# ---------------------------------------------------------------------------------------------------------- #
# --------------------------------------------- Density ---------------------------------------------------- #
# ---------------------------------------------------------------------------------------------------------- #

function plot_density_2D(f::Function, L::Int, xrefs::Tuple{Vector,String} ...; xnames = nothing, levels = 50, savepath = nothing, resolution = 100, xlims = nothing) 
    xnames = xnames === nothing ? ["x$i" for i in 1:L] : xnames
    xlims = xlims === nothing ? [(0, 1) for i in 1:L] : xlims
    nbPlots::Int, current = L * (L - 1) / 2, 1
    subplots = []
    for i = 1:L 
        xs = range(xlims[i][1], length = resolution, stop = xlims[i][2])
        for j = (i + 1):L
            ys = range(xlims[j][1], length = resolution, stop = xlims[j][2])
            @debug "Plot $(current) / $(nbPlots)..."
            current += 1
            sb = plot(xlabel = xnames[i], ylabel = xnames[j], title = "density $(xnames[i]), $(xnames[j])", 
                legend = i == 1 && j == 2, colorbar = false, margin = 5mm, tickfontsize = 5, yformatter = axe_formatter)

            partialF = (x, y)->f(x, y, i, j)
            contour!(sb, xs, ys, partialF, levels = levels)
            for (xref, label) in xrefs
                scatter!(sb, [xref[i]], [xref[j]],  label = label, markersize = 3, markerstrokewidth = 0.2)
            end
            push!(subplots, sb)
        end
    end
    p = plot(subplots..., dpi = 200,  plot_title = "density")
    if savepath !== nothing
        Plots.png(p, savepath)
        @debug "Saved in $(savepath)"
    end
    p
end

function plot_density_1D(f::Function, L::Int, xrefs::Tuple{Vector,String} ...; xnames = nothing, savepath = nothing, resolution = 100, subplot_size = (600, 400), 
        xlims = nothing, label = "density") 
    xnames = xnames === nothing ? ["x$i" for i in 1:L] : xnames
    xlims = xlims === nothing ? [(0, 1) for i in 1:L] : xlims
    nbPlots::Int, current = L, 1
    subplots = []
    for i = 1:L 
        xs = range(xlims[i][1], length = resolution, stop = xlims[i][2])
        x_ticks = round.(range(xlims[i][1], length = 10, stop = xlims[i][2]) * 100) ./ 100
        @debug "Plot $(current) / $(nbPlots)..."
        current += 1
        sb = plot(xlabel = xnames[i], ylabel = label, title = "density $(xnames[i])", 
                legend = i == 1, colorbar = false, margin = 5mm, tickfontsize = 5, xlims = xlims[i], yticks = [], xticks = x_ticks)
        partialF = (x)->f(x, i)
        plot!(sb, xs, partialF.(xs))
        for (xref, label) in xrefs  
            plot!(sb, [xref[i]], seriestype = :vline, label = label, markersize = 3, markerstrokewidth = 0.2)
        end
        push!(subplots, sb)
    end
    p = plot(subplots..., layout = (1, L), dpi = 100,  plot_title = "density", size = (subplot_size[1] * L, subplot_size[2]))
    if savepath !== nothing
        Plots.png(p, savepath)
        @debug "Saved in $(savepath)"
    end
    p
end

# ---------------------------------------------------------------------------------------------------------- #
# --------------------------------------------- Map -------------------------------------------------------- #
# ---------------------------------------------------------------------------------------------------------- #

# function latlong_to_xy(latlong::Matrix)
#     _, N = size(latlong)
#     origin = LLA(latlong[1,1], latlong[2,1], 0)
#     trans = ENUfromLLA(origin, wgs84)
#     xs, ys = [], []
#     for i in 1:N
#         enu = trans(LLA(latlong[1,i], latlong[2,i], 0))
#         push!(xs, enu.n)
#         push!(ys, enu.e)
#     end
#     xs, ys
# end

# function plot_map(values::Vector{Tuple{Vector{T},String}}, latlong::Matrix; markersize=8.5, figsize=(1000, 1000), color_palette=:rainbow) where {T}
#     xs, ys = latlong_to_xy(latlong)
#     min = minimum(minimum(t[1]) for t in values)
#     max = maximum(maximum(t[1]) for t in values)
#     subp = []
#     for (vals, label)  in values
#         push!(subp, scatter(xs, ys, zcolor=vals, title=label, colorbar=:best,
#         markersize=markersize, markercolor=color_palette, clims=(min, max), label=label,
#         legendfontsize=Config.LEGEND_FONT_SIZE, legend=:left,
#         xlims=(minimum(xs), maximum(xs) * (1 + 0.15)) # colorbar bug
#         ))
#     end
#     plot(subp..., dpi=Config.DPI, size=figsize)
# end

# function _normalize(data::Matrix)
#     vmin, vmax = minimum(data), maximum(data)
#     if 0 <= vmin && vmax <= 1 
#         # we dont normalize valid series
#         return data
#     end
#     if vmin == vmax 
#         # degenerated case 
#         return ones(size(data)) .* 0.5 
#     end
#     return (data .- vmin) ./ (vmax - vmin)
# end

# """ Plot a map of RGB colors for each element of `values` """
# function plot_map_color(values::Vector{Tuple{Matrix{T},String}}, latlong::Matrix; markersize=8.5, figsize=(1000, 1000)) where {T}
#     xs, ys = latlong_to_xy(latlong)
#     subp = []
#     for (vals, label)  in values
#         vals = _normalize(vals)
#         N, _ = size(vals)        
#         colors = [RGB(vals[n,1], vals[n,2], vals[n,3]) for n in 1:N]
#         push!(subp, scatter(xs, ys, mc=colors, title=label, markersize=markersize, label=label,
#         legendfontsize=Config.LEGEND_FONT_SIZE ,
#         ))
#     end
#     plot(subp..., dpi=Config.DPI, size=figsize)
# end


# ---------------------------------------------------------------------------------------------------------- #
# ---------------------------------------- Hapke's observables (polar plot) -------------------------------- #
# ---------------------------------------------------------------------------------------------------------- #

"""
Plots geometries in the principal plan, that is with azimut = 0 or 180.
"""
function plot_polar(Yobs::Matrix, geometries::Matrix, labels::Vector, colors::Vector; 
    inc = nothing, savepath = nothing, title::Union{Function,Nothing} = nothing) 
    D, _ = size(geometries)
    # regroupe by incidence
    sorted_indexes = Dict{Float64,Vector{Int}}() # incidence -> emergences
    for d in 1:D
        is_principal = geometries[d,3] == 0 || geometries[d,3] == 180
        match_inc = inc === nothing || geometries[d, 1] == inc
        if match_inc  && is_principal 
            l = get(sorted_indexes, geometries[d, 1], Vector{Int}())
            push!(l, d)
            sorted_indexes[geometries[d, 1]] = l
        end
    end
    subp = []
    subp_legend = plot(; framestyle = :none)
    first = true 
    # colors must be a (1, _) matrix 
    colors = permutedims(hcat(colors))

    if title === nothing 
        title = inc->"incidence $inc"
    end 
    for (k, v) in sorted_indexes 
        emes = []
        for i in v 
            eme, azi = geometries[i, 2], geometries[i, 3]
            if azi == 180 
                eme =  90 + eme
            else 
                eme = 90 - eme
            end
            push!(emes, eme * π / 180)
        end
        # sort emergences for pretty visualisation
        perm = sortperm(emes)
        inc_rad = (90 - k) * π / 180
        p = plot(emes[perm], Yobs[v[perm], :], color = colors, proj = :polar, marker = :o, 
            legend = false, 
            xlims = (0, π), 
            title = title(k[1]), 
            label = permutedims(labels),
            legendfontsize = Config.LEGEND_FONT_SIZE,
        )
        plot!(p, [inc_rad, inc_rad], [0.,maximum(Yobs[v, :]) * 1.1], label = "incidence")
        push!(subp, p)
        # copy the legend 
        if first 
            plot!(subp_legend, [1], Yobs[1:1, :], color = colors, label = permutedims(labels))
            plot!(subp_legend, [1], [1], label = "incidence")
        end
        first = false
    end 
    p = plot([subp..., subp_legend]..., dpi = Config.DPI)
    if savepath !== nothing 
        Plots.png(p, savepath)
        @info "Saved in $savepath"
    end
    p
end
