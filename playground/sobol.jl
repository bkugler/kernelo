using Kernelo
using Kernelo.Models
using Kernelo.Gllim 
using Kernelo.Sobol 
using Kernelo.Plotting
using Kernelo.Probas
using Kernelo.Archive

using Plots 

include("procedure.jl")


struct SobolGeoms 
    incs::Vector 
    emes::Vector 
    phis::Vector 
end


function sample_geometries(sg::SobolGeoms)
    I,  E, P =  length(sg.incs), length(sg.emes), length(sg.phis)
    out = zeros(I * E * P, 3)
    offsets = [-3,0,3] # in degrees
    index = 1
    for i in sg.incs 
        for e in sg.emes
            for p  in sg.phis 
                out[index, :] = [i,e + rand(offsets) ,p + rand(offsets) ]
                index += 1
            end
        end
    end
    out
end

"""
 Should be call once with `reset` = true 
"""
function setup_geometries(sg::SobolGeoms, reset::Bool) 
    if reset 
        geoms = sample_geometries(sg)
        Archive.save(geoms, Archive.FOLDER_MISC, :sobol_geoms, sg)
    end 
    Archive.load(Archive.FOLDER_MISC, :sobol_geoms, sg)
end




function run_sobol(c::AbstractContext, gllim::GLLiM, run::Bool)
    if run
        @time sobols_d, _ = Sobol.indice_sobol_direct(gllim)
        @time sobols_d2, _ = Sobol.indice_sobol_direct(Sobol.FunctionnalGLLiM(gllim), 10000)
        @time sobols_d3, _ = Sobol.indice_sobol_direct(c)
        @time sobols_i, determination = Sobol.indice_sobol_inverse(gllim)
        @time sobols_i2, _ = Sobol.indice_sobol_direct(Sobol.FunctionnalGLLiMInverse(gllim), 1000)
        sobols_i2 = Matrix(sobols_i2') # to respect plot convention
        data = collect(zip([sobols_d, sobols_d2, sobols_d3, sobols_i, determination, sobols_i2],
            ["sobol direct gllim","sobol direct F gllim","sobol direct F","sobol inverse", "determination",  "sobol inverse F-1"]))
        Archive.save(data, Archive.FOLDER_MISC, c, gllim)
    end
    l_sobols = Archive.load(Archive.FOLDER_MISC, c, gllim)
end 


function run_plot_sobol(sampled_geoms, run, normalize)
    gp = Gllim.GLLiMTraining(Gllim.MultiInit(), 50; max_iteration = 75, Ntrain = 100000)
    c = Models.CustomHapkeContext(sampled_geoms)
    if run >= 2
        gllim = inference(c, gp) 
        Archive.save(gllim, Archive.FOLDER_GLLIM, c, gp)
    end 
    gllim = Archive.load(Archive.FOLDER_GLLIM, c, gp)
    l_sobols = run_sobol(c, gllim, run >= 1)

    varnames = c.variables_names
    for (s, label) in l_sobols
        plot_one_sobol(label, s, varnames, normalize)
    end
end 


function plot_one_sobol(label, sobols, varnames, normalize)
    p = Plotting.plot_sobol(sampled_geoms, sobols; varnames = varnames,normalize = normalize)
    path = Archive.get_path(Archive.FOLDER_GRAPHS, sampled_geoms, normalize; label = label)
    png(p, path)
    @info "Saved in $path"
end