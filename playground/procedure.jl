using Dates
import Base.GC 
using Printf
using LinearAlgebra
using JSON 
using Dates 
using Statistics

using Kernelo.Probas
using Kernelo.Gllim
using Kernelo.Is 
using Kernelo.Models
using Kernelo.Pred
using Kernelo.ModeFinding
using Kernelo.Noise
using Kernelo.Archive

include("methods.jl")
include("setup.jl")
include("comptimes.jl")

const REG_COVAR = 1e-8


"""
Define Importane Samping options.
"""
abstract type IsParams end 
"""
 Must return the std of the observation 
"""
get_obs_std(params::IsParams, index::Int, yobs::VecOrSub) = error("not implemented")
"""
 Must return the std used in the training step 
"""
get_train_std(params::IsParams) = params.train_std
get_Ns(params::IsParams) = params.Ns 

struct FixedCovIs  <: IsParams
    train_std::Number
    obs_stds::Matrix
    Ns::Int
end
get_obs_std(params::FixedCovIs, index::Int, yobs::VecOrSub) = params.obs_stds[:, index]

struct DataCovIs <: IsParams 
    train_std::Number
    std_ratio::Int 
    Ns::Int 
end 
get_obs_std(params::DataCovIs, index::Int, yobs::VecOrSub) = yobs ./ params.std_ratio


mutable struct FixedObsCovariance{T <: Number} <: IsModel
    proposition::IsProposition

    context::AbstractContext
    y::VecOrSub{T}
    cov::DiagCov{T}

    mu::Vector{T}
    FixedObsCovariance{T}(c, y, s) where {T <: Number} = new(Is.UniformProposition(c.L), c, y, s, zeros(length(y))) # we allocated the zeros once for all
end

@inline Is.get_proposition(m::FixedObsCovariance) = m.proposition
# we want to compute the expectancy of X so F(X) = X and D = L
@inline Is.get_L(m::FixedObsCovariance) = m.context.L
function Is.log_p(m::FixedObsCovariance{T}, x::VecOrSub{T}, _::Int) where {T}
    Models.log_conditionnal_density(m.context, x, m.y, 1., m.mu, m.cov)
end


"""
 Requires the context to compute the errors 
"""
mutable struct CompleteInversion 
    key::String

    mean::Vector{Float64}
    cov::Union{Vector{Float64},Nothing}
    y_err::Float64
    x_err::Union{Float64,Nothing} # for testing, we may know the "true" x
    
    diagnostics::Dict{String,Float64}
end
CompleteInversion(key::String, pred::IsInversion) = CompleteInversion(key, pred.mean, pred.cov, 0, nothing, Dict("ESS"  => pred.diagnostics.ess, "ES" => pred.diagnostics.es, "QN" => pred.diagnostics.qn))
CompleteInversion(key::String, mean::Vector; cov = nothing) = CompleteInversion(key, mean, cov, 0, nothing, Dict())


function ComputationTime(context::AbstractContext, gllim_params::EMMetaGLLiM, is_params::IsParams, Nobs::Int) 
    ComputationTime(0, ComputationInvertion, Millisecond(0), Millisecond(0), Millisecond(0), "inversion - " * context.LABEL, context.L, context.D, gllim_params.K, gllim_params.Ntrain, Nobs, is_params.Ns)
end
function ComputationTime(context::AbstractContext, gllim_params::EMMetaGLLiM, noise_params::EmIsGLLiMParams, Nobs::Int)
    ComputationTime(0, ComputationNoise, Millisecond(0), Millisecond(0), Millisecond(0), "noise estimation - " * context.LABEL, context.L, context.D, gllim_params.K, gllim_params.Ntrain, Nobs, noise_params.Ns)
end



"""
 Returns the duration since`ti`
"""
function since(ti::DateTime)::Millisecond
    return Dates.now() - ti
end

function _safe_cholesky(covs::FullCovs{T}) where {T}       
    chol_covs = CholCovs{T}(undef, length(covs))
    for (i, M) in enumerate(covs)
        try
            chol_covs[i] = cholesky(M).L
        catch
            M += UniformScaling(REG_COVAR)
            chol_covs[i] = cholesky(M).L
        end
    end
    chol_covs
end

function Y_relative_err(context, yobs::VecOrSub, xpred::VecOrSub, a, mu)
    L, D = context.L, context.D
    valid = Models.mask(context, xpred, L)
    if !valid 
        return NaN 
    end
    calc = zeros(D)
    Models.f!(context, xpred, calc, D)
    calc .= a * calc .+ mu .- yobs
    return norm(calc) / norm(yobs) # relative
end
function Y_relative_err(context, Yobs, X, a, mu)
    _, N  = size(Yobs)
    out = zeros(N)
    for n in 1:N 
        out[n] = Y_relative_err(context, Yobs[:,n], X[:, n], a, mu)
    end
    return out
end

function add_y_err(context, yobs, pred::CompleteInversion)::CompleteInversion  
    pred.y_err = Y_relative_err(context, yobs, pred.mean, 1., 0.)
    pred
end

function x_denorm(context, xobs, pred::CompleteInversion)::CompleteInversion    
    # X error, in math space (before de-normalisation)
    if xobs != nothing 
        L = length(xobs)
        pred.x_err =  norm(xobs .- pred.mean) / L
    end

    # de-normalization 
    pred.mean, pred.cov = Models.to_physique(context, pred.mean, pred.cov)
    pred
end

"""
 Since we consider mode finding only as experimental, it's more coherent 
to remove it from the candidates 
"""
function best_residual(key::String, inversions::Vector{CompleteInversion}; ignore_mode_finding = true) 
    if ignore_mode_finding 
        is_mode_finding(key::String) = startswith(key, Methods.GllimModeFromBestCentroid("")) || startswith(key, Methods.GllimModeFromMergedCentroid("")) 
        inversions = [inv for inv in inversions if !is_mode_finding(inv.key)]
    end
    err, index = findmin([(isnan(iv.y_err) ? Inf : iv.y_err) for iv in inversions])
    best = inversions[index]
    return CompleteInversion(key, best.mean, best.cov, isnan(best.y_err) ? Inf : best.y_err, best.x_err, best.diagnostics)
end

_chol_cov_to_diag(chol::LowerTriangular) = diag(chol * chol')

function _find_modes(complete_gmm::Gmm, merged_centroids::Matrix, merged_covs::CholCovs) 
    _, K = size(merged_centroids)

    # we select the K highest weights in the full mixture
    idxs = sortperm(complete_gmm.weights; rev = true)[1:K]
    centroids1 = complete_gmm.means[:, idxs]
    chol_covs_centroids = complete_gmm.chol_covs[idxs]

    bc = [ CompleteInversion(Methods.GllimBestCentroid(k), centroids1[:,k]; cov = _chol_cov_to_diag(chol_covs_centroids[k]))   for k in 1:K  ]
    mc = [ CompleteInversion(Methods.GllimMergedCentroid(k), merged_centroids[:,k]; cov = _chol_cov_to_diag(merged_covs[k]))   for k in 1:K  ]

    # we run mode finding starting from merged centroids OR "best" centroids
    res1 = ModeFinding.search_modes(complete_gmm, centroids1)
    res2 = ModeFinding.search_modes(complete_gmm, merged_centroids)

    # we keep only the modes, we replace non modes by the start     
    mbc = [ CompleteInversion(Methods.GllimModeFromBestCentroid(k), res.is_maximum ? res.x : centroids1[:, k], nothing, 0, nothing, Dict("Mode" => res.is_maximum ? 10 + k : 1))   for (k, res) in enumerate(res1) ]
    mmc = [ CompleteInversion(Methods.GllimModeFromMergedCentroid(k), res.is_maximum ? res.x : merged_centroids[:, k], nothing, 0, nothing, Dict("Mode" => res.is_maximum ? 20 + k : 1))   for (k, res) in enumerate(res2) ]
    return [bc..., mc..., mbc..., mmc...], hcat([ pred.mean for pred in  mmc]...)
end

# use (relative) reconstuction as pertinence
function indice_multi_solutions(preds::Vector{CompleteInversion})
    L = length(preds[1].mean)
    mean = zeros(L)
    s = 0.
    filtered::Vector{CompleteInversion} = []
    for pred in preds 
        # filter out invalid solutions
        if !( all(isfinite.(pred.mean)) && isfinite(pred.y_err))
            continue
        end
        push!(filtered, pred)
        pertinence = exp(-0.5 * pred.y_err^2)
        s += pertinence
        mean .+= pertinence .* pred.mean
    end
    mean ./= s # normalisation

    # theoretical bound 
    bound = 0.25 - (norm(mean .- 0.5)^2 / L)
    
    out = 0.
    for pred in filtered
        out += exp(-0.5 * pred.y_err^2) * sum((pred.mean .- mean).^2) # norm squared
    end
    out = out / (s * L) # normalisation
    out / bound
end

"""
 Monitor is a thread-safe counter which 
log a progress 
"""
mutable struct Monitor
    lock::ReentrantLock
    progress::Int
    total::Int
    step::Int
    function Monitor(total::Int; step = nothing) 
        if step === nothing 
            step = total > 1000 ? 50 : 10
        end
        new(ReentrantLock(), 0, total, step)
    end
end

function update(m::Monitor)
    lock(m.lock)
    m.progress += 1
    if m.progress % m.step == 0 
        percent = round(100 * m.progress / m.total; digits = 2)
        @debug "Inversion $(m.progress) ($percent %)"
        flush(Base.stdout) # usefull on batch mode (cluster)
    end
    unlock(m.lock)
end

"""
    Run simple gllim only prediction
"""
function gllim_prediction(context::AbstractContext, Yobs::Matrix{T}, trained_gllim::GLLiM{T}, params::IsParams) where {T}
    D, N = size(Yobs)
    L = trained_gllim.L
    a, mu = 1., zeros(T, D)
    out_mean, out_cov = zeros(L, N), zeros(L, N)

    ti = Dates.now()
    Threads.@threads for n in 1:N
        # for n in 1:N
        yobs = Yobs[:, n]
        obs_std = get_obs_std(params, n, yobs)
            
            # we shift the covariance of the GLLiM model 
        sigma_obs = Diagonal(obs_std.^2) # we want the covariance
        gllim_shifted = Gllim.add_bias(trained_gllim, 1., 0., sigma_obs)
        gllim_star = Gllim.inversion(gllim_shifted)
            
            # we get the inverse density from the shifted inverse
        weights_all_i, means_all_i = Gllim.conditionnal_density(gllim_star, yobs)
            
            # compute the mean prediction (with std)
        out_mean[:, n] = Probas.mean_mixture(weights_all_i, means_all_i)
        out_cov[:, n] = diag(Probas.covariance_mixture(weights_all_i, means_all_i, gllim_star.Σ))
    end  
    dura = since(ti)
    return out_mean, out_cov,  dura
end 

"""
 Invert observations one by one
"""
function complete_prediction(context::AbstractContext, Yobs::Matrix{T}, trained_gllim::GLLiM{T}, params::IsParams,  Xobs::Union{Matrix{T},Nothing}) where {T}
    
    D, N = size(Yobs)
    L = trained_gllim.L
    K = Pred.K_MERGED
    a, mu = 1., zeros(T, D)
    out = Matrix{CompleteInversion}(undef, 2 + 5 * K + 2, N)
    entropies = zeros(N) # additionnal diagnostics
    Ns = get_Ns(params)

    ti = Dates.now()
    mon = Monitor(N)
    Threads.@threads for n in 1:N
    # for n in 1:N
        yobs = Yobs[:, n]
        xobs = Xobs == nothing ? nothing : Xobs[:, n]
        obs_std = get_obs_std(params, n, yobs)
        
        # we shift the covariance of the GLLiM model 
        sigma_obs = Diagonal(obs_std.^2) # we want the covariance
        gllim_shifted = Gllim.add_bias(trained_gllim, 1., 0., sigma_obs)
        gllim_star = Gllim.inversion(gllim_shifted)
        
        # we get the inverse density from the shifted inverse
        weights_all_i, means_all_i = Gllim.conditionnal_density(gllim_star, yobs)
        
        # we merge the mixture
        mean_mean_gllim, mean_cov_gllim,  weights_i, centroids_mean_merged_gllim, centroids_cov_gllim = Pred.merge_predict(weights_all_i, means_all_i, gllim_star.Σ)
        
        chol_covs_merged_centroids = _safe_cholesky(centroids_cov_gllim)

        # we compare with mode finding, starting from best centroids and merged centroids
        gmm = ModeFinding.Gmm{T}(weights_all_i, means_all_i, _safe_cholesky(gllim_star.Σ))
        modes, modes_merged = _find_modes(gmm, centroids_mean_merged_gllim, chol_covs_merged_centroids)
        
        
        # we choose the stat model : Sigma = Sigma_train + Sigma_obs 
        train_std = get_train_std(params)
        Sigma = Diagonal(train_std^2 .* ones(D)) + sigma_obs 
        model = FixedObsCovariance{T}(context, yobs, Sigma)

        # we apply ImportanceSampling
        model.proposition = Is.GaussianMixtureProposition{T}(weights_i, centroids_mean_merged_gllim, chol_covs_merged_centroids)
        mean_with_is = Is.importance_sampling(model, Ns)
      
        centroids_with_is, modes_with_is = Vector{IsInversion}(undef, K), Vector{IsInversion}(undef, K)
        @inbounds for k in 1:K
            # proposition around merged centroids
            model.proposition = Is.GaussianRegularizedProposition{T}(centroids_mean_merged_gllim[ :, k],  chol_covs_merged_centroids[k])
            centroids_with_is[k] = Is.importance_sampling(model,  Ns)
            
            # proposition around modes (from merged centroids)
            model.proposition = Is.GaussianRegularizedProposition{T}(modes_merged[ :, k],  chol_covs_merged_centroids[k])
            modes_with_is[k] = Is.importance_sampling(model,  Ns)
        end

        # we regroup the differents predictions
        inversions_no_is =  [
            CompleteInversion(Methods.GllimMean, mean_mean_gllim, diag(mean_cov_gllim), 0, nothing, Dict()),
            modes...,
        ]
        inversions_is = [
            CompleteInversion(Methods.IsMean, mean_with_is),
            [ CompleteInversion(Methods.IsCentroid(k), iv) for (k, iv) in enumerate(centroids_with_is)] ...
        ]
        
        # we compute y errors
        inversions_no_is = [add_y_err(context, yobs, iv) for iv in inversions_no_is]
        inversions_is = [add_y_err(context, yobs, iv) for iv in inversions_is]
        all_inversions = [inversions_no_is... , inversions_is...]

        # we compute the "entropy"
        entropies[n] = indice_multi_solutions(all_inversions)
       
        # we find the best prediction in the residual sense
        best_inversion_no_is = best_residual(Methods.BestNoIs, inversions_no_is)
        best_inversion = best_residual(Methods.Best, all_inversions)

        # we compute x errors and rescale x
        all_preds = [inversions_no_is... , inversions_is...,best_inversion_no_is, best_inversion]
        all_preds = [x_denorm(context, xobs, iv) for iv in all_preds]

        out[:, n] = all_preds
        update(mon)
        # if n % 30 == 0 
        #     GC.gc() # avoid memory use pick
        # end
    end
    @debug "\tIS predictions : $(since(ti))"

    out, entropies
end

function _extract_series(invs::Matrix{CompleteInversion}, methods::Vector{String})::Vector{Vector{CompleteInversion}}
    M, _ = size(invs)
    tmp = Dict{String,Vector{CompleteInversion}}()
    for m in 1:M 
        key = invs[m, 1].key 
        if key in methods  
            tmp[key] = invs[m, :]
        end
    end
    # we preserve `methods` order
    return [tmp[key] for key in methods]
end
extract_series(invs::Matrix{CompleteInversion}, methods::Vector{String}) = Dict(zip(methods, _extract_series(invs, methods)))

function aggregate(v::Vector{CompleteInversion})
    return hcat([inv.mean for inv in v]...)
end

"""
 cov must be defined 
"""
function aggregate_std(v::Vector{CompleteInversion})
    L = length(v[1].mean)
    return hcat([inv.cov == nothing ? NaN * ones(L) : sqrt.(inv.cov) for inv in v]...)
end

function get_by_key(v::Vector{Prediction}, key::String)::Matrix
    for p in v 
        if p.id == key
            return p.Xmean
        end
    end
end

_inf_to_missing(v) = isfinite(v)  ? v : missing
"""
 Format the serie given by `configs` 
"""
function format_inversions(invs::Matrix{CompleteInversion}, configs::Vector{Pair{String,SerieConfig}})::Vector{Prediction}
    _, N = size(invs)
    out::Vector{Prediction} = []
    L = length(invs[1,1].mean)
    series = _extract_series(invs, [ key for (key, _) in configs])
    for ((key, config), serie) in zip(configs, series)
        means, covs, yerr, xerr = zeros(L, N), zeros(L, N), zeros(N), zeros(N)
        withCov, withXerr = false, false
        diagnostics = Dict{String,Vector}()
        for n in 1:N 
            pred = serie[n]
            if pred.cov != nothing 
                covs[:, n] = pred.cov
                withCov = true
            end
            for (label, diagn) in pairs(pred.diagnostics)
                ds = get!(diagnostics, label, NaN * zeros(N))
                ds[n] = diagn
            end
            if pred.x_err != nothing 
                xerr[n] = pred.x_err 
                withXerr = true
            end
            means[:,n], yerr[n] = pred.mean,  pred.y_err
        end
        pred = Prediction(means, withCov ? covs : nothing; diagnostics = diagnostics,  Yerr =  yerr, Xerr =  withXerr ? xerr : nothing, config = config, id = key)
        push!(out, pred)
    end
    out
end

function regularize_inversions!(invs::Matrix{CompleteInversion}, methods_prefixes::Vector{String}) 
    M, N = size(invs)
    for prefix in methods_prefixes
        # regroup the methods 
        methods::Vector{Matrix{Float64}} = []
        idxs = []
        for m in 1:M 
            if startswith(invs[m, 1].key, prefix) 
                push!(methods, hcat([p.mean for p in invs[m, :]]...))
                push!(idxs, m) # remember concerned rows
            end
        end
        perms = Pred.regularization(methods)
        # update invs in place
        slice = view(invs, idxs, :)
        for (n, perm) in enumerate(perms)
            slice[:, n] .= slice[perm, n]
        end
    end
end

"""
 Sample training dictionnary, then initialise, then fit.
    Gamma is full, Sigma is Diagonal.
"""
function inference(ct::AbstractContext, gp::EMMetaGLLiM) 
    X, Y = Models.data_training(ct, gp.Ntrain; noise_std = gp.Ystd)
    L, _ = size(X)
    D, _ = size(Y)
    base_gllim = Gllim.GLLiM{Float64,FullCovs{Float64},DiagCovs{Float64}}(gp.K, L, D) 
    base_gllim, _ = Gllim.init_GLLIM(base_gllim, X, Y, gp.init)
    Gllim.train(base_gllim, X, Y, gp)
end
function inference(ct::AbstractContext, gp::VariableNoiseSimpleJointGMMTraining) 
    X, Y = Models.data_training(ct, gp.Ntrain; noise_std = 0)
    noise = randn(ct.D, gp.Ntrain)
    for i in 1:gp.Ntrain
        chol = Diagonal(Y[:,i] ./ gp.Ystd_ratio)
        Y[:, i] .+= chol * noise[:,i] 
    end
    L, _ = size(X)
    D, _ = size(Y)
    base_gllim = Gllim.GLLiM{Float64,FullCovs{Float64},DiagCovs{Float64}}(gp.K, L, D) 
    base_gllim, _ = Gllim.init_GLLIM(base_gllim, X, Y, gp.init)
    Gllim.train(base_gllim, X, Y, gp)
end



function train_inverse(run, context::AbstractContext, Yobs::Matrix, gllim_params::MetaGLLiM, is_params::IsParams; Xobs = nothing)::Tuple{Matrix{CompleteInversion},Vector,ComputationTime}
    ct = ComputationTime(context, gllim_params, is_params, size(Yobs)[2])
    if run >= 1
        Log.indent_log()
        if run >= 2
            Log.indent_log()
            ti = Dates.now()
            trained_gllim = inference(context, gllim_params)
            dura = since(ti)
            Archive.save([trained_gllim, dura],  Archive.FOLDER_GLLIM, context, gllim_params)
            Log.deindent_log()
        end
        flush(Base.stdout) # useful in batch mode 
        
        trained_gllim::Gllim.GLLiM, _ = Archive.load(Archive.FOLDER_GLLIM, context, gllim_params)
        
        # ti = Dates.now()
        # mat_preds, entropies = complete_prediction(context, Yobs, trained_gllim, is_params, Xobs)
        # dura = since(ti)
        # Archive.save((mat_preds, entropies, dura), Archive.FOLDER_PREDICTIONS, context, gllim_params, is_params)

        # time measurement
        _, _, dura_gllim_only = gllim_prediction(context, Yobs, trained_gllim, is_params)
        Archive.save(dura_gllim_only, Archive.FOLDER_BENCHMARK, context, gllim_params, is_params, :gllim_time)

        Log.deindent_log()
    end
    (mat_preds, entropies, dura) = Archive.load(Archive.FOLDER_PREDICTIONS, context, gllim_params, is_params)
    ct.prediction = dura

    # we always need to load the training time 
    _, ct.learning  = Archive.load(Archive.FOLDER_GLLIM, context, gllim_params)

    # we always need to load the gllim prediction time 
    ct.gllim_only_prediction  = Archive.load(Archive.FOLDER_BENCHMARK, context, gllim_params, is_params, :gllim_time)

    flush(Base.stdout) # useful in batch mode 
    mat_preds, entropies, ct
end

function export_predictions(context::AbstractContext, preds::Vector{Prediction}, out::String)
    res = Dict()
    for pred in preds 
        res[pred.id] = pred.Xmean
    end
    data = Dict(context.LABEL => res)
    write(out, JSON.json(data))
    @info "Data exported in $out"
end

"""
 Median filter over mean and stds neighbours, variable per variable
`c` and `Yobs` are needed for the reconstuction error 
"""
function median_filter(serie::Pred.Prediction, c::Models.AbstractContext, Yobs::Matrix, config::Pred.SerieConfig; width = 1)::Pred.Prediction
    L, N = size(serie.Xmean)
    withCov = serie.Xcovs !== nothing
    outMean, outCovs = copy(serie.Xmean), withCov ? copy(serie.Xcovs) : zeros(L, N)

    # border values are left unchanged

    for n in 1 + width:N - width
        for l in 1:L 
            outMean[l, n] = Statistics.median(outMean[l, n - width:n + width]) # 3 points window
            if withCov
                outCovs[l, n] = Statistics.median(outCovs[l, n - width:n + width]) # 3 points window
            end
        end
    end

    # reconstruction error 
    Yerr = Y_relative_err(c, Yobs, Models.from_physique(c, outMean), 1., zeros(c.D))
    return Pred.Prediction(outMean, withCov ? outCovs : nothing ; Yerr = Yerr, config = config)
end



function _test()
    ct = Models.MukundpuraBlocContext() 
    L, D, N, K = ct.L, ct.D, 10, 2
    # Yobs, _, Yobs_std = Models.load_observations(ct)
    _, Yobs = Models.data_training(ct, N)
    Yobs_std = 0.07 .* Yobs
    gllim = Gllim.randGLLiM(K, L, D)
    is_params = FixedCovIs(0.001, Yobs_std, 100)
    return complete_prediction(ct, Yobs, gllim, is_params, nothing)
end 

using InteractiveUtils
function _test_entropie()
    out, _ = _test()
    r = out[:, 1]
    @code_warntype indice_multi_solutions(r)
    @time indice_multi_solutions(r)
end
# _test_entropie();

function _test_gllim_only()
    ct = Models.MukundpuraBlocContext() 
    L, D, N, K = ct.L, ct.D, 100, 50
    # Yobs, _, Yobs_std = Models.load_observations(ct)
    _, Yobs = Models.data_training(ct, N)
    Yobs_std = 0.07 .* Yobs
    gllim = Gllim.randGLLiM(K, L, D)
    is_params = FixedCovIs(0.001, Yobs_std, 100)
    _, _ = dura = gllim_prediction(ct, Yobs, gllim, is_params)
    @show dura
end 
# _test_gllim_only()
