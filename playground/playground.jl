using LaTeXStrings
using Plots
using Statistics
using Serialization

# using Distributed
# @everywhere import GaussianMixtures

include("setup.jl")
include("procedure.jl")
include("plots.jl")
include("sobol.jl")
include("noise.jl")

function test(run)
    train_std = 0.005
    gllim_params = Gllim.GLLiMTraining(Gllim.MultiInit(), 40; Ystd = train_std)
    Ns = 10000
    
    # context = Models.JSC1HapkeContext() 
    # context = Models.BasaltHapkeContext() 
    # context = Models.NontroniteHapkeContext() 
    context = Models.MukundpuraBlocContext()

    L, D = context.L, context.D        
    Yobs, _, Yobs_std = Models.load_observations(context)
        
    # is_params = FixedCovIs(train_std, Yobs_std, Ns)
    is_params = DataCovIs(train_std, 20, 10000)

    mat_preds , _,  _ = train_inverse(run, context, Yobs, gllim_params, is_params)
    preds = format_inversions(mat_preds, [
        Methods.GllimMean => SerieConfig(L"$\hat{x}_{GLLiM}$", :basic, "red"),
        # Methods.GllimBestCentroid(1) => SerieConfig(L"best centroid 1", :basic, "blue"),
        # Methods.GllimBestCentroid(2) => SerieConfig(L"best centroid 2", :dashed, "orange"),
        # Methods.GllimMergedCentroid(1) => SerieConfig(L"merged centroid 1", :basic, "green"; alpha = 0.5),
        # Methods.GllimMergedCentroid(2) => SerieConfig(L"merged centroid 2", :basic, "orange"; alpha = 0.5),
        # Methods.GllimModeFromBestCentroid(1) => SerieConfig(L"mode from best centroid 1", :circle, "green"),
        # Methods.GllimModeFromBestCentroid(2) => SerieConfig(L"mode from best centroid 2", :circle, "orange"),
        Methods.GllimModeFromMergedCentroid(1) => SerieConfig(L"mode from merged centroid 1", :dashed, "green"),
        Methods.GllimModeFromMergedCentroid(2) => SerieConfig(L"mode from merged centroid 2", :dashed, "orange"),
        Methods.IsCentroid(1) => SerieConfig(L"IS centroid 1", :circle, "green"),
        Methods.IsCentroid(2) => SerieConfig(L"IS centroid 2", :circle, "orange"),
    ])
    # push!(preds, Prediction(Xobs; config = SerieConfig(L"$x_{obs}$", :basic, "green")))

    path = Archive.get_path(Archive.FOLDER_GRAPHS, context, gllim_params, is_params, preds; label = context.LABEL)
    Plotting.plot_components(context, preds;  savepath = path,  varnames = context.variables_names)
end

function exp_hockey_stick(run)
    train_std = 0.01
    gllim_params = Gllim.GLLiMTraining(Gllim.MultiInit(), 40; Ystd = train_std)
    Ns = 10000
    
    # context = Models.JSC1HapkeContext() 
    # context = Models.NontroniteHapkeContext() 
    contexts = [
        # Models.MukundpuraBlocContext(), 
        Models.MukundpuraPoudreContext(), 
        # Models.BlackyBlocContext(), 
        # Models.BlackyPoudreContext(), 
        # Models.HowarditeContext()
        ]
    all_preds::Vector{Vector{Prediction}} = []

    is_paramss = []
    std_ratio = 20
    for context in contexts 
        L, D = context.L, context.D        
        Yobs, _, Yobs_std = Models.load_observations(context)
        
        is_params = FixedCovIs(train_std,Yobs_std, Ns)
        # is_params = DataCovIs(train_std, std_ratio, Ns)
        push!(is_paramss, is_params)
        # Yobs, _ = Models.load_observations(context)
        
        mat_preds , _,  _ = train_inverse(run, context, Yobs, gllim_params, is_params)
        preds = format_inversions(mat_preds, [
            Methods.GllimMean => SerieConfig(L"$\hat{x}_{GLLiM}$", :basic, "red"),
            Methods.IsMean => SerieConfig(L"$\hat{x}_{IS-G}$", :basic, "orange"),
            Methods.Best => SerieConfig("Best residual", :dashed, "blue"),
        ])

        export_path = Archive.get_path(Archive.FOLDER_PREDICTIONS, context, gllim_params, is_params; label = context.LABEL) * ".json"
        export_predictions(context, preds, export_path)

        # push!(preds, Prediction(Xobs; config = SerieConfig(L"$x_{obs}$", :basic, "green")))
        push!(all_preds, preds)
    end
    path = Archive.get_path(Archive.FOLDER_GRAPHS, gllim_params, Archive.get_hash(contexts...), Archive.get_hash(is_paramss...), :hockey_stick_mukun)
    plot_hockey_stick(contexts, all_preds, path)
end

function exp_double_solution(run)
    # context = Models.DoubleSolutionContext(1)
    context = Models.TripleSolutionContext(1)
    train_std = 0.07
    gllim_params = Gllim.SimpleJointGMMTraining(Gllim.MultiInit(), 40; Ystd = train_std)
    is_params = DataCovIs(train_std, 100, 50000)

    Nobs = 100
    Xobs =  vcat([ _wave_sample(Nobs, l)' for l in 1:context.L]...) .* (1 / 3) # entre 0 et 1/3
    Yobs = F(context, Xobs)
    Xobs, _ = Models.to_physique(context, Xobs)

    mat_preds , _,  _ = train_inverse(run, context, Yobs, gllim_params, is_params)
    regularize_inversions!(mat_preds, [Methods.GllimModeFromBestCentroid(""), Methods.GllimMergedCentroid(""), Methods.IsCentroid("")])
    configs = [
        Methods.IsMean => SerieConfig("mean is", :basic, "red"),
        # Methods.GllimBestCentroid(1) => SerieConfig(L"best centroid 1", :basic, "blue"),
        # Methods.GllimBestCentroid(2) => SerieConfig(L"best centroid 2", :basic, "orange"),
        Methods.GllimMergedCentroid(1) => SerieConfig(L"merged centroid 1", :basic, "blue"; alpha = 0.4),
        Methods.GllimMergedCentroid(2) => SerieConfig(L"merged centroid 2", :basic, "orange"; alpha = 0.5),
        Methods.GllimModeFromBestCentroid(1) => SerieConfig(L"mode from best centroid 1", :dashed, "blue"),
        Methods.GllimModeFromBestCentroid(2) => SerieConfig(L"mode from best centroid 2", :dashed, "orange"),
        # Methods.GllimModeFromMergedCentroid(1) => SerieConfig(L"mode from merged centroid 1", :dashed, "blue"),
        # Methods.GllimModeFromMergedCentroid(2) => SerieConfig(L"mode from merged centroid 2", :dashed, "orange"),
        # Methods.IsCentroid(1) => SerieConfig(L"IS centroid 1", :circle, "blue"),
        # Methods.IsCentroid(2) => SerieConfig(L"IS centroid 2", :circle, "orange"),
        # Methods.Best => SerieConfig("Best", :basic, "orange")
    ]
    preds = format_inversions(mat_preds, configs)

    Xobs_alt = (2 / 3) .- Xobs
    Xobs_alt_2 = Xobs .+ 2 / 3
    preds = [preds...,
        Prediction(Xobs; config = SerieConfig("true sol. 1", :basic, "green"; alpha = 0.3)),
        Prediction(Xobs_alt; config = SerieConfig("true sol. 2", :basic, "green"; alpha = 0.3)),
        Prediction(Xobs_alt_2; config = SerieConfig("true sol. 3", :basic, "green"; alpha = 0.3)),
    ]

    path = Archive.get_path(Archive.FOLDER_GRAPHS, context, gllim_params, is_params, keys(configs); label = context.LABEL)
    Plotting.plot_components(context, preds;  savepath = path, varnames = context.variables_names)
end

function exp_sobol()
    params = SobolGeoms([10,30,50], [15, 30, 40, 50, 70, 75], [20, 50, 70, 90, 120,170])
    sampled_geoms = setup_geometries(params, false)
    run_plot_sobol(sampled_geoms, 0, false)
end

function exp_crism(run)
    c = Models.CrismHapkeContext()
    gllim_params = Gllim.JointGMMTraining(Gllim.MultiInit(), 50)

    Yobs, Ystd, Xres, Xstd, latlong = Models.load_clean(c)
    # is_params = FixedCovIs(0.001, Ystd, 10000)
    is_params = DataCovIs(0.001, 10, 10000)
    mat_preds , _,  _ = train_inverse(run, c, Yobs, gllim_params, is_params)
    configs = [
        Methods.GllimMean => SerieConfig("mean", :basic, "red"),
        Methods.IsMean => SerieConfig("mean is", :basic, "red"),
        # Methods.GllimBestCentroid(1) => SerieConfig(L"best centroid 1", :basic, "blue"),
        # Methods.GllimBestCentroid(2) => SerieConfig(L"best centroid 2", :basic, "orange"),
        # Methods.GllimMergedCentroid(1) => SerieConfig(L"merged centroid 1", :basic, "blue"; alpha = 0.4),
        # Methods.GllimMergedCentroid(2) => SerieConfig(L"merged centroid 2", :basic, "orange"; alpha = 0.5),
        # Methods.GllimModeFromBestCentroid(1) => SerieConfig(L"mode from best centroid 1", :dashed, "blue"),
        # Methods.GllimModeFromBestCentroid(2) => SerieConfig(L"mode from best centroid 2", :dashed, "orange"),
        # Methods.GllimModeFromMergedCentroid(1) => SerieConfig(L"mode from merged centroid 1", :dashed, "blue"),
        # Methods.GllimModeFromMergedCentroid(2) => SerieConfig(L"mode from merged centroid 2", :dashed, "orange"),
        # Methods.IsCentroid(1) => SerieConfig(L"IS centroid 1", :circle, "blue"),
        # Methods.IsCentroid(2) => SerieConfig(L"IS centroid 2", :circle, "orange"),
        Methods.Best => SerieConfig("best", :basic, "orange"),
    ]
    preds = format_inversions(mat_preds, configs)
    preds = [preds...,
        Prediction(Xres; config = SerieConfig("MCMC", :basic, "green"; alpha = 0.3)),
    ]

    preds = [
        (p.Xmean[1,:], p.config.label) for p in preds # omega uniquement
    ]
    p = Plotting.plot_map(preds, latlong;figsize = (2000, 1500), color_palette = :rainbow)
    path = Archive.get_path(Archive.FOLDER_GRAPHS, c, gllim_params, is_params, label = "map")
    png(p, path)
    @info "Plotted in $path"
end

function plot_nontronite()
    c = Models.NontroniteHapkeContext()
    m1, cov1 = Models.load_results_alt(c, true)
    m2, cov2 = Models.load_results_alt(c, false)
    subplots = []
    for i in 1:4
        push!(subplots,  plot(1:100, m2[i, :]))
    end
    plot(subplots...)
end

function plot_phase_function()
    path = "/Users/kuglerb/Documents/Work/misc/hapke_lab_floor:1794134556-3328198483"
    d = deserialize(path)
    X_us = d[Methods.Best]
    context = Models.NontroniteHapkeContext()
    Yobs,  _, _ = Models.load_observations(context)
    D, N = size(Yobs)
    X_mcmc::Matrix{Float64}, _ = Models.load_results_alt(context, true)
    
    P_us, P_mcmc, PY_us, PY_mcmc = zeros(D, N),zeros(D, N),zeros(D, N),zeros(D, N)
    subplots = []
    for (d, geom) in enumerate(context.settings_geometries)
        # Phase function from b, c 
        p_us = [Models.phg2(X_us[3,i], X_us[4,i], geom.cosg) for i in 1:N]
        p_mcmc = [Models.phg2(X_mcmc[3,i], X_mcmc[4,i], geom.cosg) for i in 1:N]
        
        # Phase function from y, omega, theta
        p_from_y_us = [Models.phase_from_reflectance(Yobs[d, i], X_us[1,i], X_us[2,i], Models.DEFAULT_H, Models.DEFAULT_B0, geom) for i in 1:N]
        p_from_y_mcmc = [Models.phase_from_reflectance(Yobs[d, i], X_mcmc[1,i], X_mcmc[2,i], Models.DEFAULT_H, Models.DEFAULT_B0, geom) for i in 1:N]
        
        # store for later plot 
        P_us[d, :] = p_us
        P_mcmc[d, :] = p_mcmc
        PY_us[d, :] = p_from_y_us
        PY_mcmc[d, :] = p_from_y_mcmc

        subp = plot(1:N, hcat(p_us, p_mcmc, p_from_y_us, p_from_y_mcmc), 
            labels = ["GLLiM - Is - Mean" "MCMC" "From Y : GLLiM - Is - Mean" "From Y : MCMC"], 
            linecolors = ["red" "green" "red" "green"],
            style = [:dash :dash :solid :solid],
            title = "d = $d ; g = $(round(geom.g; digits = 5))", 
            ylabel = L"P_{HG2}"
        )
        push!(subplots, subp)
    end

    diffs = hcat(
        [norm(P_us[:, i] .- P_mcmc[:, i]) for i in 1:N], 
        [norm(PY_us[:, i] .- PY_mcmc[:, i]) for i in 1:N]
    )
    plot(1:N, diffs; labels = ["From b, c" "From Y, omega, theta"])
    # plot(subplots..., size = (2000,2000))
    # p = plot()
    # scatter!(p, X_us[3, :], X_us[4, :])
    # scatter!(p, X_mcmc[3, :], X_mcmc[4, :])
    # b = range(0, 1; length= 100)
    # plot!(p, b, Models.Crelation(b), label = "Hockey stick")
    # p
end


# test()
# exp_double_solution(2)
# exp_hockey_stick(2)


# exp_noise(2)
# exp_crism(1)

# plot_nontronite()

plot_phase_function()