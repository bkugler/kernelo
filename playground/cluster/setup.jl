using Kernelo.Archive

# logging setup 
include("../setup.jl")

# change archive path
Archive.PATHS.ROOT_PATH_DATA = "/home/bkugler/Documents/DATA/"
Archive.PATHS.ROOT_PATH_ARCHIVE = "/services/scratch/mistis/bkugler/OUT/"
