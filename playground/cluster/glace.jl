using Kernelo.Models
using Kernelo.Gllim
using Kernelo.Archive

include("../inversion_cube.jl")
include("setup.jl")

const experience = ARGS[1]
@info "Starting experience $experience ..."
flush(Base.stdout) # useful in batch mode 

if experience == "B385"
    hapke_massive_B385()
elseif experience == "144E9_L"
    hapke_massive_glace(:L)
elseif experience == "144E9_S"
    hapke_massive_glace(:S)
elseif experience == "140520"
    hapke_massive_Mars2020()
end