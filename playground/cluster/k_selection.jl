using Kernelo.Archive
using Dates 

include("../k_selection.jl")
include("setup.jl")

# hint for std: 0.1, 0.01, 0.001
context_id = ARGS[1]
if context_id == "Nontronite"
    model = Models.NontroniteHapkeContext()
elseif context_id == "Glace"
    model = Models.GlaceContext()
elseif context_id == "MukundpuraBloc"
    model = Models.MukundpuraBlocContext()
else 
    error("Context not supported $(context_id)")
end
Ystd = parse(Float64, ARGS[2])
@info "K selection on $(model.LABEL) with std = $Ystd"
flush(Base.stdout) # useful in batch mode 

Ks = collect(range(5, 100; step = 5))

ti = Dates.now()
out = train_select_K(model, Ks, Ystd)
duration = Dates.now() - ti

Archive.save([out, duration], Archive.FOLDER_PREDICTIONS, model, Ks, Ystd)

